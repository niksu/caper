(*
  Copyright Nik Sultana, November 2018-December 2022
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Configuration
*)

let version = "1.0"

let quiet_mode = ref false
let diagnostic_output_mode = ref false
let parse_and_prettyprint_mode = ref false
let english_input_mode = ref false
let english_output_mode = ref false
let translate_pcap_back_mode = ref false
let show_config_mode = ref false
let process_stdin_single_expression_mode = ref false
let expression_list : string list ref = ref []
let show_brackets_mode = ref false(*FIXME not sure this is actually useful/needed*)
let relaxed_mode = ref false
let unresolved_names_mode = ref false
let not_expand = ref false
let strict_expand_only = ref false
let smt_translate = ref false
let pseudonymise = ref false
let generate_html_output = ref false
let pretty_printer_max_line_width = ref 150 (*FIXME currently this configuration parameter cannot be changed by the user*)
let pretty_printer_indentation = "  " (*FIXME currently this configuration parameter cannot be changed by the user*)
let expand_ipv6_addresses = ref false
(*
FIXME ideas for additional flags:
  - use English instead of symbols for connectives (e.g., "and" vs "&&").
*)
let not_undistribute = ref false
let weak_inference = ref false
let max_trampoline_iterate = ref 40 (*FIXME currently this configuration parameter cannot be changed by the user*)
let generate_bpf_output = ref false
let generate_optimized_bpf_output = ref false
let generate_bpf_graph_output = ref false
let max_rec = ref 0
let default_snap_len = 262144 (*NOTE This configuration parameter cannot be changed by the user. It provides a default value for the next parameter. This value was chosen in conformance with tcpdump -- see https://github.com/the-tcpdump-group/tcpdump/blob/2654afbcee6ecb34fbd29762198c974480f14f75/netdissect.h*)
let snap_len = ref default_snap_len
let linux_bpf = ref false
let num_implicit_clause_expansions = ref 1 (*FIXME currently this configuration parameter cannot be changed by the user*)
let num_distribution_application = ref 0 (*FIXME currently this configuration parameter cannot be changed by the user*)
let max_strong_inference_fmla_size = ref 50 (*FIXME currently this configuration parameter cannot be changed by the user*)
