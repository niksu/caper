#!/usr/bin/env bash

# This script will package all of the distributions that it can, given the tools available

if which nix-build; then
  nix-build --no-out-link
  # Will emit a derivation to the system nix store.
  # If you want to install, do something like:
  # `nix-env -f . -iA caper``
  # `nix-shell -p 'import ./default.nix'``
  # add `(import /absolute/path/to/default.nix)` as a package in /etc/nixos/configuration.nix
fi

if docker ps > /dev/null; then
  docker build -t caper.makepkg -f Dockerfile.makepkg .
  docker build -t caper.makedeb -f Dockerfile.makedeb .
  docker run --rm -it -v $(pwd)/pkg:/out caper.makepkg
  docker run --rm -it -v $(pwd)/pkg:/out caper.makedeb
fi

if which opam; then
  yes | opam pin remove .
  yes | opam install --working-dir .
fi