#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt BPF.
# Hyunsuk Bang, February 2023

# FIXME sensitive to the directory in which this is run
tests/bpf_regression.sh "./caper.byte -1stdin -BPF -linux -max_rec 2 -not_undistribute -q -r" tests/bpf_regression_test.sh
