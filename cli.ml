(*
  Copyright Nik Sultana, November 2018-December 2022
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Command-line configuration
*)

let contributors = "held by its contributors: see https://gitlab.com/niksu/caper#contributors"

type param_ty = {
  param : string;
  desc : string;
  action : unit -> unit;
  status : unit -> string;
}

let arg_parameter_handler = ref (fun (_ : string) -> false)

let rec supported_params : param_ty list =
  [{param = "-h";
    desc = "Show usage-related help.";
    action = (fun () -> show_usage true);
    status = (fun () -> "(not applicable)");
   };
   {param = "-q";
    desc = "Quiet mode -- suppress updates on processing.";
    action = (fun () -> Config.quiet_mode := true);
    status = (fun () -> string_of_bool !Config.quiet_mode);
   };
   {param = "-d";
    desc = "Diagnostic output -- include more updates on processing.";
    action = (fun () -> Config.diagnostic_output_mode := true);
    status = (fun () -> string_of_bool !Config.diagnostic_output_mode);
   };
   {param = "-p";
    desc = "Parse, pretty-print, and exit. Used for simple testing of an expression.";
    action = (fun () -> Config.parse_and_prettyprint_mode := true);
    status = (fun () -> string_of_bool !Config.parse_and_prettyprint_mode);
   };
   {param = "-engl-in";
    desc = "Input an english expression to standard input. If run with -engl-out, translates from english to pcap and back and outputs the english expression. Only works with -1stdin.  Incompatible with -pcap-back.";
    action = (fun () -> Config.english_input_mode := true);
    status = (fun () -> string_of_bool !Config.english_input_mode);
   };
   {param = "-engl-out";
    desc = "Output an english expression. If run with -engl-in, translates from english to pcap and back. Only works with -1stdin. Incompatible with -pcap-back.";
    action = (fun () -> Config.english_output_mode := true);
    status = (fun () -> string_of_bool !Config.english_output_mode);
   };
   {param = "-pcap-back";
    desc = "Input a pcap expression. Translates from pcap to english and back and outputs the pcap expression. Only works with -1stdin. Incompatible with -engl-in and -engl-out.";
    action = (fun () -> Config.translate_pcap_back_mode := true);
    status = (fun () -> string_of_bool !Config.translate_pcap_back_mode);
   };
   {param = "-b";
    desc = "Don't try to elide brackets aggressively for tidier output.";
    action = (fun () -> Config.show_brackets_mode := true);
    status = (fun () -> string_of_bool !Config.show_brackets_mode);
   };
   {param = "-c";
    desc = "Show config.";
    action = (fun () -> Config.show_config_mode := true);
    status = (fun () -> string_of_bool !Config.show_config_mode);
   };
   {param = "-1stdin";
    desc = "Process a single expression, via stdin.";
    action = (fun () -> Config.process_stdin_single_expression_mode := true);
    status = (fun () -> string_of_bool !Config.process_stdin_single_expression_mode);
   };
   {param = "-r";
    desc = "Relaxed printing of expressions (e.g., singleton disjuncts).";
    action = (fun () -> Config.relaxed_mode := true);
    status = (fun () -> string_of_bool !Config.relaxed_mode);
   };
   {param = "-n";
    desc = "Do not resolve names.";
    action = (fun () -> Config.unresolved_names_mode := true);
    status = (fun () -> string_of_bool !Config.unresolved_names_mode);
   };
   {param = "-e";
    desc = "Provide expression as parameter.";
    action = (fun () ->
     arg_parameter_handler := (fun s ->
       Config.expression_list := s :: !Config.expression_list;
       true));
    status = (fun () ->
      let count = ref 0 in
      let result =
        List.fold_right (fun e acc ->
          count := 1 + !count;
          ("    " ^ string_of_int !count ^ ": " ^ e) :: acc) !Config.expression_list []
        |> String.concat "\n" in
      "\n" ^ result)
   };
   {param = "-HTML";
    desc = "Generate HTML version of the output with keywords and relops styled.";
    action = (fun () -> Config.generate_html_output := true);
    status = (fun () -> string_of_bool !Config.generate_html_output);
   };
   {param = "-not_expand";
    desc = "Do not expand expressions.";
    action = (fun () -> Config.not_expand := true);
    status = (fun () -> string_of_bool !Config.not_expand);
   };
   {param = "-strict_expand_only";
    desc = "Only carry out the expansion step, without further processing.";
    action = (fun () -> Config.strict_expand_only := true);
    status = (fun () -> string_of_bool !Config.strict_expand_only);
   };
   {param = "-expand_ipv6_addresses";
    desc = "Expand IPv6 addresses in output. Does not affect expression expansion -- see '-not_expand'.";
    action = (fun () -> Config.expand_ipv6_addresses := true);
    status = (fun () -> string_of_bool !Config.expand_ipv6_addresses);
   };
   {param = "-smt_translate";
    desc = "Translate to SMT.";
    action = (fun () -> Config.smt_translate := true);
    status = (fun () -> string_of_bool !Config.smt_translate);
   };
   {param = "-pseudonymise";
    desc = "Replace host names, addresses, etc with pseudonyms to facilitate sharing of a pcap expression outside an organisation.";
    action = (fun () -> Config.pseudonymise := true);
    status = (fun () -> string_of_bool !Config.pseudonymise);
   };
   {param = "-not_undistribute";
    desc = "Do not apply the 'undistribute' simplification transformation to expressions.";
    action = (fun () -> Config.not_undistribute := true);
    status = (fun () -> string_of_bool !Config.not_undistribute);
   };
   {param = "-weak_inference";
    desc = "Do not aggresively simplify formulas.";
    action = (fun () -> Config.weak_inference := true);
    status = (fun () -> string_of_bool !Config.weak_inference);
   };
   {param = "-BPF";
    desc = "Transform expanded pcap into BPF. -not_undistribute flag will be triggered implicitly.";
    action = (fun () -> Config.generate_bpf_output := true; Config.not_undistribute := true);
    status = (fun () -> string_of_bool !Config.generate_bpf_output);
   };
   {param = "-BPF_optimized";
    desc = "Transform expanded pcap into BPF and optimize it. -not_undistribute flag will be triggered implicitly.";
    action = (fun () -> Config.generate_bpf_output := true; Config.generate_optimized_bpf_output := true; Config.not_undistribute := true);
    status = (fun () -> string_of_bool !Config.generate_optimized_bpf_output);
   };
   {param = "-linux";
   desc = "Generate BPF that accesses ancillary data from the Linux kernel. Must be used with -BPF or -BPF_optimized. More info can be found on <linux/filter.h>";
   action = (fun () -> Config.linux_bpf := true);
   status = (fun () -> string_of_bool !Config.linux_bpf);
   };
   {param = "-max_rec"; (*FIXME give it a more specific name?*)
    desc = "Maximum depth of recursion for BPF generator.";
    action = (fun () ->
     arg_parameter_handler := (fun n ->
     Config.max_rec := int_of_string n; true));
    status = (fun () -> string_of_int !Config.max_rec);
   };
   {param = "-graph"; (*FIXME give it a more specific name?*)
   desc = "Generate BPF output in DOT language for Graphviz. Must be used with -BPF or -BPF_optimized.";
   action = (fun () -> Config.generate_bpf_graph_output := true);
   status = (fun () -> string_of_bool !Config.generate_bpf_graph_output);
  };
   {param = "-s"; (*FIXME give it a more specific name?*)
    desc = "\"Snap\" length. Defaults to " ^ string_of_int Config.default_snap_len ^ ".";
    action = (fun () ->
     arg_parameter_handler := (fun n ->
     let n_int = int_of_string n in
     Config.snap_len := if n_int = 0 then Config.default_snap_len (*conforming with line 727 of https://github.com/the-tcpdump-group/tcpdump/blob/2654afbcee6ecb34fbd29762198c974480f14f75/tcpdump.1.in*) else n_int;
     true));
    status = (fun () -> string_of_int !Config.snap_len);
   };
  ]
and show_usage (with_version : bool) =
  let start_string =
    if with_version then "Caper v" ^ Config.version ^ ". " else "" in
  print_endline (start_string ^ "Copyright " ^ contributors);
  print_endline ("Caper can be downloaded from https://www.nik.network/caper");
  print_endline ("It is distributed under GNU General Public License v3 or later (https://www.gnu.org/licenses/gpl.html)");
  print_endline "";
  print_endline ("Usage:");
  List.iter (fun p ->
    print_endline (Printf.sprintf " %-25s %s" p.param p.desc)
  ) supported_params;
  exit 0

let print_config () : unit =
  print_endline ("Config:");
  List.iter (fun p ->
    print_endline ("  " ^ p.param ^ "    " ^ p.status ())
  ) supported_params

let lookup_supported_params (param_s : string) : param_ty option =
  match List.filter (fun param -> param.param = param_s) supported_params with
  | [] -> None
  | [p] -> Some p
  | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Multiple matches for parameter '" ^ param_s ^ "'")

let process_params (params : string list) : unit =
  let arg_no = ref 0 in
  List.iter (fun s ->
    if !arg_parameter_handler s then
      begin
        arg_parameter_handler := (fun _ -> false);
      end
    else
      begin
        match lookup_supported_params s with
        | None ->
            if !arg_no = 0 then ()
            else failwith (Aux.error_output_prefix ^ ": " ^ "Unrecognised parameter: " ^ s)
        | Some p -> p.action ()
      end;
    arg_no := 1 + !arg_no) params
