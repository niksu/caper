(*
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: sock_filter type definition
*)

type operator =
  | Multiplication
  | Division
  | Modulo
  | Addition
  | Subtraction
  | Arith_and
  | Arith_or
  | Arith_xor
  | Logical_shift_left
  | Logical_shift_right

type value =
  | X
  | True
  | False
  | Off of int
  | Offset of value (* Offset value for [x + 16]*)
  | Mem of int
  | Hex of int (* Literal value *)
  | Hexj of int (* Literal value used only on jump related opcode *)
  | Lit of int (* Literal value *)
  | Exp of value * value * operator
  | NilVal

type opcode =
  | Ld of value
  | Ldi of value
  | Ldh of value
  | Ldb of value
  | Ldx of value
  | Ldxi of value
  | Ldxb of value

  | St of value
  | Stx of value

  | Add of value
  | Sub of value
  | Mul of value
  | Div of value
  | Mod of value
  | Neg of value
  | And of value
  | Or of value
  | Xor of value
  | Lsh of value
  | Rsh of value

  | Tax
  | Txa

  | Logical_And
  | Logical_Or

  | No_op of opcode (* Special opcode for bpf_optimizer.ml *)
  | Nil_op

  | Num of int (* special opcode for eval.ml. *)
  | List of opcode list (* special opcode for eval.ml.*)
  | Ret of value

type cond =
  | True_cond
  | False_cond
  | No_cond
  | Ip_Protochain_cond of value
  | Ip6_Protochain_cond of value
  | Icmp6_Protochain_cond of value
  | Ip_Protochain_Bridge_cond of value
  | Ip6_Protochain_Bridge_cond of value
  | Icmp6_Protochain_Bridge_cond of value
  | Jmp of value
  | Ja of value
  | Jeq of value
  | Jneq of value
  | Jne of value
  | Jlt of value
  | Jle of value
  | Jgt of value
  | Jge of value
  | Jset of value

let num_to_int opcode =
  match opcode with
  | Num x -> x
  | _ -> failwith "only 'Num x' is allowed to be transformed into integer x"

let op_list_to_list opcode =
  match opcode with
  | List x -> x
  | other -> [other]

(*
  The name sock_filter is from <linux/filter.h>
  source: https://github.com/torvalds/linux/blob/master/include/uapi/linux/filter.h

  struct sock_filter {	/* Filter block */
	__u16	code;           /* Actual filter code */
	__u8	jt;	            /* Jump true */
	__u8	jf;	            /* Jump false */
	__u32	k;              /* Generic multiuse field */
  };
*)
type sock_filter = {
  code: opcode list;
  cond: cond;
  jt: sock_filter option;
  jf: sock_filter option;
}

let ret_true = Some {code = [Ret True]; cond = True_cond; jt = None; jf = None;}
let ret_false = Some {code = [Ret False]; cond = False_cond; jt = None; jf = None;}
let nil_sock_filter = Some {code = [Nil_op]; cond = No_cond; jt = None; jf = None;}
let gen_ip_protochain n = Some {code = []; cond = Ip_Protochain_cond (Lit n); jt = ret_true; jf = ret_false;} (* place holder for protochain *)
let gen_ip6_protochain n = Some {code = []; cond = Ip6_Protochain_cond (Lit n); jt = ret_true; jf = ret_false;} (* place holder for protochain *)
let gen_icmp6_protochain n = Some {code = []; cond = Icmp6_Protochain_cond (Lit n); jt = ret_true; jf = ret_false;} (* place holder for protochain *)
let gen_ip_protochain_bridge n = Some {code = []; cond = Ip_Protochain_Bridge_cond (Lit n); jt = ret_true; jf = ret_false;} (* place holder for protochain *)
let gen_ip6_protochain_bridge n = Some {code = []; cond = Ip6_Protochain_Bridge_cond (Lit n); jt = ret_true; jf = ret_false;} (* place holder for protochain *)
let gen_icmp6_protochain_bridge n = Some {code = []; cond = Icmp6_Protochain_Bridge_cond (Lit n); jt = ret_true; jf = ret_false;} (* place holder for protochain *)

let pretty_format (opcode : string)  (operand : string) = Printf.sprintf "%s %s" opcode operand

let operator_to_str = function
  | Multiplication -> "*"
  | Division -> "/"
  | Modulo -> "%"
  | Addition -> "+"
  | Subtraction -> "-"
  | Arith_and -> "&"
  | Arith_or  -> "|"
  | Arith_xor -> "^"
  | Logical_shift_left -> ">>"
  | Logical_shift_right -> "<<"

let value_to_str v =
  let rec helper v =
    match v with
    | X -> "x"
    | True -> "true"
    | False -> "false"
    | Off idx -> Printf.sprintf "[%d]" idx
    | Mem idx ->  Printf.sprintf "M[%d]" idx
    | Hex num -> Printf.sprintf "0x%x" num
    | Hexj num -> Printf.sprintf "#0x%x" num
    | Lit num -> Printf.sprintf "%d" num
    | Exp (val1,val2,operator) -> Printf.sprintf "(%s %s %s)" (helper val1) (operator_to_str operator) (helper val2)
    | Offset v -> Printf.sprintf "[%s]" (helper v) in
  let str_val = helper v in
  if Str.string_match (Str.regexp "(.*)") str_val 0 then
    String.sub str_val 1 (String.length str_val - 2)
  else if Str.string_match (Str.regexp "\\[(.*)\\]") str_val 0 then
    "[" ^ String.sub str_val 2 (String.length str_val - 4) ^ "]" else str_val

let opcode_to_str = function
  | Ld value -> pretty_format "ld" (value_to_str value)
  | Ldi value -> pretty_format "ldi" (value_to_str value)
  | Ldh value -> pretty_format "ldh" (value_to_str value)
  | Ldb value -> pretty_format "ldb" (value_to_str value)
  | Ldx value -> pretty_format "ldx" (value_to_str value)
  | Ldxi value -> pretty_format "ldxi" (value_to_str value)
  | Ldxb value -> pretty_format "ldxb" (value_to_str value)
  | St value ->  pretty_format "st" (value_to_str value)
  | Stx value -> pretty_format "stx" (value_to_str value)
  | Add value -> pretty_format "add" (value_to_str value)
  | Sub value -> pretty_format "sub" (value_to_str value)
  | Mul value -> pretty_format "mul" (value_to_str value)
  | Div value -> pretty_format "div" (value_to_str value)
  | Mod value -> pretty_format "mod" (value_to_str value)
  | Neg value -> pretty_format "neg" (value_to_str value)
  | And value -> pretty_format "and" (value_to_str value)
  | Or value -> pretty_format "or" (value_to_str value)
  | Xor value -> pretty_format "xor" (value_to_str value)
  | Lsh value -> pretty_format "lsh" (value_to_str value)
  | Rsh value -> pretty_format "rsh" (value_to_str value)
  | Tax -> "tax"
  | Txa -> "txa"
  | Logical_And -> "&&"
  | Logical_Or -> "||"
  | Num _ -> failwith "Num _ should not be transformed into string"
  | List x -> failwith "List should not be transformed into string"
  | Ret value ->pretty_format "ret" (value_to_str value)
  | No_op _ | Nil_op -> ""

let cond_to_str = function
  | No_cond -> "no_cond"
  | Ip_Protochain_cond value -> pretty_format "ip protochain" (value_to_str value)
  | Ip6_Protochain_cond value -> pretty_format "ip6 protochain" (value_to_str value)
  | Icmp6_Protochain_cond value -> pretty_format "icmp6_protochain" (value_to_str value)
  | Ip_Protochain_Bridge_cond value -> pretty_format "ip protochain_bridge" (value_to_str value)
  | Ip6_Protochain_Bridge_cond value -> pretty_format "ip6 protochain_bridge" (value_to_str value)
  | Icmp6_Protochain_Bridge_cond value -> pretty_format "icmp6_protochain_bridge" (value_to_str value)
  | Jmp value -> pretty_format "jmp" (value_to_str value)
  | Ja value -> pretty_format "ja" (value_to_str value)
  | Jeq value -> pretty_format "jeq" (value_to_str value)
  | Jneq value -> pretty_format "jneq" (value_to_str value)
  | Jne value -> pretty_format "jne" (value_to_str value)
  | Jlt value -> pretty_format "jlt" (value_to_str value)
  | Jle value -> pretty_format "jle" (value_to_str value)
  | Jgt value -> pretty_format "jgt" (value_to_str value)
  | Jge value -> pretty_format "jge" (value_to_str value)
  | Jset value -> pretty_format "jset" (value_to_str value)
