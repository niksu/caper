(*
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Postfix expanded pcap expressions to sock_filter tree
*)

open Inst
open Util
open Headers
open Ether
open Vlan
open Mpls
open Bound
open Ip
open Ip6
open Arp
open Rarp
open Icmp
open Icmp6
open Tcp
open Udp
open Sctp
open Eval

let base_header = ref Nil

let check_ip6_frag (pcaps : string list) : bool =
  let rec check_ip6_frag_helper (pcaps : string list) : int =
    match pcaps with
    | [] -> 0
    | "!" :: rest | "&&" :: rest | "||" :: rest -> check_ip6_frag_helper rest
    | _ :: rest -> 1 + check_ip6_frag_helper rest
  in
  check_ip6_frag_helper pcaps >= 1

let protocol_to_sock_filter (protocol : string) (protocol_info : string list) (headers : packet_headers) =
  let protocol_to_sf =
    match protocol with
    | "icmp" -> icmp_to_sock_filter
    | "tcp" -> tcp_to_sock_filter
    | "udp" -> udp_to_sock_filter
    | "icmp6" -> icmp6_to_sock_filter
    | "sctp" -> sctp_to_sock_filter
  in
  let protocol_num =
    match protocol with
    | "icmp" -> 1
    | "tcp" -> 6
    | "udp" -> 17
    | "icmp6" -> 58
    | "sctp" -> 132
  in
  let main_sf = protocol_to_sf protocol_info (encapsulate headers protocol) in
  match !Env.protochain_list, protocol with
  | [], _
  | 1 :: _, "icmp"
  | 6 :: _, "tcp"
  | 17 :: _, "udp"
  | 58 :: _, "icmp6" -> main_sf
  | 4 :: _, protocol
  | 41 :: _, protocol ->
    let sum_of_header =
      match headers with
      | Packet (_, _, L3 Ip) | Segment (_, _, L3 Ip, _) -> sum_of_header headers "ip"
      | Packet (_, _, L3 Ip6) | Segment (_, _, L3 Ip6, _) -> sum_of_header headers "ip6"
    in
    begin
      match List.assoc_opt protocol_num !Protochain.protochain_asts with
      | None ->
        let recur = make_recur (!Config.max_rec) (Protochain.generate_recursive_func sum_of_header protocol_num) in
        Protochain.protochain_bridge_asts := (protocol_num, recur) :: !Protochain.protochain_bridge_asts;
      | _ -> ()
    end;
    let protochain_bridge =
      if List.hd !Env.protochain_list = 4 then
        gen_ip_protochain_bridge protocol_num
      else if List.hd !Env.protochain_list = 41 then
        gen_ip6_protochain_bridge protocol_num
      else
        failwith "not_supported protochain_bridge"
    in
    Env.protochain_list := protocol_num :: !Env.protochain_list;
    conjoin protochain_bridge main_sf

let rec pcaps_to_sf (stack : sock_filter option list) (temp : string list) (pcaps : string list) (headers : packet_headers) =
  if List.length pcaps = 0 then stack
  else
    match List.hd pcaps with
    | "@: False :@" -> abort_bpf_gen "wrong_syntax"
    | "!" -> pcaps_to_sf (negate_from_stack stack) [] (List.tl pcaps) headers
    | "&&" -> pcaps_to_sf (conjoin_from_stack stack) [] (List.tl pcaps) headers
    | "||" -> pcaps_to_sf (disjoin_from_stack stack) [] (List.tl pcaps) headers
    | "=" | "!=" | "==" | "<" | ">" | "<=" | ">=" -> pcaps_to_sf (eval_pcaps [] (List.rev (List.hd pcaps :: temp)) headers :: stack) [] (List.tl pcaps) headers
    | other ->
      if is_digit other || is_hex other || is_offset other || is_arith_op other then
        pcaps_to_sf stack (other :: temp) (List.tl pcaps) headers
      else
        match String.split_on_char ' ' (List.hd pcaps) with
        | ["inbound"] -> pcaps_to_sf (inbound () :: stack) [] (List.tl pcaps) headers
        | ["outbound"] -> pcaps_to_sf (outbound () :: stack) [] (List.tl pcaps) headers
        | "ether" :: ["multicast"] -> 
          Env.reset_env();
          pcaps_to_sf (ether_multicast () :: stack) [] (List.tl pcaps) (encapsulate headers "ether")
        | "ether" :: ["broadcast"] -> 
          Env.reset_env();
          pcaps_to_sf (ether_braodcast () :: stack) [] (List.tl pcaps) (encapsulate headers "ether")
        | "ether" :: "proto" :: [protocol] ->
          Env.reset_env ();
          pcaps_to_sf (ether_proto protocol headers :: stack)  [] (List.tl pcaps) (encapsulate headers "ether")
        | "ip" :: ip_info -> pcaps_to_sf (ip_to_sock_filter ip_info (encapsulate headers "ip") :: stack) [] (List.tl pcaps) (encapsulate headers "ip")
        | "arp" :: arp_info -> pcaps_to_sf (arp_to_sock_filter arp_info (encapsulate headers "arp") :: stack) [] (List.tl pcaps) (encapsulate headers "arp")
        | "rarp" :: rarp_info -> pcaps_to_sf (rarp_to_sock_filter rarp_info (encapsulate headers "rarp") :: stack) [] (List.tl pcaps) (encapsulate headers "rarp")
        | "ip6" :: ip6_info ->
          if check_ip6_frag (List.tl pcaps) then
            pcaps_to_sf (ip6_to_sock_filter ip6_info false (encapsulate headers "ip6")  :: stack) [] (List.tl pcaps) (encapsulate headers "ip6")
          else
            pcaps_to_sf (ip6_to_sock_filter ip6_info true (encapsulate headers "ip6") :: stack) [] (List.tl pcaps) (encapsulate headers "ip6")
        | "icmp" :: icmp_info ->
          let new_sf = protocol_to_sock_filter "icmp" icmp_info headers in
          pcaps_to_sf (new_sf :: stack) [] (List.tl pcaps) (encapsulate headers "icmp")
        | "icmp6" :: icmp6_info ->
          let new_sf = protocol_to_sock_filter "icmp6" icmp6_info headers in
          pcaps_to_sf (new_sf :: stack) [] (List.tl pcaps) (encapsulate headers "icmp6")
        | "tcp" :: tcp_info ->
          let new_sf = protocol_to_sock_filter "tcp" tcp_info headers in
          pcaps_to_sf (new_sf :: stack) [] (List.tl pcaps) (encapsulate headers "tcp")
        | "udp" :: udp_info ->
          let new_sf = protocol_to_sock_filter "udp" udp_info headers in
          pcaps_to_sf (new_sf :: stack) [] (List.tl pcaps) (encapsulate headers "udp")
        | "sctp":: sctp_info ->
          let new_sf = protocol_to_sock_filter "sctp" sctp_info headers in
          pcaps_to_sf (new_sf :: stack) [] (List.tl pcaps) (encapsulate headers "sctp")
        | _ -> abort_bpf_gen "unsupported feature"

let init_header (pcap : string list) : (sock_filter option list * string list) =
  let rec init_header_helper stack pcap =
    match pcap with
    | [] -> stack, pcap
    | "&&" :: rest -> init_header_helper (conjoin_from_stack stack) rest
    | "||" :: rest -> init_header_helper (disjoin_from_stack stack) rest
    | "!" :: rest -> init_header_helper (negate_from_stack stack) rest
    | head :: rest ->
      let p = String.split_on_char ' ' head in
      match p with
      | "vlan" :: "SKF_AD" :: [] ->
        base_header := encapsulate Nil "ether";
        init_header_helper (vlan_to_sock_filter ["SKF_AD"] (!base_header) :: stack) rest
      | "vlan" :: "SKF_AD_TAG" :: [skf_ad_tag] ->
        base_header := encapsulate Nil "ether";
        init_header_helper (vlan_to_sock_filter ["SKF_AD_TAG"; skf_ad_tag] (!base_header) :: stack) rest
      | "vlan" :: vlan_info ->
        base_header := encapsulate !base_header "vlan";
        init_header_helper (vlan_to_sock_filter vlan_info (!base_header) :: stack) rest
      | "mpls" :: mpls_info ->
        base_header := encapsulate !base_header "mpls";
        init_header_helper (mpls_to_sock_filter mpls_info (!base_header) :: stack) rest
      | _ -> stack, pcap
  in
  init_header_helper [] pcap

let rec pcaps_to_sftree (stack : sock_filter option list) (acc : string list) (pcaps : string list) =
  match pcaps with
  | [] -> List.hd (pcaps_to_sf stack [] (lst_rev acc) !base_header)
  | p :: ps ->
    if Str.string_match (Str.regexp "ether proto.*") p 0 && List.length acc != 0 then
      pcaps_to_sftree (pcaps_to_sf stack [] (lst_rev acc) !base_header) [p] ps
    else
      pcaps_to_sftree stack (p :: acc) ps
