(*
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Infix to postfix transformer for expanded pcap expressions
*)

open Util

let de_new_line word =
  match String.get word ((String.length word) - 1) with
  | '\n' -> String.sub word 0 ((String.length word) - 1)
  | _ -> word

let de_backslash word =
  match String.get word 0 with
  | '\\' -> String.sub word 1 ((String.length word) - 1)
  | '\t' -> "t" ^ (String.sub word 1 ((String.length word) - 1)) (* edge case for \tcp.*)
  | _ -> word

let merge acc = String.concat " " (List.rev acc)

let tokenize pcap =
  let rec tokenize_helper ret acc pcap =
    match pcap with
    | [] -> (String.concat " " (List.rev acc) :: ret)
    | "(" :: rest -> tokenize_helper ("(" :: ret) acc rest
    | ")" :: rest -> tokenize_helper (")" :: (merge acc) :: ret) [] rest
    | "-" :: rest | "+" :: rest | "*" :: rest | "/" :: rest | "<<" :: rest | ">>" :: rest | "%" :: rest
    | "=" :: rest | "==" :: rest | "!=" :: rest | "&&" :: rest | "||" :: rest |"&" :: rest | "|" :: rest
    | "<" :: rest | ">" :: rest | "<=" :: rest | ">=" :: rest | "!" :: rest ->
      tokenize_helper (List.hd pcap :: (merge acc) :: ret) [] rest
    | head :: rest ->
      if String.get head 0 == '('
      then tokenize_helper ret acc ("(" :: ((String.sub head 1 ((String.length head) - 1)) :: rest))
      else if String.get head ((String.length head) - 1) == ')'
      then tokenize_helper ret acc ((String.sub head 0 ((String.length head) - 1)) :: (")" :: rest))
      else tokenize_helper ret (head :: acc) rest
    in
  tokenize_helper [] [] pcap

let merge_square_bracket (s : string list) =
  let is_square_open (s : string) =
    Str.string_match (Str.regexp ".*\\[.*") s 0
  in
  let is_square_close (s : string) =
    Str.string_match (Str.regexp ".*\\].*") s 0
  in
  let rec count_square (_open : int) (close : int) (s : string) =
    match s with
    | "" -> _open, close
    | other ->
      if s.[0] = '[' then count_square (_open + 1) close (String.sub s 1 (String.length s - 1))
      else if s.[0] = ']' then count_square _open (close + 1) (String.sub s 1 (String.length s - 1))
      else count_square _open close (String.sub s 1 (String.length s - 1))
  in
  let rec parse_square_bracket ret block counter =
    if counter = 0 then String.concat " " (List.rev ret), block else
      match block with
      | head :: rest ->
        if is_square_open head || is_square_close head then
          let square_cnt = count_square 0 0 head in
          parse_square_bracket (head :: ret) rest (counter + (fst square_cnt) - (snd square_cnt))
        else parse_square_bracket (head :: ret) rest counter
  in
  let rec merge_square_bracket_helper ret s =
    match s with
    | [] -> List.rev ret
    | head :: rest ->
      if is_square_open head || is_square_close head then
        let square_meta = count_square 0 0 head in
        let parsed = parse_square_bracket [head] rest (fst square_meta - snd square_meta) in
        merge_square_bracket_helper (fst parsed :: ret) (snd parsed)
      else merge_square_bracket_helper (head :: ret) rest
  in
  merge_square_bracket_helper [] (lst_rev s)

let rec infix_to_postfix pcap =
  let precedence op =
    match op with
    | "!" -> 11
    | "*" | "/" | "%" | "<<" | ">>" -> 9
    | "+" | "-" -> 8
    | "<" | ">" | "<=" | ">=" -> 7
    | "=" | "==" | "!=" -> 6
    | "&" -> 5
    | "|" -> 4
    | "&&" -> 3
    | "||" -> 2
    | _ -> -1
  in
  let rec infix_to_postfix_helper ret stack pcap =
    match pcap with
    | [] -> List.rev (List.rev stack @ ret)
    | "(" :: rest -> infix_to_postfix_helper ret ("(" :: stack) rest
    | ")" :: rest ->
      if String.equal (List.hd stack) "("
      then infix_to_postfix_helper ret (List.tl stack) rest
      else infix_to_postfix_helper (List.hd stack :: ret) (List.tl stack) pcap
    | "-" :: rest | "+" :: rest | "*" :: rest | "/" :: rest | "<<" :: rest | ">>" :: rest | "%" :: rest
    | "=" :: rest | "==" :: rest | "!=" :: rest | "&&" :: rest | "||" :: rest |"&" :: rest | "|" :: rest
    | "<" :: rest | ">" :: rest | "<=" :: rest | ">=" :: rest | "!" :: rest ->
      if List.length stack  == 0
      then infix_to_postfix_helper ret ((List.hd pcap) :: stack) rest
      else
      if (precedence (List.hd stack) < precedence (List.hd pcap))
      then infix_to_postfix_helper ret ((List.hd pcap) :: stack) rest
      else infix_to_postfix_helper ((List.hd stack) :: ret) (List.tl stack) pcap
    | other :: rest -> infix_to_postfix_helper (other :: ret) stack rest
  in
infix_to_postfix_helper [] [] pcap

let pcap_infix_to_postfix (pcap : string) =
  pcap
  |> String.split_on_char ' '
  |> List.filter (fun a -> a <> "" && a <> "\n")
  |> List.map de_new_line
  |> List.map de_backslash
  |> tokenize
  |> List.filter (fun a -> a <> "" && a <> "\n")
  |> merge_square_bracket
  |> infix_to_postfix
