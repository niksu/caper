(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: vlan
*)

open Inst
open Headers
open Util

let get_vlanrange_floor (vlanrange : string) : int =
  let range = String.split_on_char '-' vlanrange in
  match range with
  | f :: c :: [] -> int_of_string f
  | _ -> abort_bpf_gen "Vlan.get_vlanrage_floor"

let get_vlanrange_ceil (vlanrange : string) : int =
  let range = String.split_on_char '-' vlanrange in
  match range with
  | f :: c :: [] -> int_of_string c
  | _ -> abort_bpf_gen "Vlan.get_vlanrage_ceil"

let vlan_to_sock_filter (vlan_info : string list) (headers : packet_headers) : sock_filter option =
  match vlan_info, headers with
  (*    [snipped from <linux/filter.h>]
    Git hash: 6f52b16c5b29b89d92c0e7236f4655dc8491ad70
    https://github.com/torvalds/linux/blob/master/include/uapi/linux/filter.h

    RATIONALE. Negative offsets are invalid in BPF.
    We use them to reference ancillary data.
    Unlike introduction new instructions, it does not break existing compilers/optimizers.

    #define SKF_AD_OFF    (-0x1000)
    #define SKF_AD_VLAN_TAG	44
    #define SKF_AD_VLAN_TAG_PRESENT 48
  *)
  | ["SKF_AD"], _ ->
    Some {
      code = [
        Ldh (Off (-4048))
      ];
      cond = Jeq (Hexj 0x1);
      jt=ret_true;
      jf=ret_false
    }

  | ["SKF_AD_TAG"; skf_ad_tag], _ ->
    let vlan_skf_ad = Some {
      code = [
        Ldh (Off (-4048))
      ];
      cond = Jeq (Hexj 0x1);
      jt=ret_true;
      jf=ret_false
    } in
    let vlan_skf_ad_tag =
      if Str.string_match (Str.regexp "[0-9]+\\-[0-9]+") skf_ad_tag 0 then
        let vlan_skf_ad_floor = Some {
          code = [
            Ldh (Off (-4052));
            And (Hexj 0xfff)
          ];
          cond = Jge (Hexj (get_vlanrange_floor skf_ad_tag));
          jt = ret_true;
          jf = ret_false;
        } in
        let vlan_skf_ad_ceil = Some {
          code = [
            Ldh (Off (-4052));
            And (Hexj 0xfff)
          ];
          cond = Jgt (Hexj (get_vlanrange_ceil skf_ad_tag));
          jt = ret_false;
          jf = ret_true;
        } in
        conjoin vlan_skf_ad_floor vlan_skf_ad_ceil
      else
        Some {
          code = [
            Ldh (Off (-4052));
            And (Hexj 0xfff)
          ];
          cond = Jeq (Hexj (int_of_string skf_ad_tag));
          jt=ret_true;
          jf=ret_false
        }
    in
    conjoin vlan_skf_ad vlan_skf_ad_tag

  | [], Frame (_, l2_5)->
    let vlan_id_8100 = Some {
      code = [
        Ldh (Off (12 + 4 * (List.length l2_5 - 1)))
      ];
      cond = Jeq (Hexj 0x8100);
      jt = ret_true;
      jf = ret_false;
    } in
    let vlan_id_88a8 = Some {
      code = [
        Ldh (Off (12 + 4 * (List.length l2_5 - 1)))
      ];
      cond = Jeq (Hexj 0x88a8);
      jt = ret_true;
      jf = ret_false;
    } in
    let vlan_id_9100 = Some {
      code = [
        Ldh (Off (12 + 4 * (List.length l2_5 - 1)))
      ];
      cond = Jeq (Hexj 0x9100);
      jt = ret_true;
      jf = ret_false;
    } in
    disjoin vlan_id_8100 (disjoin vlan_id_88a8 vlan_id_9100)

  | [vlan_info], Frame (_, l2_5) ->
    let vlan_id_8100 = Some {
      code = [
        Ldh (Off (12 + 4 * (List.length l2_5 - 1)))
      ];
      cond = Jeq (Hexj 0x8100);
      jt = ret_true;
      jf = ret_false;
    } in
    let vlan_id_88a8 = Some {
      code = [
        Ldh (Off (12 + 4 * (List.length l2_5 - 1)))
      ];
      cond = Jeq (Hexj 0x88a8);
      jt = ret_true;
      jf = ret_false;
    } in
    let vlan_id_9100 = Some {
      code = [
        Ldh (Off (12 + 4 * (List.length l2_5 - 1)))
      ];
      cond = Jeq (Hexj 0x9100);
      jt = ret_true;
      jf = ret_false;
    } in
    let vlan_val =
      if Str.string_match (Str.regexp "[0-9]+\\-[0-9]+") vlan_info 0 then
        let vlan_floor = Some {
          code = [
            Ldh (Off (14 + 4 * (List.length l2_5 - 1)));
            And (Hexj 0xfff)
          ];
          cond = Jge (Hexj (get_vlanrange_floor vlan_info));
          jt = ret_true;
          jf = ret_false;
        } in
        let vlan_ceil = Some {
          code = [
            Ldh (Off (14 + 4 * (List.length l2_5 - 1)));
            And (Hexj 0xfff)
          ];
          cond = Jgt (Hexj (get_vlanrange_ceil vlan_info));
          jt = ret_false;
          jf = ret_true;
        } in
        conjoin vlan_floor vlan_ceil
      else
        Some {
          code = [
            Ldh (Off (14 + 4 * (List.length l2_5 - 1)));
            And (Hexj 0xfff)
          ];
          cond = Jeq (Hexj (int_of_string vlan_info));
          jt = ret_true;
          jf = ret_false;
        }
    in
    conjoin (disjoin vlan_id_8100 (disjoin vlan_id_88a8 vlan_id_9100)) vlan_val
