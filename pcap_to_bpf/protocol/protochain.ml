open Headers
open Inst
open Util

let protochain_asts = ref []
let protochain_bridge_asts = ref []

let generate_recursive_func (sum_of_pred_headers : int) (protochain_num : int) =
  let protochain_var_len = Some {
    code = [
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers), Addition)));
      St (Mem 15);
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers + 1), Addition)));
      Add (Hexj 1);
      Mul (Hexj 8);
      Add X;
      Tax;
      Ld (Mem 15);
    ];
    cond = Jeq (Hexj protochain_num);
    jt = ret_true;
    jf = ret_false;
  } in
  let ah_len = Some {
    code = [
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers), Addition)));
      St (Mem 15);
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers + 1), Addition)));
      Add (Hexj 2);
      Mul (Hexj 4);
      Add X;
      Tax;
      Ld (Mem 15);
     ];
     cond = Jeq (Hexj protochain_num);
     jt = ret_true;
     jf = ret_false;
  } in
  let ip6_len = Some {
    code = [
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers + 6), Addition)));
      St (Mem 15);
      Ld (Hexj 40);
      Add X;
      Tax;
      Ld (Mem 15);
     ];
     cond = Jeq (Hexj protochain_num);
     jt = ret_true;
     jf = ret_false;
  } in
  let ip_len = Some {
    code = [
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers + 9), Addition)));
      St (Mem 15);
      Ldb (Offset (Exp (X, Lit (sum_of_pred_headers), Addition)));
      And (Hexj 0xf);
      Mul (Hexj 4);
      Add X;
      Tax;
      Ld (Mem 15);
     ];
     cond = Jeq (Hexj protochain_num);
     jt = ret_true;
     jf = ret_false;
  } in
  let ext_hop_by_hop = Some {code = []; cond = Jeq(Hexj 0x0); jt = ret_true; jf = ret_false;} in
  let ext_dest = Some {code = []; cond = Jeq(Hexj 0x3c); jt = ret_true; jf = ret_false;} in
  let ext_routing = Some {code = []; cond = Jeq(Hexj 0x2b); jt = ret_true; jf = ret_false;} in
  let ext_frag = Some { code = []; cond = Jeq (Hexj 0x2c); jt = ret_true; jf = ret_false;} in
  let ext_ah = Some {code = []; cond = Jeq(Hexj 0x33); jt = ret_true; jf = ret_false;} in
  let ext_ip6 = Some {code = []; cond = Jeq(Hexj 0x29); jt = ret_true; jf = ret_false;} in
  let ext_ip = Some {code = []; cond = Jeq(Hexj 0x4); jt = ret_true; jf = ret_false;} in
  let next_header =
    ext_frag
    |> disjoin ext_routing
    |> disjoin ext_dest
    |> disjoin ext_hop_by_hop
  in
  let exts = conjoin next_header protochain_var_len in
  let ip_ah = conjoin ext_ah ah_len in
  let ip_ip = conjoin ext_ip ip_len in
  let ip_ip6 = conjoin ext_ip6 ip6_len in
  exts
  |> disjoin ip_ah
  |> disjoin ip_ip
  |> disjoin ip_ip6

let protochain (l3 : string) (_protochain_num : string) (headers : packet_headers) : sock_filter option =
  let protochain_num = int_of_string _protochain_num in
  let sum_of_pred_headers = sum_of_header headers l3 in
  let init_protochain =
    match headers with
    | Packet (_, _, L3 Ip) | Segment (_, _, L3 Ip, _)->
      Some {
        code = [
          Ldb (Off (sum_of_pred_headers + 9));
          Ldxb (Exp (Lit 4, (Exp (Off (sum_of_header headers "ip"), Hex 0xf, Arith_and)), Multiplication));
        ];
        cond = Jeq (Hexj protochain_num);
        jt = ret_true;
        jf = ret_false
      }
    | Packet (_, _, L3 Ip6) | Segment (_, _, L3 Ip6, _)->
      Some {
        code = [
         Ldx (Hexj 40);
         Ldb (Off (sum_of_pred_headers + 6));
        ];
        cond = Jeq (Hexj protochain_num);
        jt = ret_true;
        jf = ret_false
      }
  in
  begin
    if !Config.max_rec > 0 then
      match List.assoc_opt protochain_num !protochain_asts with
      | None ->
        let recur = make_recur (!Config.max_rec) (generate_recursive_func sum_of_pred_headers protochain_num) in
        protochain_asts := (protochain_num, disjoin init_protochain recur) :: !protochain_asts;
        protochain_bridge_asts := (protochain_num, recur) :: !protochain_bridge_asts
      | _ -> ()
    else
      abort_bpf_gen "protochain must be used with -max_rec [n], n must be greater than 0"
  end;
  match headers with
  | Packet (_, _, L3 Ip) | Segment (_, _, L3 Ip, _)->
    gen_ip_protochain protochain_num
  | Packet (_, _, L3 Ip6) | Segment (_, _, L3 Ip6, _)->
    gen_ip6_protochain protochain_num