(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: icmp protocol
*)

open Inst
open Util
open Headers

let is_icmp_type = function
| "icmp-echoreply"
| "icmp-unreach"
| "icmp-sourcequench"
| "icmp-redirect"
| "icmp-echo"
| "icmp-routeradvert"
| "icmp-routersolicit"
| "icmp-timxceed"
| "icmp-paramprob"
| "icmp-tstamp"
| "icmp-tstampreply"
| "icmp-ireq"
| "icmp-ireqreply"
| "icmp-maskreq"
| "icmp-maskreply" -> true
| _ -> false

let icmp_type_to_opcode = function
| "icmp-echoreply" -> Num 0
| "icmp-unreach" -> Num 3
| "icmp-sourcequench" -> Num 4
| "icmp-redirect" -> Num 5
| "icmp-echo" -> Num 8
| "icmp-routeradvert" -> Num 9
| "icmp-routersolicit" -> Num 10
| "icmp-timxceed" -> Num 11
| "icmp-paramprob" -> Num 12
| "icmp-tstamp" -> Num 13
| "icmp-tstampreply" -> Num 14
| "icmp-ireq" -> Num 15
| "icmp-ireqreply" -> Num 16
| "icmp-maskreq" -> Num 17
| "icmp-maskreply" -> Num 18
| _ -> failwith "Icmp.icmp_type_to_opcode"

let icmp_protochain (protochain_num : string) (headers : packet_headers) =
  let redirect = Some {
    code = [
      Ldxb (Exp (Lit 4, (Exp (Off (sum_of_header headers "ip"), Hex 0xf, Arith_and)), Multiplication));
      Ldb (Offset (Exp (X, Lit 14, Addition)));
    ];
    cond = Jeq (Hexj 0x5);
    jt = ret_true;
    jf = ret_false;
  } in
  let source_quench = Some {code = []; cond = Jeq (Hexj 0x4); jt = ret_true; jf = ret_false} in
  let time_exceeded = Some {code = []; cond = Jeq (Hexj 0xb); jt = ret_true; jf = ret_false} in
  let dst_unreachable = Some {code = []; cond = Jeq (Hexj 0x3); jt = ret_true; jf = ret_false} in
  let param_prob = Some {code = []; cond = Jeq (Hexj 12); jt = ret_true; jf = ret_false} in
  let icmp_with_ip_header =
    dst_unreachable
    (* |> disjoin source_quench Deprecated in 1995 by RFC 1812 *)
    |> disjoin param_prob
    |> disjoin time_exceeded
    |> disjoin redirect
  in
  let next_header = Some {
    code = [
      Ldxb (Exp (Lit 4, (Exp (Off (sum_of_header headers "ip"), Hex 0xf, Arith_and)), Multiplication));
      Ldb (Offset (Exp (X, Lit (14 + 1 (* type*) + 1 (* code *) + 2 (* checksum *) + 4 (* unused*) + 9 (* next_field of ip header *)), Addition)));
    ];
    cond = Jeq (Hexj (int_of_string protochain_num));
    jt = ret_true;
    jf = ret_false;
  } in
  conjoin icmp_with_ip_header next_header

let icmp_to_sock_filter icmp_info headers =
  match icmp_info with
  | "protochain" :: [protochain_num] -> icmp_protochain protochain_num headers
