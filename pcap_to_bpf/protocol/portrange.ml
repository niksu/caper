(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: portrange
*)

open Inst
open Inst
open Util
open Ip6
open Headers

let portrange_floor (portrange : string) : int =
  let range = String.split_on_char '-' portrange in
  match range with
  | f :: c :: [] -> int_of_string f
  | _ -> abort_bpf_gen "Portrange.portrange_floor"

let portrange_ceil (portrange : string) : int =
  let range = String.split_on_char '-' portrange in
  match range with
  | f :: c :: [] -> int_of_string c
  | _ -> abort_bpf_gen "Portrange.portrange_ceil"

let src_portrange (protocol : string) (portrange : string) (headers : packet_headers) : sock_filter option =
  let floor_port = portrange_floor portrange in
  let ceil_port = portrange_ceil portrange in
  match headers with
  | Packet (_, _, L3 Ip6) | Segment (_, _, L3 Ip6, _) ->
    let floor  =
      Some {
        code = [
          Ldh (Off (sum_of_header headers protocol));
        ];
        cond = Jge (Hexj (floor_port));
        jt = ret_true;
        jf = ret_false;
      }
    in
    let ceil =
      Some {
        code = [
          Ldh (Off (sum_of_header headers protocol))
        ];
        cond = Jgt (Hexj (ceil_port));
        jt = ret_false;
        jf = ret_true;
      }
    in
    conjoin floor ceil

  | Packet (_, _, L3 Ip) | Segment (_, _, L3 Ip, _) ->
    let ipv4_frag =
      Some {
        code = [
          Ldh (Off (sum_of_header headers "ip" + 6))
        ];
        cond = Jset (Hexj 0x1fff);
        jt = ret_false;
        jf = ret_true;
      }
    in
    let floor =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication ));
          Ldh (Offset (Exp (X, Lit (sum_of_header headers "ip"), Addition)))
        ];
        cond = Jge (Hexj (floor_port));
        jt = ret_true;
        jf = ret_false;
      }
    in
    let ceil =
      Some {
        code = [
          Ldxb (Exp (Lit 4, Exp ((Off (sum_of_header headers "ip")), Hex 0xf, Arith_and), Multiplication ));
          Ldh (Offset (Exp (X, Lit (sum_of_header headers "ip"), Addition)))];
        cond = Jgt (Hexj (ceil_port));
        jt = ret_false;
        jf = ret_true;
      }
    in
    conjoin ipv4_frag (conjoin floor ceil)

  | _ -> abort_bpf_gen "Portrange.src_portrange"

let dst_portrange (protocol : string) (portrange : string) (headers : packet_headers) : sock_filter option =
  let floor_port = portrange_floor portrange in
  let ceil_port = portrange_ceil portrange in
  match headers with
  | Packet (_, _, L3 Ip6) | Segment (_, _, L3 Ip6, _) ->
    let floor =
      Some {
        code = [
          Ldh (Off (sum_of_header headers protocol + 2))
        ];
        cond = Jge (Hexj (floor_port));
        jt = ret_true;
        jf = ret_false;
      }
    in
    let ceil =
      Some {
        code = [
          Ldh (Off (sum_of_header headers protocol + 2))
        ];
        cond = Jgt (Hexj (ceil_port));
        jt = ret_false;
        jf = ret_true;
      }
    in
    conjoin floor ceil

  | Packet (_, _, L3 Ip) | Segment (_, _, L3 Ip, _) ->
    let ipv4_frag =
      Some {
        code = [
          Ldh (Off (sum_of_header headers "ip" + 6))
        ];
        cond = Jset (Hexj 0x1fff);
        jt = ret_false;
        jf = ret_true;
      }
    in
    let floor =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication));
          Ldh (Offset (Exp (X, (Lit (sum_of_header headers "ip" + 2)), Addition)))
        ];
        cond = Jge (Hexj (floor_port));
        jt = ret_true;
        jf = ret_false;
      } in
    let ceil =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication ));
          Ldh (Offset (Exp (X, (Lit (sum_of_header headers "ip" + 2)), Addition)))
        ];
        cond = Jgt (Hexj (ceil_port));
        jt = ret_false;
        jf = ret_true;
      }
    in
    conjoin ipv4_frag (conjoin floor ceil)
  | _ -> abort_bpf_gen "Portrange.dst_portrange"

let src_dst_portrange (bind : string) (protocol : string) (portrange : string) (headers : packet_headers) : sock_filter option =
  let floor_port = portrange_floor portrange in
  let ceil_port = portrange_ceil portrange in
  match headers with
  | Packet (_, _, L3 Ip6) | Segment (_, _, L3 Ip6, _) ->
    if bind = "and" then conjoin (src_portrange protocol portrange headers) (dst_portrange protocol portrange headers)
    else if bind = "or" then disjoin (src_portrange protocol portrange headers) (dst_portrange protocol portrange headers)
    else abort_bpf_gen "Portrange.src_dst_portrange"

  | Packet (_, _, L3 Ip) | Segment (_, _, L3 Ip, _) ->
    let ipv4_frag =
      Some {
        code = [
          Ldh (Off (sum_of_header headers "ip" + 6))
        ];
        cond = Jset (Hexj 0x1fff);
        jt = ret_false;
        jf = ret_true;
      }
    in
    let dst_floor =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication));
          Ldh (Offset (Exp (X, (Lit (sum_of_header headers "ip" + 2)), Addition)))
        ];
        cond = Jge (Hexj (floor_port));
        jt = ret_true;
        jf = ret_false;
      } in
    let dst_ceil =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication ));
          Ldh (Offset (Exp (X, (Lit (sum_of_header headers "ip" + 2)), Addition)))
        ];
        cond = Jgt (Hexj (ceil_port));
        jt = ret_false;
        jf = ret_true;
      }
    in
    let src_floor =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication ));
          Ldh (Offset (Exp (X, (Lit (sum_of_header headers "ip")), Addition)))
        ];
        cond = Jge (Hexj (floor_port));
        jt = ret_true;
        jf = ret_false;
      }
    in
    let src_ceil =
      Some {
        code = [
          Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header headers "ip")), (Hex 0xf), Arith_and)), Multiplication ));
          Ldh (Offset (Exp (X, (Lit (sum_of_header headers "ip")), Addition)))];
        cond = Jgt (Hexj (ceil_port));
        jt = ret_false;
        jf = ret_true;
      }
    in
    if bind = "and" then conjoin ipv4_frag (conjoin (conjoin src_floor src_ceil) (conjoin dst_floor dst_ceil))
    else if bind = "or" then conjoin ipv4_frag (disjoin (conjoin src_floor src_ceil) (conjoin dst_floor dst_ceil))
    else abort_bpf_gen "Portrange.src_dst_portrange"
  | _ -> abort_bpf_gen "Portrange.src_dst_portrange"
