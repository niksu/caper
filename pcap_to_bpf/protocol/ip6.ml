(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: ip6 protocol
*)

open Inst
open Util
open Host
open Net
open Headers
open Protochain

let ipv6_type =  function
  | "ip" -> 0x0
  | "icmp6" -> 0x3a
  | "tcp" -> 0x6
  | "udp" -> 0x11
  | "sctp" -> 0x84
  | _ -> failwith "Ip6.ipv6_type"

let ip6_frag_proto (protocol : string) (headers : packet_headers) : sock_filter option =
  let sum_of_pred_headers = sum_of_header headers "ip6" in
  let ip6 = Some {
    code = [
      Ldb (Off (sum_of_pred_headers + 6))
    ];
    cond = Jeq (Hexj (ipv6_type protocol));
    jt = ret_true;
    jf = ret_false;
  } in
  let ip6_frag1 = Some {
    code = [
      Ldb (Off (sum_of_pred_headers + 6))
    ];
    cond = Jeq (Hexj 0x2c);
    jt = ret_true;
    jf = ret_false;
  } in
  let ip6_frag2 = Some {
    code = [
      Ldb (Off (sum_of_pred_headers + 40))
    ];
    cond = Jeq (Hexj (ipv6_type protocol));
    jt = ret_true;
    jf = ret_false;
  } in
  disjoin ip6 (conjoin ip6_frag1 ip6_frag2)

let ip6_proto (protocol : string) (headers : packet_headers) : sock_filter option =
  let sum_of_pred_headers = sum_of_header headers "ip6" in
  Some {
    code = [
      Ldb (Off (sum_of_pred_headers + 6))
    ];
    cond = Jeq (Hexj (ipv6_type protocol));
    jt = ret_true;
    jf = ret_false;
  }

let extend_ipv6 (ipv6 : string) =
  let parts = String.split_on_char ':' ipv6 in
  let full_parts, has_empty =
    List.fold_left (fun (acc, found_empty) part ->
      if part = "" then
        if found_empty then
          ("0000" :: acc, found_empty)
        else
          let fill = List.init (8 - List.length parts + 1) (fun _ -> "0000") in
          (fill @ acc, true)
      else (Printf.sprintf "%04x" (int_of_string ("0x" ^ part)) :: acc, found_empty)
    ) ([], false) parts
  in
  String.concat ":" (List.rev full_parts)

(*flag is true if ipv6 protocol still have furthre L4 to deal with*)
let ip6_to_sock_filter (ip6_info : string list) (frag : bool) (headers : packet_headers) : sock_filter option =
  match ip6_info with
  | "proto" :: [protocol] -> if frag then ip6_frag_proto protocol headers else ip6_proto protocol headers
  | "src" :: "and" :: "dst" :: "host" :: [host] -> conjoin (src_host "ip6" host headers) (dst_host "ip6" host headers)
  | "src" :: "or" :: "dst" :: "host" :: [host] -> disjoin (src_host "ip6" host headers) (dst_host "ip6" host headers)
  | "src" :: "and" :: "dst" :: "net" :: [net] -> conjoin (src_net "ip6" net headers) (dst_net "ip6" net headers)
  | "src" :: "or" :: "dst" :: "net" :: [net] -> disjoin (src_net "ip6" net headers) (dst_net "ip6" net headers)
  | "src" :: "host" :: [host] -> src_host "ip6" (extend_ipv6 host) headers
  | "dst" :: "host" :: [host] -> dst_host "ip6" (extend_ipv6 host) headers
  | "src" :: "net" :: [net] -> dst_host "ip6" net headers
  | "dst" :: "net" :: [net] -> dst_host "ip6" net headers
  | "protochain" :: [protochain_num] ->
    Env.generate_protochain := true;
    Env.protochain_list := (int_of_string protochain_num :: !Env.protochain_list);
    protochain "ip6" protochain_num headers
  | _ -> abort_bpf_gen "unsupported ip6 feature"
