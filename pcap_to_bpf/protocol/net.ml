(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: net
*)

open Inst
open Util
open Headers

let ipv6_src_net net mask headers =
  let rec ipv6_src_net_helper ret cnt net mask : sock_filter option =
    if mask = 0 then ret
    else if mask >= 32 then
      let ipv6_piece = Some {
        code = [
          Ld (Off (sum_of_header headers "ip6" + src_ip_offset "ip6" + (cnt * 4)))
        ];
        cond = Jeq (Hexj (ipv6_piece_to_lit net cnt));
        jt = ret_true;
        jf = ret_false;
      } in
      ipv6_src_net_helper (conjoin ret ipv6_piece) (cnt + 1) net (mask - 32)
    else if mask < 32 && (mask mod 4) = 0 then
      let ipv6_piece = Some {
        code = [
          Ld (Off (sum_of_header headers "ip6" + src_ip_offset "ip6" + (cnt * 4)));
          And (Hexj (ipv6_mask mask))];
        cond = Jeq (Hexj (Int.logand (ipv6_piece_to_lit net cnt) (ipv6_mask mask)));
        jt = ret_true;
        jf = ret_false;
      } in
      ipv6_src_net_helper (conjoin ret ipv6_piece) (cnt + 1) net 0
    else
      abort_bpf_gen (Printf.sprintf "net.ipv6_net, mask : %d" mask)
  in
  ipv6_src_net_helper nil_sock_filter 0 net mask

let ipv6_dst_net net mask headers =
  let rec ipv6_dst_net_helper ret cnt net mask : sock_filter option =
    if mask = 0 then ret
    else if mask >= 32 then
      let ipv6_piece = Some {
        code = [
          Ld (Off (sum_of_header headers "ip6" + dst_ip_offset "ip6" + (cnt * 4)))
        ];
        cond = Jeq (Hexj (ipv6_piece_to_lit net cnt));
        jt = ret_true;
        jf = ret_false;
      } in
      ipv6_dst_net_helper (conjoin ret ipv6_piece) (cnt + 1) net (mask - 32)
    else if mask < 32 && (mask mod 4) = 0 then
      let ipv6_piece = Some {
        code = [
          Ld (Off (sum_of_header headers "ip6" + dst_ip_offset "ip6" + (cnt * 4)));
          And (Hexj (ipv6_mask mask))];
        cond = Jeq (Hexj (Int.logand (ipv6_piece_to_lit net cnt) (ipv6_mask mask)));
        jt = ret_true;
        jf = ret_false;
      } in
      ipv6_dst_net_helper (conjoin ret ipv6_piece) (cnt + 1) net 0
    else
      abort_bpf_gen (Printf.sprintf "net.ipv6_net, mask : %d" mask)
  in
  ipv6_dst_net_helper nil_sock_filter 0 net mask

let src_net (protocol : string) (net : string) (headers : packet_headers) : sock_filter option =
  match protocol with
  | "ip" | "arp" | "rarp" ->
    let _net = String.split_on_char '.' net in
    Some {
      code = [
        Ld (Off (sum_of_header headers protocol + src_ip_offset protocol));
        And (Hexj (ipv4_mask _net))
      ];
      cond = Jeq (Hexj (ipv4_to_lit _net));
      jt = ret_true;
      jf = ret_false;
    }
  | "ip6" ->
    begin
      match String.split_on_char '/' net with
      | net' :: [mask] ->
        let mask' = int_of_string mask in
        let net'' = String.split_on_char ':' net' |> (List.filter (fun a -> a <> "")) |> ipv6_pad in
        ipv6_src_net net'' mask' headers
    end

let dst_net (protocol : string) (net : string) (headers : packet_headers) : sock_filter option =
  let _net = String.split_on_char '.' net in
  match protocol with
  | "ip" | "arp" | "rarp" ->
    let _net = String.split_on_char '.' net in
    Some {
      code = [
        Ld (Off (sum_of_header headers protocol + dst_ip_offset protocol));
        And (Hexj (ipv4_mask _net))
      ];
      cond = Jeq (Hexj (ipv4_to_lit _net));
      jt = ret_true;
      jf = ret_false;
    }
  | "ip6" ->
    begin
      match String.split_on_char '/' net with
      | net' :: [mask] ->
        let mask' = int_of_string mask in
        let net'' = String.split_on_char ':' net' |> (List.filter (fun a -> a <> "")) |> ipv6_pad in
        ipv6_dst_net net'' mask' headers
    end
