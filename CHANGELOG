v0.1-dev Dec 2018
+ Initial version

v0.2-dev Oct 2022
+ Added test suite for disambiguation (including expression expansion).
+ Emitting name and version number in error output.
+ Started CHANGELOG

v0.3-dev Dec 2022
+ Added further tests, and applied fixes to have more of thos tests pass.
+ New feature: generating HTML output using "-HTML" flag.
+ Change "prerelease" version to "dev", to cycle up to alpha -> beta -> release

v0.4-dev Feb 2023
+ New feature: pseudonumisation of pcap expressions using "-pseudonymise" flag.
+ Improvement: run_test.sh script simplifies running all tests and analysing their results.
+ Improvement: pretty-printing of ASCII and HTML output when using the "-p" flag.
+ Bug fixes: conflict detector, contradiction filter.
+ Adds more tests.
+ Adds Dockerfile to build and run Caper.

v0.5-dev April 2023
+ Bug fix: meet computation.
+ Added reordering of expressions according to layers, to improve readability.
+ Improved readability through distribution and "undistribution" transformations.
+ Improved pretty printing by removing redundant brackets.
+ Added idempotence test suite to check if Caper is converging stably to the same expression.
+ Added test suite or pretty printing.
+ Added timing to tests.
+ Bug fix: presentation and processing of IPv6 addresses.
+ Improvement to compilation script to add OPAM environment variables.
+ Fixed several compiler warnings.

v0.6-dev May 2023
+ New feature: converting from/to English and pcap expressions.
+ New feature: BPF code generation.
+ Reworked handling of negation.
+ Added stronger subsumption elimination.
+ Bug fix: Unsound simplification.
+ Other minor bug fixes.

v0.7-dev June 2023
+ Improvement: BPF generation now supports 'not'.
+ Improvement: BPF output pretty-printing.
+ Bug fixes: expansion of portrange.
+ Improvement: removed redundant newline at start of -p output.
+ Improvement: more aggressive inference to simplify formulas.
+ Improvement: use of legacy protocols now results in earlier failure since they're not currently supported.
+ Improvement: additional tests.

v0.8-dev September 2023
+ Improvement: BPF generation includes extensive extensions and testing, and supports '-s' parameter matching tcpdump's behaviour.
+ Improvement: pcap<->English conversion supports 'not' and the '-e' parameter.
+ Improvement: HTML generation for English output uses clearer mark-up.
+ Bug fix: expansion of direction-only expressions (e.g., "src") no longer returns malformed expressions.
+ Bug fix: expansion of 'multicast' and 'broadcast' now match implicit behaviour in libpcap.
+ Improvement to README.md: more usage examples and updated description.
+ Improvement: additional tests.
+ Improvement to regression script: colour output for quicker confirmation of success/failure.

v0.9 October 2023
+ Graduating to non-dev versions by default, since system is as stable and tested as we can make it.
+ Improvement: added inbound/outbound as keywords.
+ Improvement: pcap<->English: added handling of inbound/outbound.
+ Improvement: BPF generation: added handling of inbound/outbound.
+ Improvement: documentation.
+ Improvement: added test cases.

v1.0 May 2024
+ Graduating to v1.0 after 5+ years of development.
+ Improvement: pcap<->English: improved referencing of header fields.
+ Improvement: added test cases.
+ Improvement: refining scripts.
+ Improvement: stricter parsing.
+ Improvement: adding support for providing parameters as hex values.
+ Bug fix: rejecting negative numbers as input.
+ Bug fix: IPv6 address parsing used by the BPF generator.
+ Bug fix: removed more sources of singleton or empty conjuncts.
+ Improvement: package generation for OPAM, Nix, Arch, and Debian.
