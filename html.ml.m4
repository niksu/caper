(*
  Copyright Hyunsuk Bang, December 2022
  Copyright Nik Sultana, December 2022
  Copyright Marelle Leon, February 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: HTML marking-up of output
*)

let make_paragraph psg = "<p>" ^ psg ^ "</p>";;
let append_new_line line = line ^ " <br/>";;
let make_bold word = "<b>" ^ word ^ "</b>";;
let space_out s = " &nbsp; " ^ s ^ " &nbsp; ";;
let html_entity word =
  match word with
  | "||" -> space_out "||"
  | "&&" -> space_out "&amp;&amp;"
  | "&" -> "&amp;"
  | "<<" -> "&lt;&lt;"
  | "<" -> "&lt;"
  | "<=" -> "&lt;="
  | ">>" -> "&gt;&gt;"
  | ">" -> "&gt;"
  | ">=" -> "&gt;="
  | _ -> word

let is_field_keyword (is_engl : bool) (word : string) =
ifdef(`CAPER_WITH_ENGLISH',`  if is_engl then
    let keywords =
      List.map fst English_to_pcap_spec.e_to_p_typs
        @ List.map fst English_to_pcap_spec.e_to_p_protos
        @ List.map fst English_to_pcap_spec.e_to_p_dirs
      |> List.map (String.split_on_char (String.get " " 0)(*NOTE avoiding use of single quotes to avoid confusing M4*))
      |> List.concat
    in
    List.find_opt (( = ) word) keywords
    |> Option.is_some
  else',`')
    (* FIXME instead of hardcoding the formatting, reference classes/style to draw formatting from elsewhere, making the formatting parametric*)
    match word with
    | "host" | "net" | "port" | "portrange" | "proto"
    | "ether"| "vlan" | "mlps" | "ip" | "ip6" | "arp" | "rarp" | "icmp" | "tcp" | "udp" | "sctp" | "scp" | "icmp6" 
    | "src" | "dst" -> true
    | _ -> false


let make_keyword_bold (is_engl : bool) (word : string) =
  match word with
  | _ when is_field_keyword is_engl word -> make_bold (html_entity word)
  | "and" | "or" | "not" -> make_bold (html_entity word)
  | "&&" | "||" | "&" | "|"
  | "+" | "-" | "*" | "/" | "%"
  | "<" | "<<" | ">" | ">>" | ">=" | "<="
  | "=" | "!=" | "==" -> html_entity word (* NOTE symbolic operators are not made bold, to reduce clutter *)
  | _ -> word

let rec backslash (is_engl : bool) (word : string) =
  if String.length word = 0 then "" (* account for empty lines *)
  else
    match String.get word 0 with
    | '\\' -> "\\" ^ (make_keyword_bold is_engl (String.sub word 1 ((String.length word) - 1)))
    | '\t' -> backslash is_engl ("\\t" ^ (String.sub word 1 ((String.length word) - 1))) (* edge case for \tcp *)
    | _ -> make_keyword_bold is_engl word

let punctuation =
  let rec interleave w is_punct rest =
    if String.length rest = 0 then w :: []
    else
      let first_char_s = (String.sub rest 0 1) in
      let rest' = (String.sub rest 1 ((String.length rest) - 1)) in
      match String.get rest 0 with
      | '[' | ']' | '(' | ')' | ',' -> 
        if is_punct then 
          interleave (w ^ first_char_s) true rest'
        else
          w :: interleave first_char_s true rest'
      | _ ->
        if is_punct then 
          w :: interleave first_char_s false rest'
        else
          interleave (w ^ first_char_s) false rest'
  in 
    interleave "" false 

(* replace prefixed spaces with indent *)
let indent line =
  let line_len = String.length line in
  let rec space_count ct =
    if (line_len > ct && line.[ct] = ' ')
    then space_count (ct + 1)
    else ct in
  let ct = space_count 0 in
  let space = "&nbsp; " in
  let space_len = String.length space in
  String.init
    (ct * space_len)
    (fun i -> space.[i mod space_len])
   ^ String.sub line ct (line_len - ct)

let process_line is_engl line =
  line
  |> String.split_on_char '\n'
  |> List.map indent
  |> String.concat " <br/> "
  |> String.split_on_char ' '
  |> List.map punctuation
  |> List.map (List.map (backslash is_engl))
  |> List.map (String.concat "")
  |> String.concat " "
(* FIXME consider re-enabling the following, based on expectations of downstream usage:
  |> make_paragraph
  |> append_new_line
*)
