#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for disambiguation.
# Nik Sultana, October 2022
#
# Based on tests/disambiguate.sh

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 3 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "U1" "arp[0] = 0" "ether proto \arp && arp[0] = 0"
tst "U2" "atalk[0] = 0" "atalk[0] = 0" # TODO add expected result
tst "U3" "carp[0] = 0" "carp[0] = 0" # TODO add expected result
tst "U4" "decnet[0] = 0" "decnet[0] = 0" # TODO add expected result
tst "U5" "ether[0] = 0" "ether[0] = 0"
tst "U6" "fddi[0] = 0" "fddi[0] = 0" # TODO add expected result
tst "U7" "icmp[0] = 0" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
tst "U8" "icmp6[0] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[0] = 0"
tst "U9" "igmp[0] = 0" "igrp[0] = 0" # TODO add expected result
tst "U10" "igrp[0] = 0" "igrp[0] = 0" # TODO add expected result
tst "U11" "ip[0] = 0" "ether proto \ip && ip[0] = 0"
tst "U12" "ip6[0] = 0" "ether proto \ip6 && ip6[0] = 0"
tst "U13" "lat[0] = 0" "lat[0] = 0" # TODO add expected result
tst "U14" "link[0] = 0" "link[0] = 0" # TODO add expected result
tst "U15" "mopdl[0] = 0" "mopdl[0] = 0" # TODO add expected result
tst "U16" "moprc[0] = 0" "moprc[0] = 0" # TODO add expected result
tst "U17" "pim[0] = 0" "pim[0] = 0" # TODO add expected result
tst "U18" "ppp[0] = 0" "ppp[0] = 0" # TODO add expected result
tst "U19" "radio[0] = 0" "radio[0] = 0" # TODO add expected result
tst "U20" "rarp[0] = 0" "rarp[0] = 0" # TODO add expected result
tst "U21" "sca[0] = 0" "sca[0] = 0" # TODO add expected result
tst "U22" "sctp[0] = 0" "sctp[0] = 0" # TODO add expected result
tst "U23" "slip[0] = 0" "slip[0] = 0" # TODO add expected result
tst "U24" "tcp[0] = 0" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0] = 0"
tst "U25" "tcp[0:2] = 0" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0 : 2] = 0"
tst "U26" "tcp[0:2] == 0" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0 : 2] = 0"
tst "U27" "tr[0] = 0" "tr[0] = 0" # TODO add expected result
tst "U28" "udp[0] = 0" "((ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)) && udp[0] = 0"
#tst "U29" "vrrp[0] = 0" "vrrp[0] = 0" # TODO add expected result
#tst "U30" "wlan[0] = 0" "wlan[0] = 0" # TODO add expected result

# FIXME This should also generate expression alternatives involving IPv6.
# NOTE these tests are for -n; the default uses names.
tst "U31" "tcp[tcpflags] = tcp-fin" "(ether proto \ip && ip proto \tcp && tcp[13] = 0x01) || (ether proto \ip6 && ip6 proto \tcp && tcp[13] = 0x01)"
tst "U32" "tcp[tcpflags] = tcp-syn" "(ether proto \ip && ip proto \tcp && tcp[13] = 0x02) || (ether proto \ip6 && ip6 proto \tcp && tcp[13] = 0x02)"
tst "U33" "tcp[tcpflags] = tcp-rst" "(ether proto \ip && ip proto \tcp && tcp[13] = 0x04) || (ether proto \ip6 && ip6 proto \tcp && tcp[13] = 0x04)"
tst "U34" "tcp[tcpflags] = tcp-push" "(ether proto \ip && ip proto \tcp && tcp[13] = 0x08) || (ether proto \ip6 && ip6 proto \tcp && tcp[13] = 0x08)"
tst "U35" "tcp[tcpflags] = tcp-ack" "(ether proto \ip && ip proto \tcp && tcp[13] = 0x10) || (ether proto \ip6 && ip6 proto \tcp && tcp[13] = 0x10)"
tst "U36" "tcp[tcpflags] = tcp-urg" "(ether proto \ip && ip proto \tcp && tcp[13] = 0x20) || (ether proto \ip6 && ip6 proto \tcp && tcp[13] = 0x20)"
tst "U37" "tcp[tcpflags] = tcp-ece" "(ether proto \ip && ip proto \tcp && tcp[tcpflags] = tcp-ece) || (ether proto \ip6 && ip6 proto \tcp && tcp[tcpflags] = tcp-ece)"
tst "U38" "tcp[tcpflags] = tcp-cwr" "(ether proto \ip && ip proto \tcp && tcp[tcpflags] = tcp-cwr) || (ether proto \ip6 && ip6 proto \tcp && tcp[tcpflags] = tcp-cwr)"
tst "U39" "icmp[icmptype] = 0" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
tst "U40" "icmp[icmpcode] = 0" "ether proto \ip && ip proto \icmp && icmp[1] = 0"

tst "U41" "icmp[icmptype] = icmp-echoreply" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
tst "U42" "icmp[icmptype] = icmp-unreach" "ether proto \ip && ip proto \icmp && icmp[0] = 3"
tst "U43" "icmp[icmptype] = icmp-sourcequench" "ether proto \ip && ip proto \icmp && icmp[0] = 4"
tst "U44" "icmp[icmptype] = icmp-redirect" "ether proto \ip && ip proto \icmp && icmp[0] = 5"
tst "U45" "icmp[icmptype] = icmp-echo" "ether proto \ip && ip proto \icmp && icmp[0] = 8"
tst "U46" "icmp[icmptype] = icmp-routeradvert" "ether proto \ip && ip proto \icmp && icmp[0] = 9"
tst "U47" "icmp[icmptype] = icmp-routersolicit" "ether proto \ip && ip proto \icmp && icmp[0] = 10"
tst "U48" "icmp[icmptype] = icmp-timxceed" "ether proto \ip && ip proto \icmp && icmp[0] = 11"
tst "U49" "icmp[icmptype] = icmp-paramprob" "ether proto \ip && ip proto \icmp && icmp[0] = 12"
tst "U50" "icmp[icmptype] = icmp-tstamp" "ether proto \ip && ip proto \icmp && icmp[0] = 13"
tst "U51" "icmp[icmptype] = icmp-tstampreply" "ether proto \ip && ip proto \icmp && icmp[0] = 14"
tst "U52" "icmp[icmptype] = icmp-ireq" "ether proto \ip && ip proto \icmp && icmp[0] = 15"
tst "U53" "icmp[icmptype] = icmp-ireqreply" "ether proto \ip && ip proto \icmp && icmp[0] = 16"
tst "U54" "icmp[icmptype] = icmp-maskreq" "ether proto \ip && ip proto \icmp && icmp[0] = 17"
tst "U55" "icmp[icmptype] = icmp-maskreply" "ether proto \ip && ip proto \icmp && icmp[0] = 18"

tst "U56" "icmp6[icmp6type] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 0"
tst "U57" "icmp6[icmp6code] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6code] = 0"

tst "U58" "icmp6[icmp6type] = icmp6-destinationunreach" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 1"
tst "U59" "icmp6[icmp6type] = icmp6-packettoobig" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 2"
tst "U60" "icmp6[icmp6type] = icmp6-timeexceeded" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 3"
tst "U61" "icmp6[icmp6type] = icmp6-parameterproblem" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 4"
tst "U62" "icmp6[icmp6type] = icmp6-echo" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 128"
tst "U63" "icmp6[icmp6type] = icmp6-echoreply" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 129"
tst "U64" "icmp6[icmp6type] = icmp6-multicastlistenerquery" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 130"
tst "U65" "icmp6[icmp6type] = icmp6-multicastlistenerreportv1" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 131"
tst "U66" "icmp6[icmp6type] = icmp6-multicastlistenerdone" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 132"
tst "U67" "icmp6[icmp6type] = icmp6-routersolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 133"
tst "U68" "icmp6[icmp6type] = icmp6-routeradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 134"
tst "U69" "icmp6[icmp6type] = icmp6-neighborsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 135"
tst "U70" "icmp6[icmp6type] = icmp6-neighboradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 136"
tst "U71" "icmp6[icmp6type] = icmp6-redirect" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 137"
tst "U72" "icmp6[icmp6type] = icmp6-routerrenum" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 138"
tst "U73" "icmp6[icmp6type] = icmp6-nodeinformationquery" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 139"
tst "U74" "icmp6[icmp6type] = icmp6-nodeinformationresponse" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 140"
tst "U75" "icmp6[icmp6type] = icmp6-ineighbordiscoverysolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 141"
tst "U76" "icmp6[icmp6type] = icmp6-ineighbordiscoveryadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 142"
tst "U77" "icmp6[icmp6type] = icmp6-multicastlistenerreportv2" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 143"
tst "U78" "icmp6[icmp6type] = icmp6-homeagentdiscoveryrequest" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 144"
tst "U79" "icmp6[icmp6type] = icmp6-homeagentdiscoveryreply" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 145"
tst "U80" "icmp6[icmp6type] = icmp6-mobileprefixsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 146"
tst "U81" "icmp6[icmp6type] = icmp6-mobileprefixadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 147"
tst "U82" "icmp6[icmp6type] = icmp6-certpathsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 148"
tst "U83" "icmp6[icmp6type] = icmp6-certpathadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 149"
tst "U84" "icmp6[icmp6type] = icmp6-multicastrouteradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 151"
tst "U85" "icmp6[icmp6type] = icmp6-multicastroutersolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 152"
tst "U86" "icmp6[icmp6type] = icmp6-multicastrouterterm" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 153"

# Abbreviation of "port 67 and port 68".
tst "U87" "port 67 and 68" "((ether proto \ip && ip proto \sctp && sctp src or dst port 67) || (ether proto \ip && ip proto \tcp && tcp src or dst port 67) || (ether proto \ip && ip proto \udp && udp src or dst port 67) || (ether proto \ip6 && ip6 proto \sctp && sctp src or dst port 67) || (ether proto \ip6 && ip6 proto \tcp && tcp src or dst port 67) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 67)) && ((ether proto \ip && ip proto \sctp && sctp src or dst port 68) || (ether proto \ip && ip proto \tcp && tcp src or dst port 68) || (ether proto \ip && ip proto \udp && udp src or dst port 68) || (ether proto \ip6 && ip6 proto \sctp && sctp src or dst port 68) || (ether proto \ip6 && ip6 proto \tcp && tcp src or dst port 68) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 68))"
# When expanded this expression isn't contradictory; has same meaning as the previous expression.
tst "U88" "port 67 and port 68" "((ether proto \ip && ip proto \sctp && sctp src or dst port 67) || (ether proto \ip && ip proto \tcp && tcp src or dst port 67) || (ether proto \ip && ip proto \udp && udp src or dst port 67) || (ether proto \ip6 && ip6 proto \sctp && sctp src or dst port 67) || (ether proto \ip6 && ip6 proto \tcp && tcp src or dst port 67) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 67)) && ((ether proto \ip && ip proto \sctp && sctp src or dst port 68) || (ether proto \ip && ip proto \tcp && tcp src or dst port 68) || (ether proto \ip && ip proto \udp && udp src or dst port 68) || (ether proto \ip6 && ip6 proto \sctp && sctp src or dst port 68) || (ether proto \ip6 && ip6 proto \tcp && tcp src or dst port 68) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 68))"

# Abbreviation of "udp port 67 and udp port 68"
tst "U89" "udp port 67 and 68" "((ether proto \ip && ip proto \udp && udp src or dst port 67) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 67)) && ((ether proto \ip && ip proto \udp && udp src or dst port 68) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 68))"
tst "U90" "udp port 67 and port 68" "((ether proto \ip && ip proto \udp && udp src or dst port 67) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 67)) && ((ether proto \ip && ip proto \udp && udp src or dst port 68) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 68))"
tst "U91" "udp port 67 and udp port 68" "((ether proto \ip && ip proto \udp && udp src or dst port 67) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 67)) && ((ether proto \ip && ip proto \udp && udp src or dst port 68) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 68))"

# Other examples of tcpdump's syntax abbreviation approach.
tst "U92" "tcp dst port ftp or ftp-data or domain" "(ether proto \ip && ip proto \tcp && tcp dst port 21) || (ether proto \ip && ip proto \tcp && tcp dst port 22) || (ether proto \ip && ip proto \tcp && tcp dst port 53) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 21) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 22) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 53)"
tst "U93" "tcp dst port ftp or tcp dst port ftp-data or tcp dst port domain" "(ether proto \ip && ip proto \tcp && tcp dst port 21) || (ether proto \ip && ip proto \tcp && tcp dst port 22) || (ether proto \ip && ip proto \tcp && tcp dst port 53) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 21) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 22) || (ether proto \ip6 && ip6 proto \tcp && tcp dst port 53)"

# Test for expansion bug reported in https://gitlab.com/niksu/caper/-/issues/1
tst "U94" "ip and tcp dst port 80" "ether proto \ip && ip proto \tcp && tcp dst port 80"
tst "U95" "ip6 and tcp dst port 80" "ether proto \ip6 && ip6 proto \tcp && tcp dst port 80"

# Test for expansion bug reported in https://gitlab.com/niksu/caper/-/issues/2
tst "U96" "ip[0] + tcp[1] = 1" "ether proto \ip && ip proto \tcp && (ip[0] + tcp[1]) = 1"

tst "U97" "host tcpdump.org" "(ether proto \arp && arp src or dst host tcpdump.org) || (ether proto \ip && ip src or dst host tcpdump.org) || (ether proto \ip6 && ip6 src or dst host tcpdump.org) || (ether proto \rarp && rarp src or dst host tcpdump.org)"


# Test for the ordering pass that was added in 3d974e22d1becf6d63b76a8fc93724d1fda16dfc
tst "U98" "icmp and ip host 127.0.0.1" "ether proto \ip && ip proto \icmp && ip src or dst host 127.0.0.1"
tst "U99" "ip host 127.0.0.1 and icmp" "ether proto \ip && ip proto \icmp && ip src or dst host 127.0.0.1"

# IPv6-related tests
tst "U100" "ip6 net 2000::/3" "ether proto \ip6 && ip6 src or dst net 2000::/3"
tst "U101" "ip6 host fe80::" "ether proto \ip6 && ip6 src or dst host fe80::"
tst "U102" "ip6 host ff02::1" "ether proto \ip6 && ip6 src or dst host ff02::1"

# Checking for aggressive simplification -- in this case, distributing-out a common conjunct.
tst "U103" "ip && (tcp or udp)" "(ether proto \ip && ip proto \tcp) || (ether proto \ip && ip proto \udp)"
tst "U104" "ip and port 67 and 68" "((ether proto \ip && ip proto \sctp && sctp src or dst port 67) || (ether proto \ip && ip proto \tcp && tcp src or dst port 67) || (ether proto \ip && ip proto \udp && udp src or dst port 67)) && ((ether proto \ip && ip proto \sctp && sctp src or dst port 68) || (ether proto \ip && ip proto \tcp && tcp src or dst port 68) || (ether proto \ip && ip proto \udp && udp src or dst port 68))"

tst "U105" "tcp[0] = 0 && ((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp))" "(ether proto \ip && ip proto \tcp && tcp[0] = 0) || (ether proto \ip6 && ip6 proto \tcp && tcp[0] = 0)"

tst "U106" "ip" "ether proto \ip"
tst "U107" "tcp or udp" "(ether proto \ip && ip proto \tcp) || (ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \tcp) || (ether proto \ip6 && ip6 proto \udp)"
tst "U108" "icmp or udp port 53" "(ether proto \ip && ip proto \icmp) || (ether proto \ip && ip proto \udp && udp src or dst port 53) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 53)"
tst "U109" "icmp or udp port 53 or bootpc" "(ether proto \ip && ip proto \icmp) || (ether proto \ip && ip proto \udp && udp src or dst port 53) || (ether proto \ip && ip proto \udp && udp src or dst port 68) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 53) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 68)"
