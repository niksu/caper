#!/bin/bash

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 3 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "B1" "vlan" \
"l000: ldh [12]
l001: jeq #0x8100 , l008 , l002
l002: ldh [12]
l003: jeq #0x88a8 , l008 , l004
l004: ldh [12]
l005: jeq #0x9100 , l008 , l006
l006: ldh [-4048]
l007: jeq #0x1 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B2" "vlan 200" \
"l000: ldh [12]
l001: jeq #0x8100 , l006 , l002
l002: ldh [12]
l003: jeq #0x88a8 , l006 , l004
l004: ldh [12]
l005: jeq #0x9100 , l006 , l009
l006: ldh [14]
l007: and #0xfff
l008: jeq #0xc8 , l014 , l009
l009: ldh [-4048]
l010: jeq #0x1 , l011 , l015
l011: ldh [-4052]
l012: and #0xfff
l013: jeq #0xc8 , l014 , l015
l014: ret #262144
l015: ret #0"

tst "B3" "vlan && vlan 200" \
"l000: ldh [12]
l001: jeq #0x8100 , l006 , l002
l002: ldh [12]
l003: jeq #0x88a8 , l006 , l004
l004: ldh [12]
l005: jeq #0x9100 , l006 , l015
l006: ldh [16]
l007: jeq #0x8100 , l012 , l008
l008: ldh [16]
l009: jeq #0x88a8 , l012 , l010
l010: ldh [16]
l011: jeq #0x9100 , l012 , l015
l012: ldh [18]
l013: and #0xfff
l014: jeq #0xc8 , l026 , l015
l015: ldh [-4048]
l016: jeq #0x1 , l017 , l027
l017: ldh [12]
l018: jeq #0x8100 , l023 , l019
l019: ldh [12]
l020: jeq #0x88a8 , l023 , l021
l021: ldh [12]
l022: jeq #0x9100 , l023 , l027
l023: ldh [14]
l024: and #0xfff
l025: jeq #0xc8 , l026 , l027
l026: ret #262144
l027: ret #0"

tst "B4" "vlan 200 && vlan" \
"l000: ldh [12]
l001: jeq #0x8100 , l006 , l002
l002: ldh [12]
l003: jeq #0x88a8 , l006 , l004
l004: ldh [12]
l005: jeq #0x9100 , l006 , l015
l006: ldh [14]
l007: and #0xfff
l008: jeq #0xc8 , l009 , l015
l009: ldh [16]
l010: jeq #0x8100 , l026 , l011
l011: ldh [16]
l012: jeq #0x88a8 , l026 , l013
l013: ldh [16]
l014: jeq #0x9100 , l026 , l015
l015: ldh [-4048]
l016: jeq #0x1 , l017 , l027
l017: ldh [-4052]
l018: and #0xfff
l019: jeq #0xc8 , l020 , l027
l020: ldh [12]
l021: jeq #0x8100 , l026 , l022
l022: ldh [12]
l023: jeq #0x88a8 , l026 , l024
l024: ldh [12]
l025: jeq #0x9100 , l026 , l027
l026: ret #262144
l027: ret #0"

tst "B5" "ip" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B6" "ip6" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B7" "ip or ip6" \
"l000: ldh [12]
l001: jeq #0x800 , l004 , l002
l002: ldh [12]
l003: jeq #0x86dd , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B8" "arp" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B9" "rarp" \
"l000: ldh [12]
l001: jeq #0x8035 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B10" "ip or ip6 or arp or rarp" \
"l000: ldh [12]
l001: jeq #0x806 , l008 , l002
l002: ldh [12]
l003: jeq #0x800 , l008 , l004
l004: ldh [12]
l005: jeq #0x86dd , l008 , l006
l006: ldh [12]
l007: jeq #0x8035 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B11" "ip host 10.0.0.1" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l007
l002: ld [26]
l003: jeq #0xa000001 , l006 , l004
l004: ld [30]
l005: jeq #0xa000001 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B12" "arp host 10.0.0.1" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l007
l002: ld [28]
l003: jeq #0xa000001 , l006 , l004
l004: ld [38]
l005: jeq #0xa000001 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B13" "rarp host 10.0.0.1" \
"l000: ldh [12]
l001: jeq #0x8035 , l002 , l007
l002: ld [28]
l003: jeq #0xa000001 , l006 , l004
l004: ld [38]
l005: jeq #0xa000001 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B14" "ip6 host aaaa:bbbb:cccc:dddd:eeee:ffff:0000:1111" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l019
l002: ld [22]
l003: jeq #0xaaaabbbb , l004 , l010
l004: ld [26]
l005: jeq #0xccccdddd , l006 , l010
l006: ld [30]
l007: jeq #0xeeeeffff , l008 , l010
l008: ld [34]
l009: jeq #0x1111 , l018 , l010
l010: ld [38]
l011: jeq #0xaaaabbbb , l012 , l019
l012: ld [42]
l013: jeq #0xccccdddd , l014 , l019
l014: ld [46]
l015: jeq #0xeeeeffff , l016 , l019
l016: ld [50]
l017: jeq #0x1111 , l018 , l019
l018: ret #262144
l019: ret #0"

#tst "host 10.0.0.1" 

tst "B15" "ip net 10" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ld [26]
l003: and #0xff000000
l004: jeq #0xa000000 , l008 , l005
l005: ld [30]
l006: and #0xff000000
l007: jeq #0xa000000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B16" "arp net 10" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l009
l002: ld [28]
l003: and #0xff000000
l004: jeq #0xa000000 , l008 , l005
l005: ld [38]
l006: and #0xff000000
l007: jeq #0xa000000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B17" "rarp net 10" \
"l000: ldh [12]
l001: jeq #0x8035 , l002 , l009
l002: ld [28]
l003: and #0xff000000
l004: jeq #0xa000000 , l008 , l005
l005: ld [38]
l006: and #0xff000000
l007: jeq #0xa000000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B18" "ip net 192.168" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ld [26]
l003: and #0xffff0000
l004: jeq #0xc0a80000 , l008 , l005
l005: ld [30]
l006: and #0xffff0000
l007: jeq #0xc0a80000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B21" "ip net 192.168.10" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ld [26]
l003: and #0xffffff00
l004: jeq #0xc0a80a00 , l008 , l005
l005: ld [30]
l006: and #0xffffff00
l007: jeq #0xc0a80a00 , l008 , l009
l008: ret #262144
l009: ret #0" 

tst "B21" "icmp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l005
l002: ldb [23]
l003: jeq #0x1 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B21" "icmp6" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l009
l002: ldb [20]
l003: jeq #0x3a , l008 , l004
l004: ldb [20]
l005: jeq #0x2c , l006 , l009
l006: ldb [54]
l007: jeq #0x3a , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B22" "ip host 127.0.0.1 and icmp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x1 , l004 , l009
l004: ld [26]
l005: jeq #0x7f000001 , l008 , l006
l006: ld [30]
l007: jeq #0x7f000001 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B23" "icmp and ip host 127.0.0.1" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x1 , l004 , l009
l004: ld [26]
l005: jeq #0x7f000001 , l008 , l006
l006: ld [30]
l007: jeq #0x7f000001 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B24" "tcp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x6 , l012 , l004
l004: ldh [12]
l005: jeq #0x86dd , l006 , l013
l006: ldb [20]
l007: jeq #0x6 , l012 , l008
l008: ldb [20]
l009: jeq #0x2c , l010 , l013
l010: ldb [54]
l011: jeq #0x6 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B25" "udp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x11 , l012 , l004
l004: ldh [12]
l005: jeq #0x86dd , l006 , l013
l006: ldb [20]
l007: jeq #0x11 , l012 , l008
l008: ldb [20]
l009: jeq #0x2c , l010 , l013
l010: ldb [54]
l011: jeq #0x11 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B26" "sctp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x84 , l012 , l004
l004: ldh [12]
l005: jeq #0x86dd , l006 , l013
l006: ldb [20]
l007: jeq #0x84 , l012 , l008
l008: ldb [20]
l009: jeq #0x2c , l010 , l013
l010: ldb [54]
l011: jeq #0x84 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B27" "tcp or udp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x6 , l024 , l004
l004: ldh [12]
l005: jeq #0x800 , l006 , l008
l006: ldb [23]
l007: jeq #0x11 , l024 , l008
l008: ldh [12]
l009: jeq #0x86dd , l010 , l016
l010: ldb [20]
l011: jeq #0x6 , l024 , l012
l012: ldb [20]
l013: jeq #0x2c , l014 , l016
l014: ldb [54]
l015: jeq #0x6 , l024 , l016
l016: ldh [12]
l017: jeq #0x86dd , l018 , l025
l018: ldb [20]
l019: jeq #0x11 , l024 , l020
l020: ldb [20]
l021: jeq #0x2c , l022 , l025
l022: ldb [54]
l023: jeq #0x11 , l024 , l025
l024: ret #262144
l025: ret #0"

tst "B28" "tcp or udp or sctp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x84 , l036 , l004
l004: ldh [12]
l005: jeq #0x800 , l006 , l008
l006: ldb [23]
l007: jeq #0x6 , l036 , l008
l008: ldh [12]
l009: jeq #0x800 , l010 , l012
l010: ldb [23]
l011: jeq #0x11 , l036 , l012
l012: ldh [12]
l013: jeq #0x86dd , l014 , l020
l014: ldb [20]
l015: jeq #0x84 , l036 , l016
l016: ldb [20]
l017: jeq #0x2c , l018 , l020
l018: ldb [54]
l019: jeq #0x84 , l036 , l020
l020: ldh [12]
l021: jeq #0x86dd , l022 , l028
l022: ldb [20]
l023: jeq #0x6 , l036 , l024
l024: ldb [20]
l025: jeq #0x2c , l026 , l028
l026: ldb [54]
l027: jeq #0x6 , l036 , l028
l028: ldh [12]
l029: jeq #0x86dd , l030 , l037
l030: ldb [20]
l031: jeq #0x11 , l036 , l032
l032: ldb [20]
l033: jeq #0x2c , l034 , l037
l034: ldb [54]
l035: jeq #0x11 , l036 , l037
l036: ret #262144
l037: ret #0"

tst "B30" "ether[0] = 0"  \
"l000: ldb [0]
l001: jeq #0x0 , l002 , l003
l002: ret #262144
l003: ret #0" 

tst "B31" "ip[0] = 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l005
l002: ldb [14]
l003: jeq #0x0 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B32" "ip6[0] = 0" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l005
l002: ldb [14]
l003: jeq #0x0 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B33" "arp[0] = 0" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l005
l002: ldb [14]
l003: jeq #0x0 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B34" "icmp[0] = 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B35" "icmp6[0] = 0" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x0 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B36" "icmp[icmptype] = icmp-echoreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B37" "icmp[icmptype] = icmp-unreach" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x3 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B38" "icmp[icmptype] = icmp-sourcequench" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x4 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B39" "icmp[icmptype] = icmp-redirect" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x5 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B40" "icmp[icmptype] = icmp-echo" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x8 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B41" "icmp[icmptype] = icmp-routeradvert" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x9 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B42" "icmp[icmptype] = icmp-routersolicit" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xa , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B43" "icmp[icmptype] = icmp-timxceed" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xb , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B44" "icmp[icmptype] = icmp-paramprob" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xc , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B45" "icmp[icmptype] = icmp-tstamp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xd , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B46" "icmp[icmptype] = icmp-tstampreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xe , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B47" "icmp[icmptype] = icmp-ireq" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xf , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B48" "icmp[icmptype] = icmp-ireqreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x10 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B49" "icmp[icmptype] = icmp-maskreq" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x11 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B50" "icmp[icmptype] = icmp-maskreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x12 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B51" "icmp6[icmp6type] = icmp6-destinationunreach" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x1 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-packettoobig" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x2 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-timeexceeded" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x3 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-parameterproblem" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x4 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-echo" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x80 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-echoreply" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x81 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerquery" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x82 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerreportv1" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x83 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerdone" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x84 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-routersolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x85 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-neighborsolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x87 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-neighboradvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x88 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-redirect" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x89 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-routerrenum" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8a , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-nodeinformationquery" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8b , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-nodeinformationresponse" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8c , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-ineighbordiscoverysolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8d , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-ineighbordiscoveryadvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8e , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerreportv2" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8f , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-homeagentdiscoveryrequest" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x90 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-homeagentdiscoveryreply" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x91 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-mobileprefixsolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x92 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-mobileprefixadvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x93 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-certpathsolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x94 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-certpathadvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x95 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastrouteradvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x97 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastroutersolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x98 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastrouterterm" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x99 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B70" "ip6 net 2001:db8:1234:5678::/64" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l011
l002: ld [22]
l003: jeq #0x20010db8 , l004 , l006
l004: ld [26]
l005: jeq #0x12345678 , l010 , l006
l006: ld [38]
l007: jeq #0x20010db8 , l008 , l011
l008: ld [42]
l009: jeq #0x12345678 , l010 , l011
l010: ret #262144
l011: ret #0"

tst "B71" "ip6 net 2001:db8:1234:5678::/56" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l013
l002: ld [22]
l003: jeq #0x20010db8 , l004 , l007
l004: ld [26]
l005: and #0xffffff00
l006: jeq #0x12345600 , l012 , l007
l007: ld [38]
l008: jeq #0x20010db8 , l009 , l013
l009: ld [42]
l010: and #0xffffff00
l011: jeq #0x12345600 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B72" "ip6 net 2001:db8:1234:5678::/52" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l013
l002: ld [22]
l003: jeq #0x20010db8 , l004 , l007
l004: ld [26]
l005: and #0xfffff000
l006: jeq #0x12345000 , l012 , l007
l007: ld [38]
l008: jeq #0x20010db8 , l009 , l013
l009: ld [42]
l010: and #0xfffff000
l011: jeq #0x12345000 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B73" "ip6 net 2001:db8::/32" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ld [22]
l003: jeq #0x20010db8 , l006 , l004
l004: ld [38]
l005: jeq #0x20010db8 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B74" "tcp port 22 or 80 or 443" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l012
l002: ldb [23]
l003: jeq #0x6 , l004 , l012
l004: ldh [20]
l005: jset #0x1fff , l012 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x16 , l060 , l009
l009: ldxb 4 * ([14] & 0xf)
l010: ldh [x + 16]
l011: jeq #0x16 , l060 , l012
l012: ldh [12]
l013: jeq #0x800 , l014 , l024
l014: ldb [23]
l015: jeq #0x6 , l016 , l024
l016: ldh [20]
l017: jset #0x1fff , l024 , l018
l018: ldxb 4 * ([14] & 0xf)
l019: ldh [x + 14]
l020: jeq #0x50 , l060 , l021
l021: ldxb 4 * ([14] & 0xf)
l022: ldh [x + 16]
l023: jeq #0x50 , l060 , l024
l024: ldh [12]
l025: jeq #0x800 , l026 , l036
l026: ldb [23]
l027: jeq #0x6 , l028 , l036
l028: ldh [20]
l029: jset #0x1fff , l036 , l030
l030: ldxb 4 * ([14] & 0xf)
l031: ldh [x + 14]
l032: jeq #0x1bb , l060 , l033
l033: ldxb 4 * ([14] & 0xf)
l034: ldh [x + 16]
l035: jeq #0x1bb , l060 , l036
l036: ldh [12]
l037: jeq #0x86dd , l038 , l044
l038: ldb [20]
l039: jeq #0x6 , l040 , l044
l040: ldh [54]
l041: jeq #0x16 , l060 , l042
l042: ldh [56]
l043: jeq #0x16 , l060 , l044
l044: ldh [12]
l045: jeq #0x86dd , l046 , l052
l046: ldb [20]
l047: jeq #0x6 , l048 , l052
l048: ldh [54]
l049: jeq #0x50 , l060 , l050
l050: ldh [56]
l051: jeq #0x50 , l060 , l052
l052: ldh [12]
l053: jeq #0x86dd , l054 , l061
l054: ldb [20]
l055: jeq #0x6 , l056 , l061
l056: ldh [54]
l057: jeq #0x1bb , l060 , l058
l058: ldh [56]
l059: jeq #0x1bb , l060 , l061
l060: ret #262144
l061: ret #0"

tst "B74" "mpls && mpls 1024 && ether[0] = 0" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l010
l002: ldb [16]
l003: jset #0x1 , l010 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l010
l007: ldb [0]
l008: jeq #0x0 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B75" "vlan && vlan 200 && ip" \
"l000: ldh [12]
l001: jeq #0x8100 , l006 , l002
l002: ldh [12]
l003: jeq #0x88a8 , l006 , l004
l004: ldh [12]
l005: jeq #0x9100 , l006 , l017
l006: ldh [16]
l007: jeq #0x8100 , l012 , l008
l008: ldh [16]
l009: jeq #0x88a8 , l012 , l010
l010: ldh [16]
l011: jeq #0x9100 , l012 , l017
l012: ldh [18]
l013: and #0xfff
l014: jeq #0xc8 , l015 , l017
l015: ldh [20]
l016: jeq #0x800 , l030 , l017
l017: ldh [-4048]
l018: jeq #0x1 , l019 , l031
l019: ldh [12]
l020: jeq #0x8100 , l025 , l021
l021: ldh [12]
l022: jeq #0x88a8 , l025 , l023
l023: ldh [12]
l024: jeq #0x9100 , l025 , l031
l025: ldh [14]
l026: and #0xfff
l027: jeq #0xc8 , l028 , l031
l028: ldh [16]
l029: jeq #0x800 , l030 , l031
l030: ret #262144
l031: ret #0"

tst "B76" "portrange 10-20" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l018
l002: ldb [23]
l003: jeq #0x84 , l004 , l018
l004: ldh [20]
l005: jset #0x1fff , l018 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jge #0xa , l009 , l012
l009: ldxb 4 * ([14] & 0xf)
l010: ldh [x + 14]
l011: jgt #0x14 , l012 , l090
l012: ldxb 4 * ([14] & 0xf)
l013: ldh [x + 16]
l014: jge #0xa , l015 , l018
l015: ldxb 4 * ([14] & 0xf)
l016: ldh [x + 16]
l017: jgt #0x14 , l018 , l090
l018: ldh [12]
l019: jeq #0x800 , l020 , l036
l020: ldb [23]
l021: jeq #0x6 , l022 , l036
l022: ldh [20]
l023: jset #0x1fff , l036 , l024
l024: ldxb 4 * ([14] & 0xf)
l025: ldh [x + 14]
l026: jge #0xa , l027 , l030
l027: ldxb 4 * ([14] & 0xf)
l028: ldh [x + 14]
l029: jgt #0x14 , l030 , l090
l030: ldxb 4 * ([14] & 0xf)
l031: ldh [x + 16]
l032: jge #0xa , l033 , l036
l033: ldxb 4 * ([14] & 0xf)
l034: ldh [x + 16]
l035: jgt #0x14 , l036 , l090
l036: ldh [12]
l037: jeq #0x800 , l038 , l054
l038: ldb [23]
l039: jeq #0x11 , l040 , l054
l040: ldh [20]
l041: jset #0x1fff , l054 , l042
l042: ldxb 4 * ([14] & 0xf)
l043: ldh [x + 14]
l044: jge #0xa , l045 , l048
l045: ldxb 4 * ([14] & 0xf)
l046: ldh [x + 14]
l047: jgt #0x14 , l048 , l090
l048: ldxb 4 * ([14] & 0xf)
l049: ldh [x + 16]
l050: jge #0xa , l051 , l054
l051: ldxb 4 * ([14] & 0xf)
l052: ldh [x + 16]
l053: jgt #0x14 , l054 , l090
l054: ldh [12]
l055: jeq #0x86dd , l056 , l066
l056: ldb [20]
l057: jeq #0x84 , l058 , l066
l058: ldh [54]
l059: jge #0xa , l060 , l062
l060: ldh [54]
l061: jgt #0x14 , l062 , l090
l062: ldh [56]
l063: jge #0xa , l064 , l066
l064: ldh [56]
l065: jgt #0x14 , l066 , l090
l066: ldh [12]
l067: jeq #0x86dd , l068 , l078
l068: ldb [20]
l069: jeq #0x6 , l070 , l078
l070: ldh [54]
l071: jge #0xa , l072 , l074
l072: ldh [54]
l073: jgt #0x14 , l074 , l090
l074: ldh [56]
l075: jge #0xa , l076 , l078
l076: ldh [56]
l077: jgt #0x14 , l078 , l090
l078: ldh [12]
l079: jeq #0x86dd , l080 , l091
l080: ldb [20]
l081: jeq #0x11 , l082 , l091
l082: ldh [54]
l083: jge #0xa , l084 , l086
l084: ldh [54]
l085: jgt #0x14 , l086 , l090
l086: ldh [56]
l087: jge #0xa , l088 , l091
l088: ldh [56]
l089: jgt #0x14 , l091 , l090
l090: ret #262144
l091: ret #0"

tst "B77" "tcp[tcpflags] & (tcp-rst|tcp-ack) == (tcp-rst|tcp-ack)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x6 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 27]
l008: and #0x14
l009: jeq #0x14 , l017 , l010
l010: ldh [12]
l011: jeq #0x86dd , l012 , l018
l012: ldb [20]
l013: jeq #0x6 , l014 , l018
l014: ldb [67]
l015: and #0x14
l016: jeq #0x14 , l017 , l018
l017: ret #262144
l018: ret #0"

tst "B78" "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l032
l002: ldb [23]
l003: jeq #0x6 , l004 , l032
l004: ldh [20]
l005: jset #0x1fff , l032 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x50 , l012 , l009
l009: ldxb 4 * ([14] & 0xf)
l010: ldh [x + 16]
l011: jeq #0x50 , l012 , l032
l012: ldh [20]
l013: jset #0x1fff , l032 , l014
l014: ldh [16]
l015: st M[0]
l016: ldb [14]
l017: and #0xf
l018: lsh #0x2
l019: tax
l020: ld M[0]
l021: sub x
l022: st M[1]
l023: ldxb 4 * ([14] & 0xf)
l024: ldb [x + 26]
l025: and #0xf0
l026: rsh #0x2
l027: tax
l028: ld M[1]
l029: sub x
l030: jeq #0x0 , l032 , l031
l031: ret #262144
l032: ret #0"

tst "B79" "ether[0] & 1 = 0 and ip[16] >= 224" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l008
l002: ldb [0]
l003: and #0x1
l004: jeq #0x0 , l005 , l008
l005: ldb [30]
l006: jge #0xe0 , l007 , l008
l007: ret #262144
l008: ret #0"

tst "B80" "tcp[0] = 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x6 , l004 , l009
l004: ldh [20]
l005: jset #0x1fff , l009 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l015 , l009
l009: ldh [12]
l010: jeq #0x86dd , l011 , l016
l011: ldb [20]
l012: jeq #0x6 , l013 , l016
l013: ldb [54]
l014: jeq #0x0 , l015 , l016
l015: ret #262144
l016: ret #0"

tst "B81" "udp[0]= 0" "l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x11 , l004 , l009
l004: ldh [20]
l005: jset #0x1fff , l009 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l015 , l009
l009: ldh [12]
l010: jeq #0x86dd , l011 , l016
l011: ldb [20]
l012: jeq #0x11 , l013 , l016
l013: ldb [54]
l014: jeq #0x0 , l015 , l016
l015: ret #262144
l016: ret #0"

tst "B82" "not ip" \
"l000: ldh [12]
l001: jeq #0x800 , l003 , l002
l002: ret #262144
l003: ret #0"

tst "B83" "not (ip or ip6)" \
"l000: ldh [12]
l001: jeq #0x800 , l005 , l002
l002: ldh [12]
l003: jeq #0x86dd , l005 , l004
l004: ret #262144
l005: ret #0"

tst "B84" "not sctp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x84 , l013 , l004
l004: ldh [12]
l005: jeq #0x86dd , l006 , l012
l006: ldb [20]
l007: jeq #0x84 , l013 , l008
l008: ldb [20]
l009: jeq #0x2c , l010 , l012
l010: ldb [54]
l011: jeq #0x84 , l013 , l012
l012: ret #262144
l013: ret #0"

tst "B85" "not arp" \
"l000: ldh [12]
l001: jeq #0x806 , l003 , l002
l002: ret #262144
l003: ret #0"

tst "B86" "not ip[0] == 1" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [14]
l003: jeq #0x1 , l005 , l004
l004: ret #262144
l005: ret #0"

tst "B87" "not (tcp or udp)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x6 , l025 , l004
l004: ldh [12]
l005: jeq #0x800 , l006 , l008
l006: ldb [23]
l007: jeq #0x11 , l025 , l008
l008: ldh [12]
l009: jeq #0x86dd , l010 , l016
l010: ldb [20]
l011: jeq #0x6 , l025 , l012
l012: ldb [20]
l013: jeq #0x2c , l014 , l016
l014: ldb [54]
l015: jeq #0x6 , l025 , l016
l016: ldh [12]
l017: jeq #0x86dd , l018 , l024
l018: ldb [20]
l019: jeq #0x11 , l025 , l020
l020: ldb [20]
l021: jeq #0x2c , l022 , l024
l022: ldb [54]
l023: jeq #0x11 , l025 , l024
l024: ret #262144
l025: ret #0"

tst "B88" "udp and not (udp[9:1] >= 0x20 and udp[9:1] <= 0x7E)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x11 , l012 , l004
l004: ldh [12]
l005: jeq #0x86dd , l006 , l043
l006: ldb [20]
l007: jeq #0x11 , l012 , l008
l008: ldb [20]
l009: jeq #0x2c , l010 , l043
l010: ldb [54]
l011: jeq #0x11 , l012 , l043
l012: ldh [12]
l013: jeq #0x800 , l014 , l021
l014: ldb [23]
l015: jeq #0x11 , l016 , l021
l016: ldh [20]
l017: jset #0x1fff , l027 , l018
l018: ldxb 4 * ([14] & 0xf)
l019: ldb [x + 23]
l020: jge #0x20 , l027 , l021
l021: ldh [12]
l022: jeq #0x86dd , l023 , l042
l023: ldb [20]
l024: jeq #0x11 , l025 , l042
l025: ldb [63]
l026: jge #0x20 , l027 , l042
l027: ldh [12]
l028: jeq #0x800 , l029 , l036
l029: ldb [23]
l030: jeq #0x11 , l031 , l036
l031: ldh [20]
l032: jset #0x1fff , l043 , l033
l033: ldxb 4 * ([14] & 0xf)
l034: ldb [x + 23]
l035: jgt #0x7e , l036 , l043
l036: ldh [12]
l037: jeq #0x86dd , l038 , l042
l038: ldb [20]
l039: jeq #0x11 , l040 , l042
l040: ldb [63]
l041: jgt #0x7e , l042 , l043
l042: ret #262144
l043: ret #0"

tst "B89" "not (tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l031
l002: ldb [23]
l003: jeq #0x6 , l004 , l031
l004: ldh [20]
l005: jset #0x1fff , l031 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x50 , l012 , l009
l009: ldxb 4 * ([14] & 0xf)
l010: ldh [x + 16]
l011: jeq #0x50 , l012 , l031
l012: ldh [20]
l013: jset #0x1fff , l032 , l014
l014: ldh [16]
l015: st M[0]
l016: ldb [14]
l017: and #0xf
l018: lsh #0x2
l019: tax
l020: ld M[0]
l021: sub x
l022: st M[1]
l023: ldxb 4 * ([14] & 0xf)
l024: ldb [x + 26]
l025: and #0xf0
l026: rsh #0x2
l027: tax
l028: ld M[1]
l029: sub x
l030: jeq #0x0 , l031 , l032
l031: ret #262144
l032: ret #0"

# Capture HTTP GET
tst "B90" "tcp[((tcp[12:1] & 0xf0) >> 2) : 4] = 0x47455420" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l015
l002: ldb [23]
l003: jeq #0x6 , l004 , l015
l004: ldh [20]
l005: jset #0x1fff , l015 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 26]
l008: and #0xf0
l009: rsh #0x2
l010: ldxb 4 * ([14] & 0xf)
l011: add x
l012: tax
l013: ld [x + 14]
l014: jeq #0x47455420 , l025 , l015
l015: ldh [12]
l016: jeq #0x86dd , l017 , l026
l017: ldb [20]
l018: jeq #0x6 , l019 , l026
l019: ldb [66]
l020: and #0xf0
l021: rsh #0x2
l022: tax
l023: ld [x + 54]
l024: jeq #0x47455420 , l025 , l026
l025: ret #262144
l026: ret #0"

# OpenSSL Heartbleed
tst "B91" \
"tcp src port 443 
and (tcp[((tcp[12] & 0xF0) >> 4) * 4] = 0x18)
and (tcp[((tcp[12] & 0xF0) >> 4) * 4 + 1] = 0x03)
and (tcp[((tcp[12] & 0xF0) >> 4) * 4 + 2] < 0x04)
and ((ip[2:2] - 4 * (ip[0] & 0x0F)) - 4 * ((tcp[12] & 0xF0) >> 4) > 69)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l068
l002: ldb [23]
l003: jeq #0x6 , l004 , l068
l004: ldh [20]
l005: jset #0x1fff , l068 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x1bb , l009 , l068
l009: ldh [20]
l010: jset #0x1fff , l068 , l011
l011: ldxb 4 * ([14] & 0xf)
l012: ldb [x + 26]
l013: and #0xf0
l014: rsh #0x4
l015: mul #0x4
l016: add #0x2
l017: ldxb 4 * ([14] & 0xf)
l018: add x
l019: tax
l020: ldb [x + 14]
l021: jge #0x4 , l068 , l022
l022: ldh [20]
l023: jset #0x1fff , l068 , l024
l024: ldxb 4 * ([14] & 0xf)
l025: ldb [x + 26]
l026: and #0xf0
l027: rsh #0x4
l028: mul #0x4
l029: ldxb 4 * ([14] & 0xf)
l030: add x
l031: tax
l032: ldb [x + 14]
l033: jeq #0x18 , l034 , l068
l034: ldh [20]
l035: jset #0x1fff , l068 , l036
l036: ldh [16]
l037: st M[1]
l038: ldb [14]
l039: and #0xf
l040: mul #0x4
l041: tax
l042: ld M[1]
l043: sub x
l044: st M[3]
l045: ldxb 4 * ([14] & 0xf)
l046: ldb [x + 26]
l047: and #0xf0
l048: mul #0x4
l049: rsh #0x4
l050: tax
l051: ld M[3]
l052: sub x
l053: jgt #0x45 , l054 , l068
l054: ldh [20]
l055: jset #0x1fff , l068 , l056
l056: ldxb 4 * ([14] & 0xf)
l057: ldb [x + 26]
l058: and #0xf0
l059: rsh #0x4
l060: mul #0x4
l061: add #0x1
l062: ldxb 4 * ([14] & 0xf)
l063: add x
l064: tax
l065: ldb [x + 14]
l066: jeq #0x3 , l067 , l068
l067: ret #262144
l068: ret #0"

tst "B92" "ip6 protochain 17" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l088
l002: ldx #0x28
l003: ldb [20]
l004: jeq #0x11 , l087 , l005
l005: jeq #0x29 , l006 , l013
l006: ldb [x + 20]
l007: st M[15]
l008: ld #0x28
l009: add x
l010: tax
l011: ld M[15]
l012: jeq #0x11 , l087 , l013
l013: jeq #0x4 , l014 , l023
l014: ldb [x + 23]
l015: st M[15]
l016: ldb [x + 14]
l017: and #0xf
l018: mul #0x4
l019: add x
l020: tax
l021: ld M[15]
l022: jeq #0x11 , l087 , l023
l023: jeq #0x33 , l024 , l033
l024: ldb [x + 14]
l025: st M[15]
l026: ldb [x + 15]
l027: add #0x2
l028: mul #0x4
l029: add x
l030: tax
l031: ld M[15]
l032: jeq #0x11 , l087 , l033
l033: jeq #0x0 , l037 , l034
l034: jeq #0x3c , l037 , l035
l035: jeq #0x2b , l037 , l036
l036: jeq #0x2c , l037 , l046
l037: ldb [x + 14]
l038: st M[15]
l039: ldb [x + 15]
l040: add #0x1
l041: mul #0x8
l042: add x
l043: tax
l044: ld M[15]
l045: jeq #0x11 , l087 , l046
l046: jeq #0x29 , l047 , l054
l047: ldb [x + 20]
l048: st M[15]
l049: ld #0x28
l050: add x
l051: tax
l052: ld M[15]
l053: jeq #0x11 , l087 , l054
l054: jeq #0x4 , l055 , l064
l055: ldb [x + 23]
l056: st M[15]
l057: ldb [x + 14]
l058: and #0xf
l059: mul #0x4
l060: add x
l061: tax
l062: ld M[15]
l063: jeq #0x11 , l087 , l064
l064: jeq #0x33 , l065 , l074
l065: ldb [x + 14]
l066: st M[15]
l067: ldb [x + 15]
l068: add #0x2
l069: mul #0x4
l070: add x
l071: tax
l072: ld M[15]
l073: jeq #0x11 , l087 , l074
l074: jeq #0x0 , l078 , l075
l075: jeq #0x3c , l078 , l076
l076: jeq #0x2b , l078 , l077
l077: jeq #0x2c , l078 , l088
l078: ldb [x + 14]
l079: st M[15]
l080: ldb [x + 15]
l081: add #0x1
l082: mul #0x8
l083: add x
l084: tax
l085: ld M[15]
l086: jeq #0x11 , l087 , l088
l087: ret #262144
l088: ret #0"

tst "B92" "ip protochain 6" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l088
l002: ldb [23]
l003: ldxb 4 * ([14] & 0xf)
l004: jeq #0x6 , l087 , l005
l005: jeq #0x29 , l006 , l013
l006: ldb [x + 20]
l007: st M[15]
l008: ld #0x28
l009: add x
l010: tax
l011: ld M[15]
l012: jeq #0x6 , l087 , l013
l013: jeq #0x4 , l014 , l023
l014: ldb [x + 23]
l015: st M[15]
l016: ldb [x + 14]
l017: and #0xf
l018: mul #0x4
l019: add x
l020: tax
l021: ld M[15]
l022: jeq #0x6 , l087 , l023
l023: jeq #0x33 , l024 , l033
l024: ldb [x + 14]
l025: st M[15]
l026: ldb [x + 15]
l027: add #0x2
l028: mul #0x4
l029: add x
l030: tax
l031: ld M[15]
l032: jeq #0x6 , l087 , l033
l033: jeq #0x0 , l037 , l034
l034: jeq #0x3c , l037 , l035
l035: jeq #0x2b , l037 , l036
l036: jeq #0x2c , l037 , l046
l037: ldb [x + 14]
l038: st M[15]
l039: ldb [x + 15]
l040: add #0x1
l041: mul #0x8
l042: add x
l043: tax
l044: ld M[15]
l045: jeq #0x6 , l087 , l046
l046: jeq #0x29 , l047 , l054
l047: ldb [x + 20]
l048: st M[15]
l049: ld #0x28
l050: add x
l051: tax
l052: ld M[15]
l053: jeq #0x6 , l087 , l054
l054: jeq #0x4 , l055 , l064
l055: ldb [x + 23]
l056: st M[15]
l057: ldb [x + 14]
l058: and #0xf
l059: mul #0x4
l060: add x
l061: tax
l062: ld M[15]
l063: jeq #0x6 , l087 , l064
l064: jeq #0x33 , l065 , l074
l065: ldb [x + 14]
l066: st M[15]
l067: ldb [x + 15]
l068: add #0x2
l069: mul #0x4
l070: add x
l071: tax
l072: ld M[15]
l073: jeq #0x6 , l087 , l074
l074: jeq #0x0 , l078 , l075
l075: jeq #0x3c , l078 , l076
l076: jeq #0x2b , l078 , l077
l077: jeq #0x2c , l078 , l088
l078: ldb [x + 14]
l079: st M[15]
l080: ldb [x + 15]
l081: add #0x1
l082: mul #0x8
l083: add x
l084: tax
l085: ld M[15]
l086: jeq #0x6 , l087 , l088
l087: ret #262144
l088: ret #0"

tst "B93" "icmp protochain 6" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l014
l002: ldb [23]
l003: jeq #0x1 , l004 , l014
l004: ldxb 4 * ([14] & 0xf)
l005: ldb [x + 14]
l006: jeq #0x5 , l010 , l007
l007: jeq #0xb , l010 , l008
l008: jeq #0xc , l010 , l009
l009: jeq #0x3 , l010 , l014
l010: ldxb 4 * ([14] & 0xf)
l011: ldb [x + 31]
l012: jeq #0x6 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B94" "icmp6 protochain 17" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l103
l002: ldb [20]
l003: jeq #0x3a , l004 , l103
l004: ldx #0x30
l005: ldb [54]
l006: jeq #0x1 , l013 , l007
l007: jeq #0x2 , l013 , l008
l008: jeq #0x3 , l013 , l009
l009: jeq #0x4 , l013 , l010
l010: ldx #0x58
l011: ldb [54]
l012: jeq #0x89 , l013 , l020
l013: ldb [x + 20]
l014: st M[15]
l015: ld #0x28
l016: add x
l017: tax
l018: ld M[15]
l019: jeq #0x11 , l102 , l020
l020: jeq #0x29 , l021 , l028
l021: ldb [x + 20]
l022: st M[15]
l023: ld #0x28
l024: add x
l025: tax
l026: ld M[15]
l027: jeq #0x11 , l102 , l028
l028: jeq #0x4 , l029 , l038
l029: ldb [x + 23]
l030: st M[15]
l031: ldb [x + 14]
l032: and #0xf
l033: mul #0x4
l034: add x
l035: tax
l036: ld M[15]
l037: jeq #0x11 , l102 , l038
l038: jeq #0x33 , l039 , l048
l039: ldb [x + 14]
l040: st M[15]
l041: ldb [x + 15]
l042: add #0x2
l043: mul #0x4
l044: add x
l045: tax
l046: ld M[15]
l047: jeq #0x11 , l102 , l048
l048: jeq #0x0 , l052 , l049
l049: jeq #0x3c , l052 , l050
l050: jeq #0x2b , l052 , l051
l051: jeq #0x2c , l052 , l061
l052: ldb [x + 14]
l053: st M[15]
l054: ldb [x + 15]
l055: add #0x1
l056: mul #0x8
l057: add x
l058: tax
l059: ld M[15]
l060: jeq #0x11 , l102 , l061
l061: jeq #0x29 , l062 , l069
l062: ldb [x + 20]
l063: st M[15]
l064: ld #0x28
l065: add x
l066: tax
l067: ld M[15]
l068: jeq #0x11 , l102 , l069
l069: jeq #0x4 , l070 , l079
l070: ldb [x + 23]
l071: st M[15]
l072: ldb [x + 14]
l073: and #0xf
l074: mul #0x4
l075: add x
l076: tax
l077: ld M[15]
l078: jeq #0x11 , l102 , l079
l079: jeq #0x33 , l080 , l089
l080: ldb [x + 14]
l081: st M[15]
l082: ldb [x + 15]
l083: add #0x2
l084: mul #0x4
l085: add x
l086: tax
l087: ld M[15]
l088: jeq #0x11 , l102 , l089
l089: jeq #0x0 , l093 , l090
l090: jeq #0x3c , l093 , l091
l091: jeq #0x2b , l093 , l092
l092: jeq #0x2c , l093 , l103
l093: ldb [x + 14]
l094: st M[15]
l095: ldb [x + 15]
l096: add #0x1
l097: mul #0x8
l098: add x
l099: tax
l100: ld M[15]
l101: jeq #0x11 , l102 , l103
l102: ret #262144
l103: ret #0"

tst "B95" "inbound" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B96" "outbound" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B97" "inbound && tcp" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l015
l002: ldh [12]
l003: jeq #0x800 , l004 , l006
l004: ldb [23]
l005: jeq #0x6 , l014 , l006
l006: ldh [12]
l007: jeq #0x86dd , l008 , l015
l008: ldb [20]
l009: jeq #0x6 , l014 , l010
l010: ldb [20]
l011: jeq #0x2c , l012 , l015
l012: ldb [54]
l013: jeq #0x6 , l014 , l015
l014: ret #262144
l015: ret #0"

tst "B98" "outbound && udp" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l015
l002: ldh [12]
l003: jeq #0x800 , l004 , l006
l004: ldb [23]
l005: jeq #0x11 , l014 , l006
l006: ldh [12]
l007: jeq #0x86dd , l008 , l015
l008: ldb [20]
l009: jeq #0x11 , l014 , l010
l010: ldb [20]
l011: jeq #0x2c , l012 , l015
l012: ldb [54]
l013: jeq #0x11 , l014 , l015
l014: ret #262144
l015: ret #0"

tst "B99" "outbound && (tcp port 80 or 443)" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l043
l002: ldh [12]
l003: jeq #0x800 , l004 , l014
l004: ldb [23]
l005: jeq #0x6 , l006 , l014
l006: ldh [20]
l007: jset #0x1fff , l014 , l008
l008: ldxb 4 * ([14] & 0xf)
l009: ldh [x + 14]
l010: jeq #0x50 , l042 , l011
l011: ldxb 4 * ([14] & 0xf)
l012: ldh [x + 16]
l013: jeq #0x50 , l042 , l014
l014: ldh [12]
l015: jeq #0x800 , l016 , l026
l016: ldb [23]
l017: jeq #0x6 , l018 , l026
l018: ldh [20]
l019: jset #0x1fff , l026 , l020
l020: ldxb 4 * ([14] & 0xf)
l021: ldh [x + 14]
l022: jeq #0x1bb , l042 , l023
l023: ldxb 4 * ([14] & 0xf)
l024: ldh [x + 16]
l025: jeq #0x1bb , l042 , l026
l026: ldh [12]
l027: jeq #0x86dd , l028 , l034
l028: ldb [20]
l029: jeq #0x6 , l030 , l034
l030: ldh [54]
l031: jeq #0x50 , l042 , l032
l032: ldh [56]
l033: jeq #0x50 , l042 , l034
l034: ldh [12]
l035: jeq #0x86dd , l036 , l043
l036: ldb [20]
l037: jeq #0x6 , l038 , l043
l038: ldh [54]
l039: jeq #0x1bb , l042 , l040
l040: ldh [56]
l041: jeq #0x1bb , l042 , l043
l042: ret #262144
l043: ret #0"

tst "B100" "broadcast" \
"l000: ld [2]
l001: jeq #0xffffffff , l002 , l005
l002: ldh [0]
l003: jeq #0xffff , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B101" "multicast" \
"l000: ldb [0]
l001: jeq #0x1 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B102" "mpls && mpls 1024 && ip" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l014
l002: ldb [16]
l003: jset #0x1 , l014 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l014
l007: ldb [20]
l008: and #0x1
l009: jeq #0x1 , l010 , l014
l010: ldb [22]
l011: and #0xf0
l012: jeq #0x40 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B103" "mpls && mpls 1024 && ip6" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l014
l002: ldb [16]
l003: jset #0x1 , l014 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l014
l007: ldb [20]
l008: and #0x1
l009: jeq #0x1 , l010 , l014
l010: ldb [22]
l011: and #0xf0
l012: jeq #0x60 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B104" "mpls && mpls 1024 && (tcp || udp)" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l048
l002: ldb [16]
l003: jset #0x1 , l048 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l048
l007: ldb [20]
l008: and #0x1
l009: jeq #0x1 , l010 , l019
l010: ldb [22]
l011: and #0xf0
l012: jeq #0x60 , l013 , l019
l013: ldb [28]
l014: jeq #0x6 , l047 , l015
l015: ldb [28]
l016: jeq #0x2c , l017 , l019
l017: ldb [62]
l018: jeq #0x6 , l047 , l019
l019: ldb [20]
l020: and #0x1
l021: jeq #0x1 , l022 , l027
l022: ldb [22]
l023: and #0xf0
l024: jeq #0x40 , l025 , l027
l025: ldb [31]
l026: jeq #0x6 , l047 , l027
l027: ldb [20]
l028: and #0x1
l029: jeq #0x1 , l030 , l039
l030: ldb [22]
l031: and #0xf0
l032: jeq #0x60 , l033 , l039
l033: ldb [28]
l034: jeq #0x11 , l047 , l035
l035: ldb [28]
l036: jeq #0x2c , l037 , l039
l037: ldb [62]
l038: jeq #0x11 , l047 , l039
l039: ldb [20]
l040: and #0x1
l041: jeq #0x1 , l042 , l048
l042: ldb [22]
l043: and #0xf0
l044: jeq #0x40 , l045 , l048
l045: ldb [31]
l046: jeq #0x11 , l047 , l048
l047: ret #262144
l048: ret #0"

tst "B105" "ether proto \ip6 && ip6 src host fd3f:f209:c711::1" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l011
l002: ld [22]
l003: jeq #0xfd3ff209 , l004 , l011
l004: ld [26]
l005: jeq #0xc7110000 , l006 , l011
l006: ld [30]
l007: jeq #0x0 , l008 , l011
l008: ld [34]
l009: jeq #0x1 , l010 , l011
l010: ret #262144
l011: ret #0"
