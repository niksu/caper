#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for English to Pcap
# Marelle Leon, March 2023

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=$(basename "$0")

function tst() {
  if [ "$#" -ne 3 ]; then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "E1" "a source is foo" "src foo"

tst "E2" "not (a source is foo)" "! src foo"

tst "E3" "not (a source is foo) and a source is foo" "(! src foo) && src foo"

tst "E4" "a source is foo and not (a source is foo)" "src foo && (! src foo)"

tst "E5" "a source is not foo" "! src foo"

tst "E6" "a destination network is 10" "dst net 10"

tst "E7" "a destination network is not 10" "! dst net 10"

tst "E8" "a source host is host" "src host \\host"

tst "E9" "a source host is not host" "! src host \\host"

tst "E10" "a port range is 6000-6008" "portrange 6000-6008"

tst "E11" "a port range is not 6000-6008" "! portrange 6000-6008"

tst "E12" "of type ip4" "ip"

tst "E13" "protocol is ip4" "ip"

tst "E14" "not of type tcp" "! tcp"

tst "E15" "protocol is not tcp" "! tcp"

tst "E16" "ip4 that has source foo" "ip src foo"

tst "E17" "ip4 that does not have source foo" "! ip src foo"

tst "E18" "ip4 that has a source of foo" "ip src foo"

tst "E19" "ip4 that does not have a source of foo" "! ip src foo"

tst "E20" "ip4 that is broadcast" "ip broadcast"

tst "E21" "ip4 that is not broadcast" "! ip broadcast"

tst "E22" "a destination network is 172.16 with mask 255.255.0.0" "dst net 172.16 mask 255.255.0.0"

tst "E23" "a destination network is 172.16 with a mask of 255.255.0.0" "dst net 172.16 mask 255.255.0.0"

tst "E24" "a destination network is not 172.16 with mask 255.255.0.0" "! dst net 172.16 mask 255.255.0.0"

tst "E25" "a destination network is 172.16 of length 16 bits" "dst net 172.16/16"

tst "E26" "a destination network is 10 of length 8 bits" "dst net 10/8"

tst "E27" "ip4 that has a source of foo or
        arp that has a source of foo or
        rarp that has a source of foo" "ip src foo || arp src foo || rarp src foo"

tst "E28" "ip4 that has a network of bar or
        arp that has a network of bar or
        rarp that has a network of bar" "ip net bar || arp net bar || rarp net bar"

tst "E29" "tcp that has a port of 53 or
        udp that has a port of 53" "tcp port 53 || udp port 53"

tst "E30" "ip4 that has a network of bar or
        arp that has a network of bar or
        rarp that has a network of bar" "ip net bar || arp net bar || rarp net bar"

tst "E31" "a host is foo and 
        a port is not ftp and 
        a port is not ftp-data" "host foo && (! port ftp) && (! port ftp-data)"

tst "E32" "tcp that has a destination port of ftp or
        tcp that has a destination port of ftp-data or
        tcp that has a destination port of domain" "tcp dst port ftp || tcp dst port ftp-data || tcp dst port domain"

tst "E33" "ethernet that has a proto of ip4 and
        a host is host" "ether proto \\ip && host \\host"

tst "E34" "ethernet that has a destination of ehost" "ether dst ehost"

tst "E35" "ethernet that has a source of ehost" "ether src ehost"

tst "E36" "ethernet that has a host of ehost" "ether host ehost"

tst "E37" "a gateway is host" "gateway \\host"

tst "E38" "ethernet that has a host of ehost and
        a host is not host" "ether host ehost && (! host \\host)"

tst "E39" "a host is not vs and a host is not ace" "(! host vs) && (! host ace)"

tst "E40" "a host is not vs and a host is ace" "(! host vs) && host ace"

tst "E41" "{0} equals {0}" "0 = 0"

tst "E42" "{ip[0]} equals {0}" "ip[0] = 0"

tst "E43" "not ({ip[0]} equals {0})" "! ip[0] = 0"

tst "E44" "{# the 0th byte of the ethernet header} 
        does not equal {0}" "ether[0] != 0"

tst "E45" "{# the 0th byte of the ethernet header & 1} 
        does not equal {0}" "(ether[0] & 1) != 0"

tst "E46" "{# the 0th byte of the ip4 header & 0xf} 
        does not equal {5}" "(ip[0] & 0xf) != 5"

tst "E47" "{# the 6th 2 bytes of the ip4 header & 0x1fff} 
        equals {0}" "(ip[6 : 2] & 0x1fff) = 0"

tst "E48" "{tcp[tcpflags] & (tcp-syn | tcp-fin)} does not equal {0}" "(tcp[tcpflags] & (tcp-syn | tcp-fin)) != 0"

tst "E49" "examining the 1st byte of the tcp header:  
        it equals {25}" "tcp[1] = 25"

tst "E50" "examining the 0th byte of the ethernet header:
        {1 & # it} does not equal {0}" "(1 & ether[0]) != 0"

tst "E52" "examining the 0th byte of the ip4 header:
        {# it & 0xf} does not equal {5}" "(ip[0] & 0xf) != 5"

tst "E53" "examining the 6th 2 bytes of the ip4 header: 
        {# it & 0x1fff} equals {0}" "(ip[6 : 2] & 0x1fff) = 0"

tst "E54" "if (a source is foo), then (a host is ace)" "(! src foo) || host ace"

tst "E55" "if a source is foo, then a host is ace" "(! src foo) || host ace"

tst "E56" "if not (a source is foo), then not (a host is ace)" "(! (! src foo)) || (! host ace)"

tst "E57" "if a port is ftp, then a net is 10" "(! port ftp) || net 10"

tst "E58" "if a source is foo,
        then (if a port is ftp,
              then a net is 10)" "(! src foo) || ((! port ftp) || net 10)"

tst "E59" "if (a source is foo and a net is 42),
        then a host is ace" "(! (src foo && net 42)) || host ace"

tst "E60" "if a source is foo,
        then a host is ace;
        else ip4 that has destination foo" "((! src foo) || host ace) && (src foo || ip dst foo)"

tst "E61" "if tcp that has source port 443,
        then the 5th byte of the tcp header is {5};
        else the 4th byte of the tcp header is less than {5}" "((! tcp src port 443) || tcp[5] = 5) && (tcp src port 443 || tcp[4] < 5)"

tst "E62" "a port is one of [ftp, ftp-data]" "port ftp || ftp-data"

tst "E63" "a port is in [ftp, ftp-data]" "port ftp || ftp-data"

tst "E64" "a port is not one of [ftp, ftp-data]" "! (port ftp || ftp-data)"

tst "E65" "a port is not in [ftp, ftp-data]" "! (port ftp || ftp-data)"

tst "E66" "a port is one of [ftp, ftp-data, domain]" "port ftp || ftp-data || domain"

tst "E67" "tcp that has a destination port 
        which is one of [ftp, ftp-data, domain]" "tcp dst port ftp || ftp-data || domain"

tst "E68" "tcp that has a destination port 
        which is in [ftp, ftp-data, domain]" "tcp dst port ftp || ftp-data || domain"

tst "E69" "a port is ftp if and only if a source is foo" "(port ftp && src foo) || ((! port ftp) && (! src foo))"

tst "E70" "not (a port is ftp) if and only if not (a source is foo)" "((! port ftp) && (! src foo)) || ((! (! port ftp)) && (! (! src foo)))"

tst "E71" "(a port is ftp if and only if a source is foo) and a net is 10" "((port ftp && src foo) || ((! port ftp) && (! src foo))) && net 10"

tst "E72" "a port is ftp if and only if (a source is foo and a net is 10)" "(port ftp && (src foo && net 10)) || ((! port ftp) && (! (src foo && net 10)))"

tst "E73" "(a port is ftp and a net is 10) if and only if a source is foo" "((port ftp && net 10) && src foo) || ((! (port ftp && net 10)) && (! src foo))"

tst "E74" "a port is ftp and (a net is 10 if and only if a source is foo)" "port ftp && ((net 10 && src foo) || ((! net 10) && (! src foo)))"

tst "E75" "a host is 0:0:0:0:0:0:0:1" "host 0:0:0:0:0:0:0:1"

tst "E76" "of type inbound" "inbound"

tst "E77" "of type outbound" "outbound"

tst "E78" "the port of the tcp header is 80" "tcp port 80"

tst "E79" "the source address of the IPv4 header is foo" "ip src host foo"

tst "E80" "the source of some header is foo" "src foo"

tst "E81" "the source of some header is not foo" "! src foo"

tst "E82" "the destination network of some header is 10" "dst net 10"

tst "E83" "the source host of some header is host" "src host \host"

tst "E84" "the port range of some header is 6000-6008" "portrange 6000-6008"

tst "E85" "protocol is ip4" "ip"

tst "E86" "protocol is not tcp" "! tcp"

tst "E87" "the source of the IPv4 header is foo" "ip src foo"

tst "E88" "ip4 that is broadcast" "ip broadcast"

tst "E89" "ip4 that is not broadcast" "! ip broadcast"

tst "E90" "the destination network of some header is 172.16 with mask 255.255.0.0" "dst net 172.16 mask 255.255.0.0"

tst "E91" "the destination network of some header is 172.16 with a mask of 255.255.0.0" "dst net 172.16 mask 255.255.0.0"

tst "E92" "the destination network of some header is 172.16 of length 16 bits" "dst net 172.16/16"

tst "E93" "the destination network of some header is 10 of length 8 bits" "dst net 10/8"

tst "E94" "the source of the IPv4 header is foo or
        the source of the arp header is foo or
        the source of the rarp header is foo" "ip src foo || arp src foo || rarp src foo"

tst "E95" "the network of the IPv4 header is bar or
        the network of the arp header is bar or
        the network of the rarp header is bar" "ip net bar || arp net bar || rarp net bar"

tst "E96" "the port of the tcp header is 53 or
        the port of the udp header is 53" "tcp port 53 || udp port 53"

tst "E97" "the host of some header is foo and
        the port of some header is not ftp and
        the port of some header is not ftp-data" "host foo && (! port ftp) && (! port ftp-data)"

tst "E98" "the destination port of the tcp header is ftp or
        the destination port of the tcp header is ftp-data or
        the destination port of the tcp header is domain" "tcp dst port ftp || tcp dst port ftp-data || tcp dst port domain"

tst "E99" "the proto of the ethernet header is ip4 and
        the host of some header is host" "ether proto \ip && host \host"

tst "E100" "the destination of the ethernet header is ehost" "ether dst ehost"

tst "E101" "the source of the ethernet header is ehost" "ether src ehost"

tst "E102" "the host of the ethernet header is ehost" "ether host ehost"

tst "E103" "the gateway of some header is host" "gateway \host"

tst "E104" "the host of the ethernet header is ehost and
        the host of some header is not host" "ether host ehost && (! host \host)"

tst "E105" "the host of some header is not vs and the host of some header is not ace" "(! host vs) && (! host ace)"

tst "E106" "the host of some header is not vs and the host of some header is ace" "(! host vs) && host ace"

tst "E107" "{0} equals {0}" "0 = 0"

tst "E108" "{ip[0]} equals {0}" "ip[0] = 0"

tst "E109" "not ({ip[0]} equals {0})" "! ip[0] = 0"

tst "E110" "{# the 0th byte of the ethernet header} 
        does not equal {0}" "ether[0] != 0"

tst "E111" "{# the 0th byte of the ethernet header & 1} 
        does not equal {0}" "(ether[0] & 1) != 0"

tst "E112" "{# the 0th byte of the ip4 header & 0xf} 
        does not equal {5}" "(ip[0] & 0xf) != 5"

tst "E113" "{# the 6th 2 bytes of the ip4 header & 0x1fff} 
        equals {0}" "(ip[6 : 2] & 0x1fff) = 0"

tst "E114" "{tcp[tcpflags] & (tcp-syn | tcp-fin)} does not equal {0}" "(tcp[tcpflags] & (tcp-syn | tcp-fin)) != 0"

tst "E115" "examining the 1st byte of the tcp header:  
        it equals {25}" "tcp[1] = 25"

tst "E116" "examining the 0th byte of the ethernet header:
        {1 & # it} does not equal {0}" "(1 & ether[0]) != 0"

tst "E117" "examining the 0th byte of the ip4 header:
        {# it & 0xf} does not equal {5}" "(ip[0] & 0xf) != 5"

tst "E118" "examining the 6th 2 bytes of the ip4 header: 
        {# it & 0x1fff} equals {0}" "(ip[6 : 2] & 0x1fff) = 0"

tst "E119" "if (the source of some header is foo), then (the host of some header is ace)" "(! src foo) || host ace"

tst "E120" "if the source of some header is foo, then the host of some header is ace" "(! src foo) || host ace"

tst "E121" "if not (the source of some header is foo), then not (the host of some header is ace)" "(! (! src foo)) || (! host ace)"

tst "E122" "if the port of some header is ftp, then the net of some header is 10" "(! port ftp) || net 10"

tst "E123" "if the source of some header is foo,
        then (if the port of some header is ftp,
              then the net of some header is 10)" "(! src foo) || ((! port ftp) || net 10)"

tst "E124" "if (the source of some header is foo and the net of some header is 42),
        then the host of some header is ace" "(! (src foo && net 42)) || host ace"

tst "E125" "if the source of some header is foo,
        then the host of some header is ace;
        else ip4 that has destination foo" "((! src foo) || host ace) && (src foo || ip dst foo)"

tst "E126" "if the source port of the tcp header is 443,
        then the 5th byte of the tcp header is {5};
        else the 4th byte of the tcp header is less than {5}" "((! tcp src port 443) || tcp[5] = 5) && (tcp src port 443 || tcp[4] < 5)"

tst "E127" "the port of some header is one of [ftp, ftp-data]" "port ftp || ftp-data"

tst "E128" "the port of some header is in [ftp, ftp-data]" "port ftp || ftp-data"

tst "E129" "the port of some header is not one of [ftp, ftp-data]" "! (port ftp || ftp-data)"

tst "E130" "the port of some header is not in [ftp, ftp-data]" "! (port ftp || ftp-data)"

tst "E131" "the port of some header is one of [ftp, ftp-data, domain]" "port ftp || ftp-data || domain"

tst "E132" "the destination port of the tcp header 
        is one of [ftp, ftp-data, domain]" "tcp dst port ftp || ftp-data || domain"

tst "E133" "the destination port of the tcp header 
        is in [ftp, ftp-data, domain]" "tcp dst port ftp || ftp-data || domain"

tst "E134" "the port of some header is ftp if and only if the source of some header is foo" "(port ftp && src foo) || ((! port ftp) && (! src foo))"

tst "E135" "not (the port of some header is ftp) if and only if not (the source of some header is foo)" "((! port ftp) && (! src foo)) || ((! (! port ftp)) && (! (! src foo)))"

tst "E136" "(the port of some header is ftp if and only if the source of some header is foo) and the net of some header is 10" "((port ftp && src foo) || ((! port ftp) && (! src foo))) && net 10"

tst "E137" "the port of some header is ftp if and only if (the source of some header is foo and the net of some header is 10)" "(port ftp && (src foo && net 10)) || ((! port ftp) && (! (src foo && net 10)))"

tst "E138" "(the port of some header is ftp and the net of some header is 10) if and only if the source of some header is foo" "((port ftp && net 10) && src foo) || ((! (port ftp && net 10)) && (! src foo))"

tst "E139" "the port of some header is ftp and (the net of some header is 10 if and only if the source of some header is foo)" "port ftp && ((net 10 && src foo) || ((! net 10) && (! src foo)))"

tst "E140" "the host of some header is 0:0:0:0:0:0:0:1" "host 0:0:0:0:0:0:0:1"

tst "E141" "direction is inbound" "inbound"

tst "E142" "direction is outbound" "outbound"

tst "E143" "(protocol is IPv4 and the eighth 2 bytes of the udp header is equal to { 0x1111 }) or (the eighth 2 bytes of the icmp header is equal to { 0x1111 } or { tcp[(tcp[12] & 0xf0) >> 2 : 2] } is equal to { 0x1111 })" "(ip && udp[8 : 2] = 0x1111) || (icmp[8 : 2] = 0x1111 || tcp[(tcp[12] & 0xf0) >> 2 : 2] = 0x1111)"

tst "E144" "the port of the tcp header is 80 and { (# the total length of the IPv4 header - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 } does not equal { 0 }" "tcp port 80 && ((ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2) != 0"

tst "E145" "the destination host of the IPv6 header is fe80:0000:0000:0000:1111:11ff:fe11:1111" "ip6 dst host fe80:0000:0000:0000:1111:11ff:fe11:1111"

tst "E146" "the port of some header is 80 and { tcp[(tcp[12 : 1] & 0xf0) >> 2 : 4] } is equal to { 0x12345678 }" "port 80 && tcp[(tcp[12 : 1] & 0xf0) >> 2 : 4] = 0x12345678"

tst "E147" "the source port of the tcp header is less than { 81 }" "tcp[0 : 2] < 81"

tst "E148" "the destination port of the tcp header is less than { 81 }" "tcp[2 : 2] < 81"

tst "E149" "the source address of the IPv4 header is greater than { 0 }" "ip[12 : 4] > 0"

tst "E150" "the destination address of the IPv4 header is greater than { 0 }" "ip[16 : 4] > 0"

tst "E151" "the source port of the udp header is less than { 21 }" "udp[0 : 2] < 21"

tst "E152" "the protocol of the ip4 header is equal to { 0x37 }" "ip[9] = 0x37"

tst "E153" "the nextHeader of the ip6 header is equal to { 0x37 }" "ip6[6] = 0x37"

tst "E154" "the CWR of the tcp header is less than { 20 }" "(tcp[tcp-flags] & 1 << 7) < 20"

tst "E156" "ip4 with protocol 0x37" "ip[9] = 0x37"

tst "E157" "ip6 with nextHeader 0x37" "ip6[6] = 0x37"

tst "E158" "tcp with acknowledgement number 0x20" "tcp[8 : 4] = 0x20"

tst "E159" "tcp with flag CWR" "(tcp[tcp-flags] & 1 << 7) = 1 << 7"

tst "E160" "tcp with flag SYN" "(tcp[tcp-flags] & tcp-syn) = tcp-syn"

tst "E161" "tcp with flag FIN" "(tcp[tcp-flags] & tcp-fin) = tcp-fin"

tst "E162" "tcp with flags [SYN, FIN]" "(tcp[tcp-flags] & (tcp-syn | tcp-fin)) = (tcp-syn | tcp-fin)"

tst "E163" "tcp with flags [ECE, SYN]" "(tcp[tcp-flags] & (1 << 6 | tcp-syn)) = (1 << 6 | tcp-syn)"

# tst "E164" "tcp with flags [FIN, ACK, RST]" "(tcp[tcp-flags] & (tcp-fin | (1 << 4 | 1 << 2))) = (tcp-fin | (1 << 4 | 1 << 2))"

tst "E165" "tcp with flag ACK and tcp with acknowledgement number 0x20" "(tcp[tcp-flags] & 1 << 4) = 1 << 4 && tcp[8 : 4] = 0x20"
