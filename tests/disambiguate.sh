#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for disambiguation.
# Nik Sultana, October 2022

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 3 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "D1" "arp[0] = 0" "ether proto \arp && arp[0] = 0"
tst "D2" "atalk[0] = 0" "atalk[0] = 0" # TODO add expected result
tst "D3" "carp[0] = 0" "carp[0] = 0" # TODO add expected result
tst "D4" "decnet[0] = 0" "decnet[0] = 0" # TODO add expected result
tst "D5" "ether[0] = 0" "ether[0] = 0"
tst "D6" "fddi[0] = 0" "fddi[0] = 0" # TODO add expected result
tst "D7" "icmp[0] = 0" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
tst "D8" "icmp6[0] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[0] = 0"
tst "D9" "igmp[0] = 0" "igrp[0] = 0" # TODO add expected result
tst "D10" "igrp[0] = 0" "igrp[0] = 0" # TODO add expected result
tst "D11" "ip[0] = 0" "ether proto \ip && ip[0] = 0"
tst "D12" "ip6[0] = 0" "ether proto \ip6 && ip6[0] = 0"
tst "D13" "lat[0] = 0" "lat[0] = 0" # TODO add expected result
tst "D14" "link[0] = 0" "link[0] = 0" # TODO add expected result
tst "D15" "mopdl[0] = 0" "mopdl[0] = 0" # TODO add expected result
tst "D16" "moprc[0] = 0" "moprc[0] = 0" # TODO add expected result
tst "D17" "pim[0] = 0" "pim[0] = 0" # TODO add expected result
tst "D18" "ppp[0] = 0" "ppp[0] = 0" # TODO add expected result
tst "D19" "radio[0] = 0" "radio[0] = 0" # TODO add expected result
tst "D20" "rarp[0] = 0" "rarp[0] = 0" # TODO add expected result
tst "D21" "sca[0] = 0" "sca[0] = 0" # TODO add expected result
tst "D22" "sctp[0] = 0" "sctp[0] = 0" # TODO add expected result
tst "D23" "slip[0] = 0" "slip[0] = 0" # TODO add expected result
tst "D24" "tcp[0] = 0" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0] = 0"
tst "D25" "tcp[0:2] = 0" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0 : 2] = 0"
tst "D26" "tcp[0:2] == 0" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0 : 2] = 0"
tst "D27" "tr[0] = 0" "tr[0] = 0" # TODO add expected result
tst "D28" "udp[0] = 0" "((ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)) && udp[0] = 0"
#tst "D29" "vrrp[0] = 0" "vrrp[0] = 0" # TODO add expected result
#tst "D30" "wlan[0] = 0" "wlan[0] = 0" # TODO add expected result

# FIXME This should also generate expression alternatives involving IPv6.
# NOTE these tests are for -n; the default uses names.
tst "D31" "tcp[tcpflags] = tcp-fin" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[13] = 0x01"
tst "D32" "tcp[tcpflags] = tcp-syn" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[13] = 0x02"
tst "D33" "tcp[tcpflags] = tcp-rst" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[13] = 0x04"
tst "D34" "tcp[tcpflags] = tcp-push" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[13] = 0x08"
tst "D35" "tcp[tcpflags] = tcp-ack" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[13] = 0x10"
tst "D36" "tcp[tcpflags] = tcp-urg" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[13] = 0x20"
tst "D37" "tcp[tcpflags] = tcp-ece" "(ether proto \ip && ip proto \tcp && tcp[tcpflags] = tcp-ece) || (ether proto \ip6 && ip6 proto \tcp && tcp[tcpflags] = tcp-ece)"
tst "D38" "tcp[tcpflags] = tcp-cwr" "(ether proto \ip && ip proto \tcp && tcp[tcpflags] = tcp-cwr) || (ether proto \ip6 && ip6 proto \tcp && tcp[tcpflags] = tcp-cwr)"
tst "D39" "icmp[icmptype] = 0" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
tst "D40" "icmp[icmpcode] = 0" "ether proto \ip && ip proto \icmp && icmp[1] = 0"

tst "D41" "icmp[icmptype] = icmp-echoreply" "ether proto \ip && ip proto \icmp && icmp[0] = 0"
tst "D42" "icmp[icmptype] = icmp-unreach" "ether proto \ip && ip proto \icmp && icmp[0] = 3"
tst "D43" "icmp[icmptype] = icmp-sourcequench" "ether proto \ip && ip proto \icmp && icmp[0] = 4"
tst "D44" "icmp[icmptype] = icmp-redirect" "ether proto \ip && ip proto \icmp && icmp[0] = 5"
tst "D45" "icmp[icmptype] = icmp-echo" "ether proto \ip && ip proto \icmp && icmp[0] = 8"
tst "D46" "icmp[icmptype] = icmp-routeradvert" "ether proto \ip && ip proto \icmp && icmp[0] = 9"
tst "D47" "icmp[icmptype] = icmp-routersolicit" "ether proto \ip && ip proto \icmp && icmp[0] = 10"
tst "D48" "icmp[icmptype] = icmp-timxceed" "ether proto \ip && ip proto \icmp && icmp[0] = 11"
tst "D49" "icmp[icmptype] = icmp-paramprob" "ether proto \ip && ip proto \icmp && icmp[0] = 12"
tst "D50" "icmp[icmptype] = icmp-tstamp" "ether proto \ip && ip proto \icmp && icmp[0] = 13"
tst "D51" "icmp[icmptype] = icmp-tstampreply" "ether proto \ip && ip proto \icmp && icmp[0] = 14"
tst "D52" "icmp[icmptype] = icmp-ireq" "ether proto \ip && ip proto \icmp && icmp[0] = 15"
tst "D53" "icmp[icmptype] = icmp-ireqreply" "ether proto \ip && ip proto \icmp && icmp[0] = 16"
tst "D54" "icmp[icmptype] = icmp-maskreq" "ether proto \ip && ip proto \icmp && icmp[0] = 17"
tst "D55" "icmp[icmptype] = icmp-maskreply" "ether proto \ip && ip proto \icmp && icmp[0] = 18"

tst "D56" "icmp6[icmp6type] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 0"
tst "D57" "icmp6[icmp6code] = 0" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6code] = 0"

tst "D58" "icmp6[icmp6type] = icmp6-destinationunreach" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 1"
tst "D59" "icmp6[icmp6type] = icmp6-packettoobig" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 2"
tst "D60" "icmp6[icmp6type] = icmp6-timeexceeded" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 3"
tst "D61" "icmp6[icmp6type] = icmp6-parameterproblem" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 4"
tst "D62" "icmp6[icmp6type] = icmp6-echo" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 128"
tst "D63" "icmp6[icmp6type] = icmp6-echoreply" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 129"
tst "D64" "icmp6[icmp6type] = icmp6-multicastlistenerquery" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 130"
tst "D65" "icmp6[icmp6type] = icmp6-multicastlistenerreportv1" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 131"
tst "D66" "icmp6[icmp6type] = icmp6-multicastlistenerdone" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 132"
tst "D67" "icmp6[icmp6type] = icmp6-routersolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 133"
tst "D68" "icmp6[icmp6type] = icmp6-routeradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 134"
tst "D69" "icmp6[icmp6type] = icmp6-neighborsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 135"
tst "D70" "icmp6[icmp6type] = icmp6-neighboradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 136"
tst "D71" "icmp6[icmp6type] = icmp6-redirect" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 137"
tst "D72" "icmp6[icmp6type] = icmp6-routerrenum" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 138"
tst "D73" "icmp6[icmp6type] = icmp6-nodeinformationquery" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 139"
tst "D74" "icmp6[icmp6type] = icmp6-nodeinformationresponse" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 140"
tst "D75" "icmp6[icmp6type] = icmp6-ineighbordiscoverysolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 141"
tst "D76" "icmp6[icmp6type] = icmp6-ineighbordiscoveryadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 142"
tst "D77" "icmp6[icmp6type] = icmp6-multicastlistenerreportv2" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 143"
tst "D78" "icmp6[icmp6type] = icmp6-homeagentdiscoveryrequest" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 144"
tst "D79" "icmp6[icmp6type] = icmp6-homeagentdiscoveryreply" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 145"
tst "D80" "icmp6[icmp6type] = icmp6-mobileprefixsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 146"
tst "D81" "icmp6[icmp6type] = icmp6-mobileprefixadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 147"
tst "D82" "icmp6[icmp6type] = icmp6-certpathsolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 148"
tst "D83" "icmp6[icmp6type] = icmp6-certpathadvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 149"
tst "D84" "icmp6[icmp6type] = icmp6-multicastrouteradvert" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 151"
tst "D85" "icmp6[icmp6type] = icmp6-multicastroutersolicit" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 152"
tst "D86" "icmp6[icmp6type] = icmp6-multicastrouterterm" "ether proto \ip6 && ip6 proto \icmp6 && icmp6[icmp6type] = 153"

# Abbreviation of "port 67 and port 68".
tst "D87" "port 67 and 68" "((ether proto \ip && ((ip proto \sctp && sctp src or dst port 67) || (ip proto \tcp && tcp src or dst port 67) || (ip proto \udp && udp src or dst port 67))) || (ether proto \ip6 && ((ip6 proto \sctp && sctp src or dst port 67) || (ip6 proto \tcp && tcp src or dst port 67) || (ip6 proto \udp && udp src or dst port 67)))) && ((ether proto \ip && ((ip proto \sctp && sctp src or dst port 68) || (ip proto \tcp && tcp src or dst port 68) || (ip proto \udp && udp src or dst port 68))) || (ether proto \ip6 && ((ip6 proto \sctp && sctp src or dst port 68) || (ip6 proto \tcp && tcp src or dst port 68) || (ip6 proto \udp && udp src or dst port 68))))"
# Version with shorter output: tst "D87" "port 67 and 68" "(ether proto \ip && ((ip proto \sctp && sctp src or dst port 67 && sctp src or dst port 68) || (ip proto \tcp && tcp src or dst port 67 && tcp src or dst port 68) || (ip proto \udp && udp src or dst port 67 && udp src or dst port 68))) || (ether proto \ip6 && ((ip6 proto \sctp && sctp src or dst port 67 && sctp src or dst port 68) || (ip6 proto \tcp && tcp src or dst port 67 && tcp src or dst port 68) || (ip6 proto \udp && udp src or dst port 67 && udp src or dst port 68)))"
# When expanded this expression isn't contradictory; has same meaning as the previous expression.
tst "D88" "port 67 and port 68" "((ether proto \ip && ((ip proto \sctp && sctp src or dst port 67) || (ip proto \tcp && tcp src or dst port 67) || (ip proto \udp && udp src or dst port 67))) || (ether proto \ip6 && ((ip6 proto \sctp && sctp src or dst port 67) || (ip6 proto \tcp && tcp src or dst port 67) || (ip6 proto \udp && udp src or dst port 67)))) && ((ether proto \ip && ((ip proto \sctp && sctp src or dst port 68) || (ip proto \tcp && tcp src or dst port 68) || (ip proto \udp && udp src or dst port 68))) || (ether proto \ip6 && ((ip6 proto \sctp && sctp src or dst port 68) || (ip6 proto \tcp && tcp src or dst port 68) || (ip6 proto \udp && udp src or dst port 68))))"
# Version with shorter output: tst "D88" "port 67 and port 68" "(ether proto \ip && ((ip proto \sctp && sctp src or dst port 67 && sctp src or dst port 68) || (ip proto \tcp && tcp src or dst port 67 && tcp src or dst port 68) || (ip proto \udp && udp src or dst port 67 && udp src or dst port 68))) || (ether proto \ip6 && ((ip6 proto \sctp && sctp src or dst port 67 && sctp src or dst port 68) || (ip6 proto \tcp && tcp src or dst port 67 && tcp src or dst port 68) || (ip6 proto \udp && udp src or dst port 67 && udp src or dst port 68)))"

# Abbreviation of "udp port 67 and udp port 68"
tst "D89" "udp port 67 and 68" "((ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)) && udp src or dst port 67 && udp src or dst port 68"
tst "D90" "udp port 67 and port 68" "((ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)) && udp src or dst port 67 && udp src or dst port 68"
tst "D91" "udp port 67 and udp port 68" "((ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)) && udp src or dst port 67 && udp src or dst port 68"

# Other examples of tcpdump's syntax abbreviation approach.
tst "D92" "tcp dst port ftp or ftp-data or domain" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && (tcp dst port 21 || tcp dst port 22 || tcp dst port 53)"
tst "D93" "tcp dst port ftp or tcp dst port ftp-data or tcp dst port domain" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && (tcp dst port 21 || tcp dst port 22 || tcp dst port 53)"

# Test for expansion bug reported in https://gitlab.com/niksu/caper/-/issues/1
tst "D94" "ip and tcp dst port 80" "ether proto \ip && ip proto \tcp && tcp dst port 80"
tst "D95" "ip6 and tcp dst port 80" "ether proto \ip6 && ip6 proto \tcp && tcp dst port 80"

# Test for expansion bug reported in https://gitlab.com/niksu/caper/-/issues/2
tst "D96" "ip[0] + tcp[1] = 1" "ether proto \ip && ip proto \tcp && (ip[0] + tcp[1]) = 1"

tst "D97" "host tcpdump.org" "(ether proto \arp && arp src or dst host tcpdump.org) || (ether proto \ip && ip src or dst host tcpdump.org) || (ether proto \ip6 && ip6 src or dst host tcpdump.org) || (ether proto \rarp && rarp src or dst host tcpdump.org)"


# Test for the ordering pass that was added in 3d974e22d1becf6d63b76a8fc93724d1fda16dfc
tst "D98" "icmp and ip host 127.0.0.1" "ether proto \ip && ip proto \icmp && ip src or dst host 127.0.0.1"
tst "D99" "ip host 127.0.0.1 and icmp" "ether proto \ip && ip proto \icmp && ip src or dst host 127.0.0.1"

# IPv6-related tests
tst "D100" "ip6 net 2000::/3" "ether proto \ip6 && ip6 src or dst net 2000::/3"
tst "D101" "ip6 host fe80::" "ether proto \ip6 && ip6 src or dst host fe80::"
tst "D102" "ip6 host ff02::1" "ether proto \ip6 && ip6 src or dst host ff02::1"

# Checking for aggressive simplification -- in this case, distributing-out a common conjunct.
tst "D103" "ip && (tcp or udp)" "ether proto \ip && (ip proto \tcp || ip proto \udp)"
tst "D104" "ip and port 67 and 68" "ether proto \ip && ((ip proto \sctp && sctp src or dst port 67) || (ip proto \tcp && tcp src or dst port 67) || (ip proto \udp && udp src or dst port 67)) && ((ip proto \sctp && sctp src or dst port 68) || (ip proto \tcp && tcp src or dst port 68) || (ip proto \udp && udp src or dst port 68))"
# Version with shorter output: tst "D104" "ip and port 67 and 68" "ether proto \ip && ((ip proto \sctp && sctp src or dst port 67 && sctp src or dst port 68) || (ip proto \tcp && tcp src or dst port 67 && tcp src or dst port 68) || (ip proto \udp && udp src or dst port 67 && udp src or dst port 68))"

tst "D105" "tcp[0] = 0 && ((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp))" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp[0] = 0"

tst "D106" "ip" "ether proto \ip"
tst "D107" "tcp or udp" "(ether proto \ip && (ip proto \tcp || ip proto \udp)) || (ether proto \ip6 && (ip6 proto \tcp || ip6 proto \udp))"
tst "D108" "icmp or udp port 53" "(ether proto \ip && (ip proto \icmp || (ip proto \udp && udp src or dst port 53))) || (ether proto \ip6 && ip6 proto \udp && udp src or dst port 53)"
tst "D109" "icmp or udp port 53 or bootpc" "(ether proto \ip && (ip proto \icmp || (ip proto \udp && (udp src or dst port 53 || udp src or dst port 68)))) || (ether proto \ip6 && ip6 proto \udp && (udp src or dst port 53 || udp src or dst port 68))"

# Tests involving negation.
tst "D110" "not tcp" "((! ether proto \ip) || (! ip proto \tcp)) && ((! ether proto \ip6) || (! ip6 proto \tcp))"
tst "D111" "not port 67" "((! ether proto \ip) || (((! ip proto \sctp) || (! sctp src or dst port 67)) && ((! ip proto \tcp) || (! tcp src or dst port 67)) && ((! ip proto \udp) || (! udp src or dst port 67)))) && ((! ether proto \ip6) || (((! ip6 proto \sctp) || (! sctp src or dst port 67)) && ((! ip6 proto \tcp) || (! tcp src or dst port 67)) && ((! ip6 proto \udp) || (! udp src or dst port 67))))"
tst "D112" "not (udp or tcp)" "((! ether proto \ip) || ((! ip proto \tcp) && (! ip proto \udp))) && ((! ether proto \ip6) || ((! ip6 proto \tcp) && (! ip6 proto \udp)))"
tst "D113" "udp and not (udp[9:1] >= 0x20 and udp[9:1] <= 0x7E)" "((ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)) && (udp[9 : 1] < 0x20 || udp[9 : 1] > 0x7E)"

tst "D114" "not ip" "! ether proto \ip"
tst "D115" "not (ip or ip6)" "(! ether proto \ip) && (! ether proto \ip6)"
tst "D116" "not sctp" "((! ether proto \ip) || (! ip proto \sctp)) && ((! ether proto \ip6) || (! ip6 proto \sctp))"
tst "D117" "not arp" "! ether proto \arp"
tst "D118" "not ((ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0)" "(! ether proto \ip) || (! ip proto \tcp) || ((ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2) = 0"

tst "D119" "tcp or not tcp" "@: True :@"
tst "D120" "not (tcp or not tcp)" "@: False :@"

tst "D121" "inbound and tcp" "inbound && ((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp))"
tst "D122" "udp or outbound" "outbound || (ether proto \ip && ip proto \udp) || (ether proto \ip6 && ip6 proto \udp)"
tst "D123" "outbound and ether" "outbound && ether"
tst "D124" "ether and outbound" "outbound && ether"
tst "D125" "ip and outbound" "outbound && ether proto \ip"
tst "D126" "outbound and ip" "outbound && ether proto \ip"

tst "D127" "tcp port 0x1234" "((ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)) && tcp src or dst port 4660"
tst "D128" "ether proto 0x0806" "ether proto 2054"
tst "D129" "tcp port -1" "@: False :@"

# From https://gitlab.com/niksu/caper/-/issues/9
tst "D130" "vxlan" "vxlan"
tst "D131" "geneve" "geneve"
tst "D132" "pppoes" "pppoes"
tst "D133" "pppoed" "pppoed"
