#!/bin/bash

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 3 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "B1" "vlan" \
"l000: ldh [12]
l001: jeq #0x8100 , l006 , l002
l002: jeq #0x88a8 , l006 , l003
l003: jeq #0x9100 , l006 , l004
l004: ldh [-4048]
l005: jeq #0x1 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B2" "vlan 200" \
"l000: ldh [12]
l001: jeq #0x8100 , l004 , l002
l002: jeq #0x88a8 , l004 , l003
l003: jeq #0x9100 , l004 , l007
l004: ldh [14]
l005: and #0xfff
l006: jeq #0xc8 , l012 , l007
l007: ldh [-4048]
l008: jeq #0x1 , l009 , l013
l009: ldh [-4052]
l010: and #0xfff
l011: jeq #0xc8 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B3" "vlan && vlan 200" \
"l000: ldh [12]
l001: jeq #0x8100 , l004 , l002
l002: jeq #0x88a8 , l004 , l003
l003: jeq #0x9100 , l004 , l011
l004: ldh [16]
l005: jeq #0x8100 , l008 , l006
l006: jeq #0x88a8 , l008 , l007
l007: jeq #0x9100 , l008 , l011
l008: ldh [18]
l009: and #0xfff
l010: jeq #0xc8 , l020 , l011
l011: ldh [-4048]
l012: jeq #0x1 , l013 , l021
l013: ldh [12]
l014: jeq #0x8100 , l017 , l015
l015: jeq #0x88a8 , l017 , l016
l016: jeq #0x9100 , l017 , l021
l017: ldh [14]
l018: and #0xfff
l019: jeq #0xc8 , l020 , l021
l020: ret #262144
l021: ret #0"

tst "B4" "vlan 200 && vlan" \
"l000: ldh [12]
l001: jeq #0x8100 , l004 , l002
l002: jeq #0x88a8 , l004 , l003
l003: jeq #0x9100 , l004 , l011
l004: ldh [14]
l005: and #0xfff
l006: jeq #0xc8 , l007 , l011
l007: ldh [16]
l008: jeq #0x8100 , l020 , l009
l009: jeq #0x88a8 , l020 , l010
l010: jeq #0x9100 , l020 , l011
l011: ldh [-4048]
l012: jeq #0x1 , l013 , l021
l013: ldh [-4052]
l014: and #0xfff
l015: jeq #0xc8 , l016 , l021
l016: ldh [12]
l017: jeq #0x8100 , l020 , l018
l018: jeq #0x88a8 , l020 , l019
l019: jeq #0x9100 , l020 , l021
l020: ret #262144
l021: ret #0"

tst "B5" "ip" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B6" "ip6" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B7" "ip or ip6" \
"l000: ldh [12]
l001: jeq #0x800 , l003 , l002
l002: jeq #0x86dd , l003 , l004
l003: ret #262144
l004: ret #0"

tst "B8" "arp" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B9" "rarp" \
"l000: ldh [12]
l001: jeq #0x8035 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B10" "ip or ip6 or arp or rarp" \
"l000: ldh [12]
l001: jeq #0x806 , l005 , l002
l002: jeq #0x800 , l005 , l003
l003: jeq #0x86dd , l005 , l004
l004: jeq #0x8035 , l005 , l006
l005: ret #262144
l006: ret #0"

tst "B11" "ip host 10.0.0.1" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l007
l002: ld [26]
l003: jeq #0xa000001 , l006 , l004
l004: ld [30]
l005: jeq #0xa000001 , l006 , l007
l006: ret #262144
l007: ret #0"


tst "B14" "ip6 host aaaa:bbbb:cccc:dddd:eeee:ffff:0000:1111" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l019
l002: ld [22]
l003: jeq #0xaaaabbbb , l004 , l010
l004: ld [26]
l005: jeq #0xccccdddd , l006 , l010
l006: ld [30]
l007: jeq #0xeeeeffff , l008 , l010
l008: ld [34]
l009: jeq #0x1111 , l018 , l010
l010: ld [38]
l011: jeq #0xaaaabbbb , l012 , l019
l012: ld [42]
l013: jeq #0xccccdddd , l014 , l019
l014: ld [46]
l015: jeq #0xeeeeffff , l016 , l019
l016: ld [50]
l017: jeq #0x1111 , l018 , l019
l018: ret #262144
l019: ret #0"

tst "B15" "host 10.0.0.1" \
"l000: ldh [12]
l001: jeq #0x806 , l008 , l002
l002: jeq #0x800 , l003 , l007
l003: ld [26]
l004: jeq #0xa000001 , l012 , l005
l005: ld [30]
l006: jeq #0xa000001 , l012 , l013
l007: jeq #0x8035 , l008 , l013
l008: ld [28]
l009: jeq #0xa000001 , l012 , l010
l010: ld [38]
l011: jeq #0xa000001 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B15" "ip net 10" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ld [26]
l003: and #0xff000000
l004: jeq #0xa000000 , l008 , l005
l005: ld [30]
l006: and #0xff000000
l007: jeq #0xa000000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B16" "arp net 10" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l009
l002: ld [28]
l003: and #0xff000000
l004: jeq #0xa000000 , l008 , l005
l005: ld [38]
l006: and #0xff000000
l007: jeq #0xa000000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B17" "rarp net 10" \
"l000: ldh [12]
l001: jeq #0x8035 , l002 , l009
l002: ld [28]
l003: and #0xff000000
l004: jeq #0xa000000 , l008 , l005
l005: ld [38]
l006: and #0xff000000
l007: jeq #0xa000000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B18" "ip net 192.168" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ld [26]
l003: and #0xffff0000
l004: jeq #0xc0a80000 , l008 , l005
l005: ld [30]
l006: and #0xffff0000
l007: jeq #0xc0a80000 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B21" "ip net 192.168.10" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ld [26]
l003: and #0xffffff00
l004: jeq #0xc0a80a00 , l008 , l005
l005: ld [30]
l006: and #0xffffff00
l007: jeq #0xc0a80a00 , l008 , l009
l008: ret #262144
l009: ret #0" 

tst "B21" "icmp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l005
l002: ldb [23]
l003: jeq #0x1 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B21" "icmp6" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l008
l002: ldb [20]
l003: jeq #0x3a , l007 , l004
l004: jeq #0x2c , l005 , l008
l005: ldb [54]
l006: jeq #0x3a , l007 , l008
l007: ret #262144
l008: ret #0"

tst "B22" "ip host 127.0.0.1 and icmp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x1 , l004 , l009
l004: ld [26]
l005: jeq #0x7f000001 , l008 , l006
l006: ld [30]
l007: jeq #0x7f000001 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B23" "icmp and ip host 127.0.0.1" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x1 , l004 , l009
l004: ld [26]
l005: jeq #0x7f000001 , l008 , l006
l006: ld [30]
l007: jeq #0x7f000001 , l008 , l009
l008: ret #262144
l009: ret #0"

tst "B24" "tcp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x6 , l010 , l011
l004: jeq #0x86dd , l005 , l011
l005: ldb [20]
l006: jeq #0x6 , l010 , l007
l007: jeq #0x2c , l008 , l011
l008: ldb [54]
l009: jeq #0x6 , l010 , l011
l010: ret #262144
l011: ret #0"

tst "B25" "udp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x11 , l010 , l011
l004: jeq #0x86dd , l005 , l011
l005: ldb [20]
l006: jeq #0x11 , l010 , l007
l007: jeq #0x2c , l008 , l011
l008: ldb [54]
l009: jeq #0x11 , l010 , l011
l010: ret #262144
l011: ret #0"

tst "B26" "sctp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x84 , l010 , l011
l004: jeq #0x86dd , l005 , l011
l005: ldb [20]
l006: jeq #0x84 , l010 , l007
l007: jeq #0x2c , l008 , l011
l008: ldb [54]
l009: jeq #0x84 , l010 , l011
l010: ret #262144
l011: ret #0"

tst "B27" "tcp or udp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l005
l002: ldb [23]
l003: jeq #0x6 , l013 , l004
l004: jeq #0x11 , l013 , l014
l005: jeq #0x86dd , l006 , l014
l006: ldb [20]
l007: jeq #0x6 , l013 , l008
l008: jeq #0x2c , l009 , l011
l009: ldb [54]
l010: jeq #0x6 , l013 , l012
l011: jeq #0x11 , l013 , l014
l012: jeq #0x11 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B28" "tcp or udp or sctp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l006
l002: ldb [23]
l003: jeq #0x84 , l016 , l004
l004: jeq #0x6 , l016 , l005
l005: jeq #0x11 , l016 , l017
l006: jeq #0x86dd , l007 , l017
l007: ldb [20]
l008: jeq #0x84 , l016 , l009
l009: jeq #0x2c , l010 , l012
l010: ldb [54]
l011: jeq #0x84 , l016 , l013
l012: jeq #0x6 , l016 , l014
l013: jeq #0x6 , l016 , l015
l014: jeq #0x11 , l016 , l017
l015: jeq #0x11 , l016 , l017
l016: ret #262144
l017: ret #0"

tst "B30" "ether[0] = 0"  \
"l000: ldb [0]
l001: jeq #0x0 , l002 , l003
l002: ret #262144
l003: ret #0" 

tst "B31" "ip[0] = 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l005
l002: ldb [14]
l003: jeq #0x0 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B32" "ip6[0] = 0" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l005
l002: ldb [14]
l003: jeq #0x0 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B33" "arp[0] = 0" \
"l000: ldh [12]
l001: jeq #0x806 , l002 , l005
l002: ldb [14]
l003: jeq #0x0 , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B34" "icmp[0] = 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B35" "icmp6[0] = 0" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x0 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B36" "icmp[icmptype] = icmp-echoreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B37" "icmp[icmptype] = icmp-unreach" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x3 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B38" "icmp[icmptype] = icmp-sourcequench" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x4 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B39" "icmp[icmptype] = icmp-redirect" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x5 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B40" "icmp[icmptype] = icmp-echo" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x8 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B41" "icmp[icmptype] = icmp-routeradvert" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x9 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B42" "icmp[icmptype] = icmp-routersolicit" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xa , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B43" "icmp[icmptype] = icmp-timxceed" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xb , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B44" "icmp[icmptype] = icmp-paramprob" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xc , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B45" "icmp[icmptype] = icmp-tstamp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xd , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B46" "icmp[icmptype] = icmp-tstampreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xe , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B47" "icmp[icmptype] = icmp-ireq" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0xf , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B48" "icmp[icmptype] = icmp-ireqreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x10 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B49" "icmp[icmptype] = icmp-maskreq" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x11 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B50" "icmp[icmptype] = icmp-maskreply" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x1 , l004 , l010
l004: ldh [20]
l005: jset #0x1fff , l010 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x12 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B51" "icmp6[icmp6type] = icmp6-destinationunreach" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x1 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-packettoobig" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x2 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-timeexceeded" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x3 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-parameterproblem" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x4 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-echo" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x80 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-echoreply" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x81 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerquery" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x82 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerreportv1" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x83 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerdone" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x84 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-routersolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x85 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-neighborsolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x87 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-neighboradvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x88 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-redirect" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x89 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-routerrenum" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8a , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-nodeinformationquery" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8b , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-nodeinformationresponse" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8c , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-ineighbordiscoverysolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8d , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-ineighbordiscoveryadvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8e , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastlistenerreportv2" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x8f , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-homeagentdiscoveryrequest" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x90 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-homeagentdiscoveryreply" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x91 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-mobileprefixsolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x92 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-mobileprefixadvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x93 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-certpathsolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x94 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-certpathadvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x95 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastrouteradvert" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x97 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastroutersolicit" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x98 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B52" "icmp6[icmp6type] = icmp6-multicastrouterterm" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ldb [20]
l003: jeq #0x3a , l004 , l007
l004: ldb [54]
l005: jeq #0x99 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B70" "ip6 net 2001:db8:1234:5678::/64" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l011
l002: ld [22]
l003: jeq #0x20010db8 , l004 , l006
l004: ld [26]
l005: jeq #0x12345678 , l010 , l006
l006: ld [38]
l007: jeq #0x20010db8 , l008 , l011
l008: ld [42]
l009: jeq #0x12345678 , l010 , l011
l010: ret #262144
l011: ret #0"

tst "B71" "ip6 net 2001:db8:1234:5678::/56" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l013
l002: ld [22]
l003: jeq #0x20010db8 , l004 , l007
l004: ld [26]
l005: and #0xffffff00
l006: jeq #0x12345600 , l012 , l007
l007: ld [38]
l008: jeq #0x20010db8 , l009 , l013
l009: ld [42]
l010: and #0xffffff00
l011: jeq #0x12345600 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B72" "ip6 net 2001:db8:1234:5678::/52" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l013
l002: ld [22]
l003: jeq #0x20010db8 , l004 , l007
l004: ld [26]
l005: and #0xfffff000
l006: jeq #0x12345000 , l012 , l007
l007: ld [38]
l008: jeq #0x20010db8 , l009 , l013
l009: ld [42]
l010: and #0xfffff000
l011: jeq #0x12345000 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B73" "ip6 net 2001:db8::/32" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l007
l002: ld [22]
l003: jeq #0x20010db8 , l006 , l004
l004: ld [38]
l005: jeq #0x20010db8 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B74" "tcp port 22 or 80 or 443" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l015
l002: ldb [23]
l003: jeq #0x6 , l004 , l027
l004: ldh [20]
l005: jset #0x1fff , l027 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x16 , l026 , l009
l009: jeq #0x50 , l026 , l010
l010: jeq #0x1bb , l026 , l011
l011: ldh [x + 16]
l012: jeq #0x16 , l026 , l013
l013: jeq #0x50 , l026 , l014
l014: jeq #0x1bb , l026 , l027
l015: jeq #0x86dd , l016 , l027
l016: ldb [20]
l017: jeq #0x6 , l018 , l027
l018: ldh [54]
l019: jeq #0x16 , l026 , l020
l020: jeq #0x50 , l026 , l021
l021: jeq #0x1bb , l026 , l022
l022: ldh [56]
l023: jeq #0x16 , l026 , l024
l024: jeq #0x50 , l026 , l025
l025: jeq #0x1bb , l026 , l027
l026: ret #262144
l027: ret #0"

tst "B74" "mpls && mpls 1024 && ether[0] = 0" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l010
l002: ldb [16]
l003: jset #0x1 , l010 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l010
l007: ldb [0]
l008: jeq #0x0 , l009 , l010
l009: ret #262144
l010: ret #0"

tst "B75" "vlan && vlan 200 && ip" \
"l000: ldh [12]
l001: jeq #0x8100 , l004 , l002
l002: jeq #0x88a8 , l004 , l003
l003: jeq #0x9100 , l004 , l013
l004: ldh [16]
l005: jeq #0x8100 , l008 , l006
l006: jeq #0x88a8 , l008 , l007
l007: jeq #0x9100 , l008 , l013
l008: ldh [18]
l009: and #0xfff
l010: jeq #0xc8 , l011 , l013
l011: ldh [20]
l012: jeq #0x800 , l024 , l013
l013: ldh [-4048]
l014: jeq #0x1 , l015 , l025
l015: ldh [12]
l016: jeq #0x8100 , l019 , l017
l017: jeq #0x88a8 , l019 , l018
l018: jeq #0x9100 , l019 , l025
l019: ldh [14]
l020: and #0xfff
l021: jeq #0xc8 , l022 , l025
l022: ldh [16]
l023: jeq #0x800 , l024 , l025
l024: ret #262144
l025: ret #0"

tst "B76" "portrange 10-20" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l015
l002: ldb [23]
l003: jeq #0x84 , l006 , l004
l004: jeq #0x6 , l006 , l005
l005: jeq #0x11 , l006 , l027
l006: ldh [20]
l007: jset #0x1fff , l027 , l008
l008: ldxb 4 * ([14] & 0xf)
l009: ldh [x + 14]
l010: jge #0xa , l011 , l012
l011: jgt #0x14 , l012 , l026
l012: ldh [x + 16]
l013: jge #0xa , l014 , l027
l014: jgt #0x14 , l027 , l026
l015: jeq #0x86dd , l016 , l027
l016: ldb [20]
l017: jeq #0x84 , l020 , l018
l018: jeq #0x6 , l020 , l019
l019: jeq #0x11 , l020 , l027
l020: ldh [54]
l021: jge #0xa , l022 , l023
l022: jgt #0x14 , l023 , l026
l023: ldh [56]
l024: jge #0xa , l025 , l027
l025: jgt #0x14 , l027 , l026
l026: ret #262144
l027: ret #0"

tst "B77" "tcp[tcpflags] & (tcp-rst|tcp-ack) == (tcp-rst|tcp-ack)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l010
l002: ldb [23]
l003: jeq #0x6 , l004 , l017
l004: ldh [20]
l005: jset #0x1fff , l017 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 27]
l008: and #0x14
l009: jeq #0x14 , l016 , l017
l010: jeq #0x86dd , l011 , l017
l011: ldb [20]
l012: jeq #0x6 , l013 , l017
l013: ldb [67]
l014: and #0x14
l015: jeq #0x14 , l016 , l017
l016: ret #262144
l017: ret #0"

tst "B78" "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l028
l002: ldb [23]
l003: jeq #0x6 , l004 , l028
l004: ldh [20]
l005: jset #0x1fff , l028 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x50 , l011 , l009
l009: ldh [x + 16]
l010: jeq #0x50 , l011 , l028
l011: ldh [16]
l012: st M[0]
l013: ldb [14]
l014: and #0xf
l015: lsh #0x2
l016: tax
l017: ld M[0]
l018: sub x
l019: st M[1]
l020: ldxb 4 * ([14] & 0xf)
l021: ldb [x + 26]
l022: and #0xf0
l023: rsh #0x2
l024: tax
l025: ld M[1]
l026: jeq x , l028 , l027
l027: ret #262144
l028: ret #0"

tst "B79" "ether[0] & 1 = 0 and ip[16] >= 224" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l007
l002: ldb [0]
l003: jset #0x1 , l004 , l007
l004: ldb [30]
l005: jge #0xe0 , l006 , l007
l006: ret #262144
l007: ret #0"

tst "B80" "tcp[0] = 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x6 , l004 , l015
l004: ldh [20]
l005: jset #0x1fff , l015 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l014 , l015
l009: jeq #0x86dd , l010 , l015
l010: ldb [20]
l011: jeq #0x6 , l012 , l015
l012: ldb [54]
l013: jeq #0x0 , l014 , l015
l014: ret #262144
l015: ret #0"

tst "B81" "udp[0]= 0" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l009
l002: ldb [23]
l003: jeq #0x11 , l004 , l015
l004: ldh [20]
l005: jset #0x1fff , l015 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 14]
l008: jeq #0x0 , l014 , l015
l009: jeq #0x86dd , l010 , l015
l010: ldb [20]
l011: jeq #0x11 , l012 , l015
l012: ldb [54]
l013: jeq #0x0 , l014 , l015
l014: ret #262144
l015: ret #0"

tst "B82" "not ip" \
"l000: ldh [12]
l001: jeq #0x800 , l003 , l002
l002: ret #262144
l003: ret #0"

tst "B83" "not (ip or ip6)" \
"l000: ldh [12]
l001: jeq #0x800 , l004 , l002
l002: jeq #0x86dd , l004 , l003
l003: ret #262144
l004: ret #0"

tst "B84" "not sctp" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x84 , l011 , l010
l004: jeq #0x86dd , l005 , l010
l005: ldb [20]
l006: jeq #0x84 , l011 , l007
l007: jeq #0x2c , l008 , l010
l008: ldb [54]
l009: jeq #0x84 , l011 , l010
l010: ret #262144
l011: ret #0"

tst "B85" "not arp" \
"l000: ldh [12]
l001: jeq #0x806 , l003 , l002
l002: ret #262144
l003: ret #0"

tst "B86" "not ip[0] == 1" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [14]
l003: jeq #0x1 , l005 , l004
l004: ret #262144
l005: ret #0"

tst "B87" "not (tcp or udp)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l005
l002: ldb [23]
l003: jeq #0x6 , l014 , l004
l004: jeq #0x11 , l014 , l013
l005: jeq #0x86dd , l006 , l013
l006: ldb [20]
l007: jeq #0x6 , l014 , l008
l008: jeq #0x2c , l009 , l011
l009: ldb [54]
l010: jeq #0x6 , l014 , l012
l011: jeq #0x11 , l014 , l013
l012: jeq #0x11 , l014 , l013
l013: ret #262144
l014: ret #0"

tst "B88" "udp and not (udp[9:1] >= 0x20 and udp[9:1] <= 0x7E)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l004
l002: ldb [23]
l003: jeq #0x11 , l010 , l020
l004: jeq #0x86dd , l005 , l020
l005: ldb [20]
l006: jeq #0x11 , l015 , l007
l007: jeq #0x2c , l008 , l020
l008: ldb [54]
l009: jeq #0x11 , l019 , l020
l010: ldh [20]
l011: jset #0x1fff , l020 , l012
l012: ldxb 4 * ([14] & 0xf)
l013: ldb [x + 23]
l014: jge #0x20 , l017 , l019
l015: ldb [63]
l016: jge #0x20 , l018 , l019
l017: jgt #0x7e , l019 , l020
l018: jgt #0x7e , l019 , l020
l019: ret #262144
l020: ret #0"

tst "B89" "not (tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l027
l002: ldb [23]
l003: jeq #0x6 , l004 , l027
l004: ldh [20]
l005: jset #0x1fff , l027 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x50 , l011 , l009
l009: ldh [x + 16]
l010: jeq #0x50 , l011 , l027
l011: ldh [16]
l012: st M[0]
l013: ldb [14]
l014: and #0xf
l015: lsh #0x2
l016: tax
l017: ld M[0]
l018: sub x
l019: st M[1]
l020: ldxb 4 * ([14] & 0xf)
l021: ldb [x + 26]
l022: and #0xf0
l023: rsh #0x2
l024: tax
l025: ld M[1]
l026: jeq x , l027 , l028
l027: ret #262144
l028: ret #0"

# HTTP get
tst "B90" "tcp[((tcp[12:1] & 0xf0) >> 2) : 4] = 0x47455420" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l015
l002: ldb [23]
l003: jeq #0x6 , l004 , l025
l004: ldh [20]
l005: jset #0x1fff , l025 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldb [x + 26]
l008: and #0xf0
l009: rsh #0x2
l010: ldxb 4 * ([14] & 0xf)
l011: add x
l012: tax
l013: ld [x + 14]
l014: jeq #0x47455420 , l024 , l025
l015: jeq #0x86dd , l016 , l025
l016: ldb [20]
l017: jeq #0x6 , l018 , l025
l018: ldb [66]
l019: and #0xf0
l020: rsh #0x2
l021: tax
l022: ld [x + 54]
l023: jeq #0x47455420 , l024 , l025
l024: ret #262144
l025: ret #0"

# OpenSSL Hearbleed
tst "B91" \
"tcp src port 443 
and (tcp[((tcp[12] & 0xF0) >> 4) * 4] = 0x18)
and (tcp[((tcp[12] & 0xF0) >> 4) * 4 + 1] = 0x03)
and (tcp[((tcp[12] & 0xF0) >> 4) * 4 + 2] < 0x04)
and ((ip[2:2] - 4 * (ip[0] & 0x0F)) - 4 * ((tcp[12] & 0xF0) >> 4) > 69)" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l059
l002: ldb [23]
l003: jeq #0x6 , l004 , l059
l004: ldh [20]
l005: jset #0x1fff , l059 , l006
l006: ldxb 4 * ([14] & 0xf)
l007: ldh [x + 14]
l008: jeq #0x1bb , l009 , l059
l009: ldb [x + 26]
l010: and #0xf0
l011: rsh #0x4
l012: mul #0x4
l013: add #0x2
l014: ldxb 4 * ([14] & 0xf)
l015: add x
l016: tax
l017: ldb [x + 14]
l018: jge #0x4 , l059 , l019
l019: ldxb 4 * ([14] & 0xf)
l020: ldb [x + 26]
l021: and #0xf0
l022: rsh #0x4
l023: mul #0x4
l024: ldxb 4 * ([14] & 0xf)
l025: add x
l026: tax
l027: ldb [x + 14]
l028: jeq #0x18 , l029 , l059
l029: ldh [16]
l030: st M[1]
l031: ldb [14]
l032: and #0xf
l033: mul #0x4
l034: tax
l035: ld M[1]
l036: sub x
l037: st M[3]
l038: ldxb 4 * ([14] & 0xf)
l039: ldb [x + 26]
l040: and #0xf0
l041: mul #0x4
l042: rsh #0x4
l043: tax
l044: ld M[3]
l045: sub x
l046: jgt #0x45 , l047 , l059
l047: ldxb 4 * ([14] & 0xf)
l048: ldb [x + 26]
l049: and #0xf0
l050: rsh #0x4
l051: mul #0x4
l052: add #0x1
l053: ldxb 4 * ([14] & 0xf)
l054: add x
l055: tax
l056: ldb [x + 14]
l057: jeq #0x3 , l058 , l059
l058: ret #262144
l059: ret #0"

tst "B92" "ip6 protochain 17" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l088
l002: ldx #0x28
l003: ldb [20]
l004: jeq #0x11 , l087 , l005
l005: jeq #0x29 , l006 , l013
l006: ldb [x + 20]
l007: st M[15]
l008: ld #0x28
l009: add x
l010: tax
l011: ld M[15]
l012: jeq #0x11 , l087 , l013
l013: jeq #0x4 , l014 , l023
l014: ldb [x + 23]
l015: st M[15]
l016: ldb [x + 14]
l017: and #0xf
l018: mul #0x4
l019: add x
l020: tax
l021: ld M[15]
l022: jeq #0x11 , l087 , l023
l023: jeq #0x33 , l024 , l033
l024: ldb [x + 14]
l025: st M[15]
l026: ldb [x + 15]
l027: add #0x2
l028: mul #0x4
l029: add x
l030: tax
l031: ld M[15]
l032: jeq #0x11 , l087 , l033
l033: jeq #0x0 , l037 , l034
l034: jeq #0x3c , l037 , l035
l035: jeq #0x2b , l037 , l036
l036: jeq #0x2c , l037 , l046
l037: ldb [x + 14]
l038: st M[15]
l039: ldb [x + 15]
l040: add #0x1
l041: mul #0x8
l042: add x
l043: tax
l044: ld M[15]
l045: jeq #0x11 , l087 , l046
l046: jeq #0x29 , l047 , l054
l047: ldb [x + 20]
l048: st M[15]
l049: ld #0x28
l050: add x
l051: tax
l052: ld M[15]
l053: jeq #0x11 , l087 , l054
l054: jeq #0x4 , l055 , l064
l055: ldb [x + 23]
l056: st M[15]
l057: ldb [x + 14]
l058: and #0xf
l059: mul #0x4
l060: add x
l061: tax
l062: ld M[15]
l063: jeq #0x11 , l087 , l064
l064: jeq #0x33 , l065 , l074
l065: ldb [x + 14]
l066: st M[15]
l067: ldb [x + 15]
l068: add #0x2
l069: mul #0x4
l070: add x
l071: tax
l072: ld M[15]
l073: jeq #0x11 , l087 , l074
l074: jeq #0x0 , l078 , l075
l075: jeq #0x3c , l078 , l076
l076: jeq #0x2b , l078 , l077
l077: jeq #0x2c , l078 , l088
l078: ldb [x + 14]
l079: st M[15]
l080: ldb [x + 15]
l081: add #0x1
l082: mul #0x8
l083: add x
l084: tax
l085: ld M[15]
l086: jeq #0x11 , l087 , l088
l087: ret #262144
l088: ret #0"

tst "B92" "ip protochain 6" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l088
l002: ldb [23]
l003: ldxb 4 * ([14] & 0xf)
l004: jeq #0x6 , l087 , l005
l005: jeq #0x29 , l006 , l013
l006: ldb [x + 20]
l007: st M[15]
l008: ld #0x28
l009: add x
l010: tax
l011: ld M[15]
l012: jeq #0x6 , l087 , l013
l013: jeq #0x4 , l014 , l023
l014: ldb [x + 23]
l015: st M[15]
l016: ldb [x + 14]
l017: and #0xf
l018: mul #0x4
l019: add x
l020: tax
l021: ld M[15]
l022: jeq #0x6 , l087 , l023
l023: jeq #0x33 , l024 , l033
l024: ldb [x + 14]
l025: st M[15]
l026: ldb [x + 15]
l027: add #0x2
l028: mul #0x4
l029: add x
l030: tax
l031: ld M[15]
l032: jeq #0x6 , l087 , l033
l033: jeq #0x0 , l037 , l034
l034: jeq #0x3c , l037 , l035
l035: jeq #0x2b , l037 , l036
l036: jeq #0x2c , l037 , l046
l037: ldb [x + 14]
l038: st M[15]
l039: ldb [x + 15]
l040: add #0x1
l041: mul #0x8
l042: add x
l043: tax
l044: ld M[15]
l045: jeq #0x6 , l087 , l046
l046: jeq #0x29 , l047 , l054
l047: ldb [x + 20]
l048: st M[15]
l049: ld #0x28
l050: add x
l051: tax
l052: ld M[15]
l053: jeq #0x6 , l087 , l054
l054: jeq #0x4 , l055 , l064
l055: ldb [x + 23]
l056: st M[15]
l057: ldb [x + 14]
l058: and #0xf
l059: mul #0x4
l060: add x
l061: tax
l062: ld M[15]
l063: jeq #0x6 , l087 , l064
l064: jeq #0x33 , l065 , l074
l065: ldb [x + 14]
l066: st M[15]
l067: ldb [x + 15]
l068: add #0x2
l069: mul #0x4
l070: add x
l071: tax
l072: ld M[15]
l073: jeq #0x6 , l087 , l074
l074: jeq #0x0 , l078 , l075
l075: jeq #0x3c , l078 , l076
l076: jeq #0x2b , l078 , l077
l077: jeq #0x2c , l078 , l088
l078: ldb [x + 14]
l079: st M[15]
l080: ldb [x + 15]
l081: add #0x1
l082: mul #0x8
l083: add x
l084: tax
l085: ld M[15]
l086: jeq #0x6 , l087 , l088
l087: ret #262144
l088: ret #0"

tst "B93" "icmp protochain 6" \
"l000: ldh [12]
l001: jeq #0x800 , l002 , l014
l002: ldb [23]
l003: jeq #0x1 , l004 , l014
l004: ldxb 4 * ([14] & 0xf)
l005: ldb [x + 14]
l006: jeq #0x5 , l010 , l007
l007: jeq #0xb , l010 , l008
l008: jeq #0xc , l010 , l009
l009: jeq #0x3 , l010 , l014
l010: ldxb 4 * ([14] & 0xf)
l011: ldb [x + 31]
l012: jeq #0x6 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B94" "icmp6 protochain 17" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l103
l002: ldb [20]
l003: jeq #0x3a , l004 , l103
l004: ldx #0x30
l005: ldb [54]
l006: jeq #0x1 , l013 , l007
l007: jeq #0x2 , l013 , l008
l008: jeq #0x3 , l013 , l009
l009: jeq #0x4 , l013 , l010
l010: ldx #0x58
l011: ldb [54]
l012: jeq #0x89 , l013 , l020
l013: ldb [x + 20]
l014: st M[15]
l015: ld #0x28
l016: add x
l017: tax
l018: ld M[15]
l019: jeq #0x11 , l102 , l020
l020: jeq #0x29 , l021 , l028
l021: ldb [x + 20]
l022: st M[15]
l023: ld #0x28
l024: add x
l025: tax
l026: ld M[15]
l027: jeq #0x11 , l102 , l028
l028: jeq #0x4 , l029 , l038
l029: ldb [x + 23]
l030: st M[15]
l031: ldb [x + 14]
l032: and #0xf
l033: mul #0x4
l034: add x
l035: tax
l036: ld M[15]
l037: jeq #0x11 , l102 , l038
l038: jeq #0x33 , l039 , l048
l039: ldb [x + 14]
l040: st M[15]
l041: ldb [x + 15]
l042: add #0x2
l043: mul #0x4
l044: add x
l045: tax
l046: ld M[15]
l047: jeq #0x11 , l102 , l048
l048: jeq #0x0 , l052 , l049
l049: jeq #0x3c , l052 , l050
l050: jeq #0x2b , l052 , l051
l051: jeq #0x2c , l052 , l061
l052: ldb [x + 14]
l053: st M[15]
l054: ldb [x + 15]
l055: add #0x1
l056: mul #0x8
l057: add x
l058: tax
l059: ld M[15]
l060: jeq #0x11 , l102 , l061
l061: jeq #0x29 , l062 , l069
l062: ldb [x + 20]
l063: st M[15]
l064: ld #0x28
l065: add x
l066: tax
l067: ld M[15]
l068: jeq #0x11 , l102 , l069
l069: jeq #0x4 , l070 , l079
l070: ldb [x + 23]
l071: st M[15]
l072: ldb [x + 14]
l073: and #0xf
l074: mul #0x4
l075: add x
l076: tax
l077: ld M[15]
l078: jeq #0x11 , l102 , l079
l079: jeq #0x33 , l080 , l089
l080: ldb [x + 14]
l081: st M[15]
l082: ldb [x + 15]
l083: add #0x2
l084: mul #0x4
l085: add x
l086: tax
l087: ld M[15]
l088: jeq #0x11 , l102 , l089
l089: jeq #0x0 , l093 , l090
l090: jeq #0x3c , l093 , l091
l091: jeq #0x2b , l093 , l092
l092: jeq #0x2c , l093 , l103
l093: ldb [x + 14]
l094: st M[15]
l095: ldb [x + 15]
l096: add #0x1
l097: mul #0x8
l098: add x
l099: tax
l100: ld M[15]
l101: jeq #0x11 , l102 , l103
l102: ret #262144
l103: ret #0"

tst "B95" "inbound" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B96" "outbound" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B97" "inbound && tcp" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l013
l002: ldh [12]
l003: jeq #0x800 , l004 , l006
l004: ldb [23]
l005: jeq #0x6 , l012 , l013
l006: jeq #0x86dd , l007 , l013
l007: ldb [20]
l008: jeq #0x6 , l012 , l009
l009: jeq #0x2c , l010 , l013
l010: ldb [54]
l011: jeq #0x6 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B98" "outbound && udp" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l013
l002: ldh [12]
l003: jeq #0x800 , l004 , l006
l004: ldb [23]
l005: jeq #0x11 , l012 , l013
l006: jeq #0x86dd , l007 , l013
l007: ldb [20]
l008: jeq #0x11 , l012 , l009
l009: jeq #0x2c , l010 , l013
l010: ldb [54]
l011: jeq #0x11 , l012 , l013
l012: ret #262144
l013: ret #0"

tst "B99" "outbound && (tcp port 80 or 443)" \
"l000: ldh [-4092]
l001: jeq #0x4 , l002 , l025
l002: ldh [12]
l003: jeq #0x800 , l004 , l015
l004: ldb [23]
l005: jeq #0x6 , l006 , l025
l006: ldh [20]
l007: jset #0x1fff , l025 , l008
l008: ldxb 4 * ([14] & 0xf)
l009: ldh [x + 14]
l010: jeq #0x50 , l024 , l011
l011: jeq #0x1bb , l024 , l012
l012: ldh [x + 16]
l013: jeq #0x50 , l024 , l014
l014: jeq #0x1bb , l024 , l025
l015: jeq #0x86dd , l016 , l025
l016: ldb [20]
l017: jeq #0x6 , l018 , l025
l018: ldh [54]
l019: jeq #0x50 , l024 , l020
l020: jeq #0x1bb , l024 , l021
l021: ldh [56]
l022: jeq #0x50 , l024 , l023
l023: jeq #0x1bb , l024 , l025
l024: ret #262144
l025: ret #0"

tst "B100" "broadcast" \
"l000: ld [2]
l001: jeq #0xffffffff , l002 , l005
l002: ldh [0]
l003: jeq #0xffff , l004 , l005
l004: ret #262144
l005: ret #0"

tst "B101" "multicast" \
"l000: ldb [0]
l001: jeq #0x1 , l002 , l003
l002: ret #262144
l003: ret #0"

tst "B102" "mpls && mpls 1024 && ip" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l014
l002: ldb [16]
l003: jset #0x1 , l014 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l014
l007: ldb [20]
l008: and #0x1
l009: jeq #0x1 , l010 , l014
l010: ldb [22]
l011: and #0xf0
l012: jeq #0x40 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B103" "mpls && mpls 1024 && ip6" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l014
l002: ldb [16]
l003: jset #0x1 , l014 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l014
l007: ldb [20]
l008: and #0x1
l009: jeq #0x1 , l010 , l014
l010: ldb [22]
l011: and #0xf0
l012: jeq #0x60 , l013 , l014
l013: ret #262144
l014: ret #0"

tst "B104" "mpls && mpls 1024 && (tcp || udp)" \
"l000: ldh [12]
l001: jeq #0x8847 , l002 , l025
l002: ldb [16]
l003: jset #0x1 , l025 , l004
l004: ld [18]
l005: and #0xfffff000
l006: jeq #0x400000 , l007 , l025
l007: ldb [20]
l008: and #0x1
l009: jeq #0x1 , l010 , l025
l010: ldb [22]
l011: and #0xf0
l012: jeq #0x60 , l013 , l018
l013: ldb [28]
l014: jeq #0x6 , l024 , l015
l015: jeq #0x2c , l016 , l021
l016: ldb [62]
l017: jeq #0x6 , l024 , l022
l018: jeq #0x40 , l019 , l025
l019: ldb [31]
l020: jeq #0x6 , l024 , l023
l021: jeq #0x11 , l024 , l025
l022: jeq #0x11 , l024 , l025
l023: jeq #0x11 , l024 , l025
l024: ret #262144
l025: ret #0"

tst "B105" "ether proto \ip6 && ip6 src host fd3f:f209:c711::1" \
"l000: ldh [12]
l001: jeq #0x86dd , l002 , l011
l002: ld [22]
l003: jeq #0xfd3ff209 , l004 , l011
l004: ld [26]
l005: jeq #0xc7110000 , l006 , l011
l006: ld [30]
l007: jeq #0x0 , l008 , l011
l008: ld [34]
l009: jeq #0x1 , l010 , l011
l010: ret #262144
l011: ret #0"
