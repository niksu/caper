#!/bin/bash
#
# Test consists of checking that disambiguating an expression returns the
#   expected result.
# Nik Sultana, October 2022.
#
# NOTE error output from Caper is sent to /dev/null

CAPER="$1"
QUERY="$2"
EXPECTATION="$3"
TEST_ID="$4"

if [[ "${CAPER}" = "" || "${QUERY}" = "" || "${EXPECTATION}" = "" ]]
then
  echo "Need to specify path to Caper, a query, and the expected result." >&2
  exit 1
fi

TEMP=TEMP_${RANDOM}
# FIXME check that ${TEMP} doesn't exist

RESULT=`echo "${QUERY}" | ${CAPER} 2> ${TEMP}`

ERROR_OUT=`cat ${TEMP}`
rm ${TEMP}
if [[ "${ERROR_OUT}" != "" ]]
then
  ERROR_OUT=". stderr output: '${ERROR_OUT}'"
fi

# FIXME in event the EXPECTATION matches RESULT, check anyway if ERROR_OUT is not empty, in which case show them to user.
[[ "${EXPECTATION}" = "${RESULT}" ]] && echo "${TEST_ID}: OK : ${QUERY}${ERROR_OUT}" || echo "${TEST_ID}: FAIL (${QUERY}) : Expected '${EXPECTATION}' but got '${RESULT}'${ERROR_OUT}"
