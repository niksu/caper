#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for pretty printing
# Nik Sultana, August 2015
#
# NOTE this is based on the contents of tests/syntax_regression.sh
#
# FIXME port over more tests from tests/syntax_regression.sh

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 3 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "P1" "host foo" "ether proto \arp && arp src or dst host foo
  || ether proto \ip && ip src or dst host foo
  || ether proto \ip6 && ip6 src or dst host foo
  || ether proto \rarp && rarp src or dst host foo"

tst "P2" "net 128.3" "ether proto \arp && arp src or dst net 128.3
  || ether proto \ip && ip src or dst net 128.3
  || ether proto \rarp && rarp src or dst net 128.3"

tst "P3" "port 20" "    ether proto \ip
    && 
    (ip proto \sctp && sctp src or dst port 20
      || ip proto \tcp && tcp src or dst port 20
      || ip proto \udp && udp src or dst port 20)
  || 
    ether proto \ip6
    && 
    (ip6 proto \sctp && sctp src or dst port 20
      || ip6 proto \tcp && tcp src or dst port 20
      || ip6 proto \udp && udp src or dst port 20)"

tst "P4" "portrange 6000-6008" "    ether proto \ip
    && 
    (ip proto \sctp && sctp src or dst portrange 6000-6008
      || ip proto \tcp && tcp src or dst portrange 6000-6008
      || ip proto \udp && udp src or dst portrange 6000-6008)
  || 
    ether proto \ip6
    && 
    (ip6 proto \sctp && sctp src or dst portrange 6000-6008
      || ip6 proto \tcp && tcp src or dst portrange 6000-6008
      || ip6 proto \udp && udp src or dst portrange 6000-6008)"

tst "P5" "src foo" "ether proto \arp && arp src host foo
  || ether proto \ip && ip src host foo
  || ether proto \ip6 && ip6 src host foo
  || ether proto \rarp && rarp src host foo"

tst "P6" "dst net 128.3" "ether proto \arp && arp dst net 128.3
  || ether proto \ip && ip dst net 128.3
  || ether proto \rarp && rarp dst net 128.3"

tst "P7" "src or dst port ftp-data" "    ether proto \ip
    && 
    (ip proto \sctp && sctp src or dst port 22
      || ip proto \tcp && tcp src or dst port 22
      || ip proto \udp && udp src or dst port 22)
  || 
    ether proto \ip6
    && 
    (ip6 proto \sctp && sctp src or dst port 22
      || ip6 proto \tcp && tcp src or dst port 22
      || ip6 proto \udp && udp src or dst port 22)"

tst "P8" "ether src foo" "ether src host foo"

tst "P9" "arp net 128.3" "ether proto \arp && arp src or dst net 128.3"

tst "P10" "tcp port 21" "  (ether proto \ip && ip proto \tcp
    || ether proto \ip6 && ip6 proto \tcp)
  && 
  tcp src or dst port 21"

tst "P11" "udp portrange 7000-7009" "  (ether proto \ip && ip proto \udp
    || ether proto \ip6 && ip6 proto \udp)
  && 
  udp src or dst portrange 7000-7009"

tst "P17" "ip src foo || arp src foo || rarp src foo" "ether proto \arp && arp src host foo
  || ether proto \ip && ip src host foo
  || ether proto \rarp && rarp src host foo"

tst "P18" "net bar" "ether proto \arp && arp src or dst net bar
  || ether proto \ip && ip src or dst net bar
  || ether proto \ip6 && ip6 src or dst net bar
  || ether proto \rarp && rarp src or dst net bar"

tst "P19" "ip net bar || arp net bar || rarp net bar" "ether proto \arp && arp src or dst net bar
  || ether proto \ip && ip src or dst net bar
  || ether proto \rarp && rarp src or dst net bar"

tst "P20" "port 53" "    ether proto \ip
    && 
    (ip proto \sctp && sctp src or dst port 53
      || ip proto \tcp && tcp src or dst port 53
      || ip proto \udp && udp src or dst port 53)
  || 
    ether proto \ip6
    && 
    (ip6 proto \sctp && sctp src or dst port 53
      || ip6 proto \tcp && tcp src or dst port 53
      || ip6 proto \udp && udp src or dst port 53)"
