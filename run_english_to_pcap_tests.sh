#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt pretty printer.
# Marelle Leon, March 2023

# FIXME sensitive to the directory in which this is run
tests/english_to_pcap_regression.sh "./caper.byte -1stdin -q -engl-in -not_expand" tests/disambiguate_test.sh
