(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Parser spec for pcap
  Target parser-generator: menhir 20140422
*)

%token <string> IDENTIFIER
%token <string> ESCAPED_IDENTIFIER
%token <string> HEX

%token <string * string option * string option * string option> IPv4_NETWORK
%token <string * string * string * string> IPv4_ADDRESS
%token <string> IPv6_ADDRESS
%token <string * string> IPv6_NETWORK
%token <string> MAC_ADDRESS
(*
%token <int> PORT_VALUE
%token <int * int> PORT_RANGE_VALUE
*)

(*
%token NLNL
*)

(*Punctuation*)
%token COLON
%token LEFT_R_BRACKET
%token RIGHT_R_BRACKET
%token LEFT_S_BRACKET
%token RIGHT_S_BRACKET
%token DASH
%token GT
%token LT
%token GTE
%token LTE
%token EQUALS
%token NEQUALS
%token EOF

(*Reserved words*)
%token HOST
%token NET
%token PORT
%token PORTRANGE
%token MASK
(*dir*)
%token SRC
%token DST
%token SRC_OR_DST
%token SRC_AND_DST
%token INBOUND
%token OUTBOUND

(*proto*)
%token ETHER

%token FDDI
%token TR
%token WLAN
%token DECNET

%token IP
%token IP6
%token ARP
%token RARP
%token TCP
%token UDP
%token SCTP
(*more*)
%token GATEWAY
%token LESS
%token GREATER
%token LEN
%token BROADCAST
%token MULTICAST
%token PROTO
%token PROTOCHAIN
(*
%token ATALK
%token AARP
%token SCA
%token LAT
%token MOPDL
%token MOPRC
%token ISO
%token STP
%token IPX
*)
%token VLAN
%token MPLS
(*
%token PPPOED
%token PPPOES
%token CLNP
%token ESIS
%token ISIS
*)
%token ICMP
%token ICMP6
(*
%token IGMP
%token IGRP
%token PIM
%token AH
%token ESP
%token VRRP

%token FTP
%token FTP_DATA
*)
%token VXLAN
%token GENEVE
%token PPPOES
%token PPPOED

%token PERCENT
%token PLUS
%token QUOTIENT
%token ASTERISK
%token AND
%token NOT
%token OR
%token TRUE
%token FALSE

%token EXTENSION_OPEN
%token EXTENSION_CLOSE

%token SHIFT_RIGHT
%token SHIFT_LEFT
%token BINARY_AND
%token BINARY_OR

%token CARET
(*
%nonassoc brackets2
%nonassoc brackets1
*)
%right OR
%right AND
%nonassoc NOT
(*
%nonassoc EQUALS
%nonassoc GT LT
*)
%nonassoc DASH
%right PLUS
%nonassoc QUOTIENT
%right ASTERISK

%nonassoc PERCENT
%nonassoc CARET

%nonassoc SHIFT_RIGHT
%nonassoc SHIFT_LEFT
%right BINARY_OR
%right BINARY_AND

%start <Pcap_syntax.pcap_expression> pcap_main
%%

typ:
  | HOST; id = IDENTIFIER;
    {((None, None, Some Pcap_syntax.Host), Pcap_syntax.String id)}
  | HOST; id = ESCAPED_IDENTIFIER;
    {let id' = String.sub id 1 (String.length id - 1)
     in ((None, None, Some Pcap_syntax.Host), Pcap_syntax.Escaped_String id')}
(*  | HOST; id = DOTTED_INT;
    {((None, None, Some [Pcap_syntax.Host]), Pcap_syntax.Only ((), id))}*)
(*  | PORT; i = NAT;*)
  | PORT; id = ESCAPED_IDENTIFIER;
    (*FIXME check value of "id"*)
    {((None, None, Some Pcap_syntax.Port_type),
     Pcap_syntax.Port id)}
  | PORT; id = IDENTIFIER;
    {(*let port_number = int_of_string id in
     assert (port_number <= 65535);*)
     ((None, None, Some Pcap_syntax.Port_type),
     Pcap_syntax.Port id)}
  | PORT; id = HEX;
    {(*let port_number = int_of_string id in
     assert (port_number <= 65535);*)
     ((None, None, Some Pcap_syntax.Port_type),
     Pcap_syntax.Port (Pcap_syntax_aux.normalize_nonnegative_integer_s id))}

(*
  (*FIXME currently not supporting named ports -- e.g., "port ftp"*)
  | PORT; port_number = NATURAL;
    {assert (port_number <= 65535);
     ((None, None, Some Pcap_syntax.Port_type),
     Pcap_syntax.Port (string_of_int port_number))}
*)
  | PORTRANGE; range = IDENTIFIER
    {let split sep s =
       let result = ref [] in
       let buffer = ref "" in
       for i = 0 to String.length s - 1 do
          let this = String.sub s i 1 in
          if this = sep then
            begin
              result := !buffer :: !result;
              buffer := ""
            end
          else
            buffer := !buffer ^ this
       done;
       result := !buffer :: !result;
       List.rev !result in
     let p1, p2 =
       match split "-" range with
       | [p1; p2] -> p1, p2
       | _ ->
         failwith (Aux.error_output_prefix ^ ": " ^ "Failed to parse port range") in
(*
     let from_port = int_of_string p1 in
     let until_port = int_of_string p2 in
     assert (from_port <= 65535);
     assert (until_port <= 65535);
*)
     ((None, None, Some Pcap_syntax.Portrange),
(*     Pcap_syntax.Port_Range (from_port, until_port))}*)
     Pcap_syntax.Port_Range (p1, p2))}
  | PORTRANGE; p1 = IDENTIFIER; DASH; p2 = IDENTIFIER
    {(*let from_port = int_of_string p1 in
     let until_port = int_of_string p2 in
     assert (from_port <= 65535);
     assert (until_port <= 65535);
*)
     ((None, None, Some Pcap_syntax.Portrange),
(*     Pcap_syntax.Port_Range (from_port, until_port))}*)
     Pcap_syntax.Port_Range (p1, p2))}

  | GATEWAY; id = ESCAPED_IDENTIFIER;
    {((None, None, Some Pcap_syntax.Gateway),
     Pcap_syntax.Port id)}
  | GATEWAY; id = IDENTIFIER;
    {((None, None, Some Pcap_syntax.Gateway),
     Pcap_syntax.Port id)}

  | PROTOCHAIN; id = ESCAPED_IDENTIFIER;
    {((None, None, Some Pcap_syntax.Protochain),
     Pcap_syntax.Escaped_String id)}
  | PROTOCHAIN; id = IDENTIFIER;
    {(*let port_number = int_of_string id in
     assert (port_number <= 65535);*)
     ((None, None, Some Pcap_syntax.Protochain),
     Pcap_syntax.String id)}

  (*FIXME restrict "id" to recognised protocols*)
  | PROTO; id = IDENTIFIER;
    {((None, None, Some Pcap_syntax.Proto), Pcap_syntax.String id)}
  | PROTO; id = ESCAPED_IDENTIFIER;
    {let id' = String.sub id 1 (String.length id - 1)
     in ((None, None, Some Pcap_syntax.Proto), Pcap_syntax.Escaped_String id')}
  | PROTO; id = HEX;
    {((None, None, Some Pcap_syntax.Proto),
     Pcap_syntax.String (Pcap_syntax_aux.normalize_nonnegative_integer_s id))}

  | HOST; id = IPv6_ADDRESS;
    { let raw_address = Pcap_syntax.IPv6_Address (id, "") in
      let address =
        match Pcap_syntax_aux.ipv6_expand raw_address with
        | (Pcap_syntax.IPv6_Address (_, expanded_addr)) as address ->
            if not !Config.expand_ipv6_addresses then address else Pcap_syntax.IPv6_Address (expanded_addr, expanded_addr)
        | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Parsing IPv6 address: Impossible") in
      ((None, None, Some Pcap_syntax.Host), address)}
  | HOST; id = MAC_ADDRESS;
    {((None, None, Some Pcap_syntax.Host), Pcap_syntax.MAC_Address id)}
  | NET; net_params = IPv6_NETWORK
    { let raw_address = Pcap_syntax.IPv6_Network (fst net_params, "", snd net_params) in
      let address =
        match Pcap_syntax_aux.ipv6_expand raw_address with
        | (Pcap_syntax.IPv6_Network (_, expanded_addr, prefix_len)) as address ->
            if not !Config.expand_ipv6_addresses then address else Pcap_syntax.IPv6_Network (expanded_addr, expanded_addr, prefix_len)
        | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Parsing IPv6 network: Impossible") in
      ((None, None, Some Pcap_syntax.Net), address)}

  | NET; id = IDENTIFIER;
    {((None, None, Some Pcap_syntax.Net), Pcap_syntax.String id)}
  | NET; id = ESCAPED_IDENTIFIER;
    {let id' = String.sub id 1 (String.length id - 1)
     in ((None, None, Some Pcap_syntax.Net), Pcap_syntax.Escaped_String id')}

  | HOST; address = IPv4_ADDRESS
    {let (s1, s2, s3, s4) = address in
     let o1 = Pcap_syntax_aux.normalize_nonnegative_integer s1 in
     let o2 = Pcap_syntax_aux.normalize_nonnegative_integer s2 in
     let o3 = Pcap_syntax_aux.normalize_nonnegative_integer s3 in
     let o4 = Pcap_syntax_aux.normalize_nonnegative_integer s4 in
     assert (o1 <= 255);
     assert (o2 <= 255);
     assert (o3 <= 255);
     assert (o4 <= 255);
     ((None, None, Some Pcap_syntax.Host),
      Pcap_syntax.IPv4_Address (o1, o2, o3, o4))}

  | NET; net = IPv4_NETWORK; QUOTIENT; mask_size = IDENTIFIER
    {let o1, o2, o3, o4 =
       match net with
       | (s1, Some s2, Some s3, Some ("0" as s4)) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), Some (Pcap_syntax_aux.normalize_nonnegative_integer s3), Some (Pcap_syntax_aux.normalize_nonnegative_integer s4)
       | (s1, Some s2, Some s3, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), Some (Pcap_syntax_aux.normalize_nonnegative_integer s3), None
       | (s1, Some s2, None, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), None, None
       | (s1, None, None, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, None, None, None
       | _ ->
         failwith (Aux.error_output_prefix ^ ": " ^ "Failed to parse IPv4 network") in
     let mask_size = Pcap_syntax_aux.normalize_nonnegative_integer mask_size in
     assert (mask_size <= 32);
(*FIXME bind_opt oX
     assert (o1 <= 255);
     assert (o2 <= 255);
     assert (o3 <= 255);
     assert (o4 <= 255);
*)
     ((None, None, Some Pcap_syntax.Net),
      Pcap_syntax.IPv4_Network (o1, o2, o3, o4, Some mask_size))}

  | NET; net = IPv4_NETWORK
    {let o1, o2, o3, o4 =
       match net with
       | (s1, Some s2, Some s3, Some s4) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), Some (Pcap_syntax_aux.normalize_nonnegative_integer s3), Some (Pcap_syntax_aux.normalize_nonnegative_integer s4)
       | (s1, Some s2, Some s3, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), Some (Pcap_syntax_aux.normalize_nonnegative_integer s3), None
       | (s1, Some s2, None, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), None, None
       | (s1, None, None, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, None, None, None
       | _ ->
         failwith (Aux.error_output_prefix ^ ": " ^ "Failed to parse IPv4 network") in
(*
(*FIXME bind_opt on oX*)
     assert (o1 <= 255);
     assert (o2 <= 255);
     assert (o3 <= 255);
     assert (o4 <= 255);
*)
     ((None, None, Some Pcap_syntax.Net),
      Pcap_syntax.IPv4_Network (o1, o2, o3, o4, None))}
  | NET; net = IPv4_NETWORK; MASK; mask = IPv4_NETWORK
(*
    {let o1, o2, o3, o4 = net in
     let o1 = int_of_string o1 in
     let o2 = int_of_string o2 in
     let o3 = int_of_string o3 in
     let o4 = int_of_string o4 in
*)
    {let o1, o2, o3, o4 =
       match net with
       | (s1, Some s2, Some s3, Some s4) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), Some (Pcap_syntax_aux.normalize_nonnegative_integer s3), Some (Pcap_syntax_aux.normalize_nonnegative_integer s4)
       | (s1, Some s2, Some s3, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), Some (Pcap_syntax_aux.normalize_nonnegative_integer s3), None
       | (s1, Some s2, None, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, Some (Pcap_syntax_aux.normalize_nonnegative_integer s2), None, None
       | (s1, None, None, None) ->
         Pcap_syntax_aux.normalize_nonnegative_integer s1, None, None, None
       | _ ->
         failwith (Aux.error_output_prefix ^ ": " ^ "Failed to parse IPv4 network") in
(*
     assert (o1 <= 255);
     assert (o2 <= 255);
     assert (o3 <= 255);
     assert (o4 <= 255);
*)
     let mask =
(*
       let o1, o2, o3, o4 = mask in
       assert (int_of_string o1 <= 255);
       assert (int_of_string o2 <= 255);
       assert (int_of_string o3 <= 255);
       assert (int_of_string o4 <= 255);
       o1 ^ "." ^ o2 ^ "." ^ o3 ^ "." ^ o4 in
*)
       match mask with
       | (s1, Some s2, Some s3, Some s4) ->
         s1 ^ "." ^ s2 ^ "." ^ s3 ^ "." ^ s4
       | (s1, Some s2, Some s3, None) ->
         s1 ^ "." ^ s2 ^ "." ^ s3
       | (s1, Some s2, None, None) ->
         s1 ^ "." ^ s2
       | (s1, None, None, None) ->
         s1
       | _ ->
         failwith (Aux.error_output_prefix ^ ": " ^ "Failed to parse IPv4 network mask") in
     ((None, None, Some Pcap_syntax.Net),
      Pcap_syntax.IPv4_Network_Masked (o1, o2, o3, o4, mask))}


  | NET; net = IDENTIFIER; QUOTIENT; mask_size = IDENTIFIER
    {let o1, o2, o3, o4 =
         Pcap_syntax_aux.normalize_nonnegative_integer net, None, None, None in
     let mask_size = Pcap_syntax_aux.normalize_nonnegative_integer mask_size in
     assert (mask_size <= 32);
     ((None, None, Some Pcap_syntax.Net),
      Pcap_syntax.IPv4_Network (o1, o2, o3, o4, Some mask_size))}
  | NET; net = IDENTIFIER; MASK; mask = IPv4_NETWORK
    {let o1, o2, o3, o4 =
         Pcap_syntax_aux.normalize_nonnegative_integer net, None, None, None in
     let mask =
       match mask with
       | (s1, Some s2, Some s3, Some s4) ->
         s1 ^ "." ^ s2 ^ "." ^ s3 ^ "." ^ s4
       | (s1, Some s2, Some s3, None) ->
         s1 ^ "." ^ s2 ^ "." ^ s3
       | (s1, Some s2, None, None) ->
         s1 ^ "." ^ s2
       | (s1, None, None, None) ->
         s1
       | _ ->
         failwith (Aux.error_output_prefix ^ ": " ^ "Failed to parse IPv4 network") in
     ((None, None, Some Pcap_syntax.Net),
      Pcap_syntax.IPv4_Network_Masked (o1, o2, o3, o4, mask))}

  | LESS; id = IDENTIFIER;
    {let size = Pcap_syntax_aux.normalize_nonnegative_integer id in
     assert (size <= 65535);
     ((None, None, Some Pcap_syntax.Less),
     Pcap_syntax.String id)}

  | GREATER; id = IDENTIFIER;
    {let size = Pcap_syntax_aux.normalize_nonnegative_integer id in
     assert (size <= 65535);
     ((None, None, Some Pcap_syntax.Greater),
     Pcap_syntax.String id)}

(*
  | IPv4_Network of int * int * int * int * mask_size
  | IPv4_Network_Masked of int * int * int * int * mask
*)

(*  | NET*)
(*  | MASK*)

proto:
(*FIXME proto followed by dir or primitive*)
(*
  | ETHER; pid = typ
    {Pcap_syntax.add_proto Pcap_syntax.Ether pid}
(*((Some [Pcap_syntax.Ether], None, None), Pcap_syntax.Nothing)}*)
  | ETHER; pid = direction
    {Pcap_syntax.add_proto Pcap_syntax.Ether pid}
(*    {((Some [Pcap_syntax.Ether], None, None), Pcap_syntax.Nothing)}*)
  | ETHER
    {((Some [Pcap_syntax.Ether], None, None), Pcap_syntax.Nothing)}
*)
  | ETHER
    {Pcap_syntax_aux.set_proto Pcap_syntax.Ether}

  | FDDI
    {(*Pcap_syntax_aux.set_proto Pcap_syntax.Fddi*)
      failwith "Unsupported protocol: FDDI"
    }
  | TR
    {(*Pcap_syntax_aux.set_proto Pcap_syntax.Tr*)
      failwith "Unsupported protocol: TR"
    }
  | WLAN
    {(*Pcap_syntax_aux.set_proto Pcap_syntax.Wlan*)
      failwith "Unsupported protocol: WLAN"
    }
  | DECNET
    {(*Pcap_syntax_aux.set_proto Pcap_syntax.Decnet*)
      failwith "Unsupported protocol: DECNET"
    }

  | IP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Ip}
  | IP6
    {Pcap_syntax_aux.set_proto Pcap_syntax.Ip6}
  | ARP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Arp}
  | RARP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Rarp}
  | TCP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Tcp}
  | UDP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Udp}
  | SCTP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Sctp}
  | VLAN
    {Pcap_syntax_aux.set_proto Pcap_syntax.Vlan}
  | MPLS
    {Pcap_syntax_aux.set_proto Pcap_syntax.Mpls}
  | ICMP
    {Pcap_syntax_aux.set_proto Pcap_syntax.Icmp}
  | ICMP6
    {Pcap_syntax_aux.set_proto Pcap_syntax.Icmp6}
  | VXLAN
    {
      failwith "Unsupported protocol: VXLAN"
    }
  | GENEVE
    {
      failwith "Unsupported protocol: GENEVE"
    }
  | PPPOES
    {
      failwith "Unsupported protocol: PPPoE(S)"
    }
  | PPPOED
    {
      failwith "Unsupported protocol: PPPoE(D)"
    }

proto_string:
  | ETHER
    {"ether"}

  | FDDI
    {(*"fddi"*)
      failwith "Unsupported protocol: FDDI"
    }
  | TR
    {(*"tr"*)
      failwith "Unsupported protocol: TR"
    }
  | WLAN
    {(*"wlan"*)
      failwith "Unsupported protocol: WLAN"
    }
  | DECNET
    {(*"decnet"*)
      failwith "Unsupported protocol: DECNET"
    }
  | VXLAN
    {
      failwith "Unsupported protocol: VXLAN"
    }
  | GENEVE
    {
      failwith "Unsupported protocol: GENEVE"
    }
  | PPPOES
    {
      failwith "Unsupported protocol: PPPoE(S)"
    }
  | PPPOED
    {
      failwith "Unsupported protocol: PPPoE(D)"
    }

  | IP
    {"ip"}
  | IP6
    {"ip6"}
  | ARP
    {"arp"}
  | RARP
    {"rarp"}
  | TCP
    {"tcp"}
  | UDP
    {"udp"}
  | SCTP
    {"sctp"}
  | ICMP
    {"icmp"}
  | ICMP6
    {"icmp6"}

direction:
(*
  | SRC; pid = primitive_id
    {Pcap_syntax.set_dir Pcap_syntax.Src}
  | DST; pid = primitive_id
    {Pcap_syntax.set_dir Pcap_syntax.Dst}
  | SRC_OR_DST; pid = primitive_id
    {Pcap_syntax.set_dir Pcap_syntax.Src_or_dst}
  | SRC_AND_DST; pid = primitive_id
    {Pcap_syntax.set_dir Pcap_syntax.Src_and_dst}
*)
  | SRC
    {Pcap_syntax_aux.set_dir Pcap_syntax.Src}
  | DST
    {Pcap_syntax_aux.set_dir Pcap_syntax.Dst}
  | SRC_OR_DST
    {Pcap_syntax_aux.set_dir Pcap_syntax.Src_or_dst}
  | SRC_AND_DST
    {Pcap_syntax_aux.set_dir Pcap_syntax.Src_and_dst}
  | BROADCAST
    {(fun pid ->
      match pid with
      | ((None, None, None), Pcap_syntax.Nothing) ->
        Pcap_syntax_aux.set_dir Pcap_syntax.Broadcast pid
      | _ -> failwith "Invalid use of 'broadcast'")}
  | MULTICAST
    {(fun pid ->
      match pid with
      | ((None, None, None), Pcap_syntax.Nothing) ->
        Pcap_syntax_aux.set_dir Pcap_syntax.Multicast pid
      | _ -> failwith "Invalid use of 'multicast'")}

  | INBOUND
    {Pcap_syntax_aux.set_dir Pcap_syntax.Inbound}
  | OUTBOUND
    {Pcap_syntax_aux.set_dir Pcap_syntax.Outbound}

(*
  | SRC; t = typ
    {Pcap_syntax.set_dir Pcap_syntax.Src t}
  | DST; t = typ
    {Pcap_syntax.set_dir Pcap_syntax.Dst t}
  | SRC_OR_DST; t = typ
    {Pcap_syntax.set_dir Pcap_syntax.Src_or_dst t}
  | SRC_AND_DST; t = typ
    {Pcap_syntax.set_dir Pcap_syntax.Src_and_dst t}
*)

primitive_id:
  | id = IDENTIFIER;
    {((None, None, None), Pcap_syntax.String id)}
  | id = ESCAPED_IDENTIFIER;
    {let id' = String.sub id 1 (String.length id - 1)
     in ((None, None, None), Pcap_syntax.Escaped_String id')}
(*  | id = DOTTED_INT;
    {((None, None, None), Pcap_syntax.Only ((), id))}
  | id = HEX;
    {((None, None, None), Pcap_syntax.Only ((), id))}*)

primitive_atom:
  | pid = primitive_id
    {pid}
(*  | id = IDENTIFIER;
    {((None, None, None), Pcap_syntax.String id)}
  | id = ESCAPED_IDENTIFIER;
    {((None, None, None), Pcap_syntax.Escaped_String id)}*)
(*  | id = DOTTED_INT;
    {((None, None, None), Pcap_syntax.Only ((), id))}
  | id = HEX;
    {((None, None, None), Pcap_syntax.Only ((), id))}*)
  | t = typ;
    {t}
  | d = direction; t = typ;
    {d t}
  | d = direction; pid = primitive_id;
    {d pid}
  | d = direction;
    {d Pcap_syntax.empty}
(*  | d = direction; t = typ;
  | d = direction; DOTTED_INT; (*FIXME IPv4_ADDRESS*)
  | d = direction; IPv6_ADDRESS;
  | p = proto; d = direction; t = typ;
*)
  | p = proto; d = direction; t = typ
    {p (d t)}
  | p = proto; t = typ
    {let parsed = p t in
     if parsed = ((Some Pcap_syntax.Ether, None, Some Pcap_syntax.Proto), Pcap_syntax.Escaped_String "ether") then
       failwith (Aux.error_output_prefix ^ ": " ^ "Invalid pcap expression")
     else parsed}
  | p = proto; d = direction; pid = primitive_id
    {p (d pid)}
  | p = proto; d = direction
    {p (d Pcap_syntax.empty)}
  | p = proto; pid = primitive_id
    {p pid}
  | p = proto
    {p Pcap_syntax.empty}

(*FIXME is this more general than necessary?
        perhaps some forms of expression can only be made in Packet_Access*)
expression:
  | id = IDENTIFIER
    {let is_int x = x == '0' || x == '1' || x == '2' || x == '3' || x == '4' || x == '5' || x == '6' || x == '7' || x == '8' || x == '9' in
     let id_is_int = ref (false) in
     let len_left = ref (String.length id) in
     assert (!len_left > 0);
     while (!len_left > 0) do
       if not (is_int (String.get id (!len_left - 1))) then
          begin
            id_is_int := false;
            len_left := 0;
          end
       else id_is_int := true;
       len_left := !len_left - 1;
     done;
     if !id_is_int then Pcap_syntax.Nat_Literal (Pcap_syntax_aux.normalize_nonnegative_integer id)
     else Pcap_syntax.Identifier id}
  | v = HEX
    {Pcap_syntax.Hex_Literal v}
  | LEN {Pcap_syntax.Len}
  | LEFT_R_BRACKET; e = expression; RIGHT_R_BRACKET
    (*%prec brackets1*)
    {e}
  | e1 = expression; PLUS; e2 = expression
    {Pcap_syntax.Plus [e1; e2]}
  | e1 = expression; DASH; e2 = expression
    {Pcap_syntax.Minus (e1, e2)}
  | e1 = expression; ASTERISK; e2 = expression
    {Pcap_syntax.Times [e1; e2]}
  | e1 = expression; QUOTIENT; e2 = expression
    {Pcap_syntax.Quotient (e1, e2)}
  | e1 = expression; SHIFT_RIGHT; e2 = expression
    {Pcap_syntax.Shift_Right (e1, e2)}
  | e1 = expression; SHIFT_LEFT; e2 = expression
    {Pcap_syntax.Shift_Left (e1, e2)}
  | e1 = expression; BINARY_AND; e2 = expression
    {Pcap_syntax.Binary_And [e1; e2]}
  | e1 = expression; BINARY_OR; e2 = expression
    {Pcap_syntax.Binary_Or [e1; e2]}
  | e1 = expression; PERCENT; e2 = expression
    {Pcap_syntax.Mod (e1, e2)}
  | e1 = expression; CARET; e2 = expression
    {Pcap_syntax.Binary_Xor [e1; e2]}

  | id = proto_string; LEFT_S_BRACKET; e = expression; RIGHT_S_BRACKET
    {Pcap_syntax.Packet_Access (id, e, None)}
(*FIXME instead of IDENTIFIER use tokens for protocols: ether, ip, tcp, etc*)
  | id = proto_string; LEFT_S_BRACKET; e = expression; COLON; nb_id = IDENTIFIER; RIGHT_S_BRACKET
    {let nb =
       match nb_id with
       | "1" -> Pcap_syntax.One
       | "2" -> Pcap_syntax.Two
       | "4" -> Pcap_syntax.Four
       | _ ->
         failwith ("Packet_Access given incorrect no_bytes value: " ^ nb_id) in
     Pcap_syntax.Packet_Access (id, e, Some nb)}

relational_atom:
  | e1 = expression; GT; e2 = expression;
    {Pcap_syntax.Gt (e1, e2)}
  | e1 = expression; LT; e2 = expression;
    {Pcap_syntax.Lt (e1, e2)}
  | e1 = expression; GTE; e2 = expression;
    {Pcap_syntax.Geq (e1, e2)}
  | e1 = expression; LTE; e2 = expression;
    {Pcap_syntax.Leq (e1, e2)}
  | e1 = expression; EQUALS; e2 = expression;
    {Pcap_syntax.Eq (e1, e2)}
  | e1 = expression; NEQUALS; e2 = expression;
    {Pcap_syntax.Neq (e1, e2)}

pcap_main:
  | p = pcap_expression; EOF;
    {p}

pcap_expression:
  | pa = primitive_atom
    {Pcap_syntax.Primitive pa}
  | ra = relational_atom
    {Pcap_syntax.Atom ra}
  | NOT; pe = pcap_expression
    {Pcap_syntax.Not pe}
  | pe1 = pcap_expression; AND; pe2 = pcap_expression
    {Pcap_syntax.And [pe1; pe2]}
  | pe1 = pcap_expression; OR; pe2 = pcap_expression
    {Pcap_syntax.Or [pe1; pe2]}
  | EXTENSION_OPEN; TRUE; EXTENSION_CLOSE
    {Pcap_syntax.True}
  | EXTENSION_OPEN; FALSE; EXTENSION_CLOSE
    {Pcap_syntax.False}
  | LEFT_R_BRACKET; pe = pcap_expression; RIGHT_R_BRACKET
    (*%prec brackets2*)
    {pe}
