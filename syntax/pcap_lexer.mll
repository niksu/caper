(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Lexer for pcap expressions.
*)

{
open Lexing
open Pcap_parser
}

(*NOTE tabs are not recognised.*)
let ws = ' '+
let comment = "//"[^'\n']*
let decimal = ['0'-'9']
let natural = decimal+
let hex = ['a'-'f''A'-'F''0'-'9']
let colon = ':'
let period = '.'
let dash = '-'
let slash = '/'

let upto3nat = (decimal | decimal decimal | decimal decimal decimal)
let hex4 = hex hex hex hex
let upto4hex = (hex | hex hex | hex hex hex | hex4)
let ipv6_address = (upto4hex colon upto4hex colon upto4hex colon upto4hex colon upto4hex colon upto4hex colon upto4hex colon upto4hex | (upto4hex colon?)* colon colon (upto4hex colon?)*)

let colondashed_mac_address = hex hex (colon|dash) hex hex (colon|dash) hex hex (colon|dash) hex hex (colon|dash) hex hex (colon|dash) hex hex
let dotted_mac_address = hex4 period hex4 period hex4
let mac_address = colondashed_mac_address | dotted_mac_address

let ipv4_address = upto3nat period upto3nat period upto3nat period upto3nat

let mask = ipv4_address

(*NOTE currently only Unix-style newline is supported because it's simpler.*)
let nl = '\n'

rule main = parse
(*  | nl nl {NLNL}*)
  | nl {main lexbuf}
  | ws {main lexbuf}

  (*type*)
  | "host" {HOST} (*default*) (*defaults to ip*)
  | "net" {NET}
  | "port" {PORT}
  | "portrange" {PORTRANGE}

  | "mask" {MASK}

  (*dir*)
  | "src" {SRC}
  | "dst" {DST}
  | "src or dst" {SRC_OR_DST} (*default*)
  | "src and dst" {SRC_AND_DST}
  | "dst or src" {SRC_OR_DST}
  | "dst and src" {SRC_AND_DST}
  | "inbound" {INBOUND}
  | "outbound" {OUTBOUND}

  (*proto*)
  (*the default depends on the contents of the rest of the expression*)
  | "ether" {ETHER}

  | "fddi" {FDDI}
  | "tr" {TR}
  | "wlan" {WLAN}
  | "decnet" {DECNET}

  | "ip" {IP}
  | "ip6" {IP6}
  | "arp" {ARP}
  | "rarp" {RARP}
  | "tcp" {TCP}
  | "udp" {UDP}
  | "sctp" {SCTP}
  | "vlan" {VLAN}
  | "mpls" {MPLS}

  | "vxlan" {VXLAN}
  | "geneve" {GENEVE}
  | "pppoes" {PPPOES}
  | "pppoed" {PPPOED}

  | "gateway" {GATEWAY}
  | "less" {LESS}
  | "greater" {GREATER}
  | "len" {LEN}

  | "broadcast" {BROADCAST} (*defaults to ether*)
  | "multicast" {MULTICAST} (*defaults to ether*)

  | "proto" {PROTO}
  | "protochain" {PROTOCHAIN}
  (*NOTE these protocols are only availble as embedded under something else.
         FIXME make precise*)
(*
  | "atalk" {ATALK}
  | "aarp" {AARP}
  | "sca" {SCA}
  | "lat" {LAT}
  | "mopdl" {MOPDL}
  | "moprc" {MOPRC}
  | "iso" {ISO}
  | "stp" {STP}
  | "ipx" {IPX}

  | "pppoed" {PPPOED}
  | "pppoes" {PPPOES}

  (* NOTE only available within iso*)
  | "clnp" {CLNP}
  | "esis" {ESIS}
  | "isis" {ISIS}
*)
  (*NOTE available over IP.
         FIXME perhaps i shouldnt set these to be keywords*)
  | "icmp" {ICMP}
  | "icmp6" {ICMP6}
(* FIXME include the following too?
  | "igmp" {IGMP}
  | "igrp" {IGRP}
  | "pim" {PIM}
  | "ah" {AH}
  | "esp" {ESP}
  | "vrrp" {VRRP}
*)
  | "-" {DASH}
  | ":" {COLON}
  | "[" {LEFT_S_BRACKET}
  | "]" {RIGHT_S_BRACKET}
  | "(" {LEFT_R_BRACKET}
  | ")" {RIGHT_R_BRACKET}

  | "+" {PLUS}
  | "*" {ASTERISK}
  | "/" {QUOTIENT}
  | "%" {PERCENT}
  | "^" {CARET}
  | ">>" {SHIFT_RIGHT}
  | "<<" {SHIFT_LEFT}
  | "&" {BINARY_AND}
  | "|" {BINARY_OR}

  | ">" {GT}
  | "<" {LT}
  | ">=" {GTE}
  | "<=" {LTE}
  | "=" {EQUALS}
  | "==" {EQUALS}
  | "!=" {NEQUALS}

  | "and" {AND}
  | "or" {OR}
  | "not" {NOT}
  | "&&" {AND}
  | "||" {OR}
  | "!" {NOT}

  (*FIXME include protocol field types, for icmp, and tcp
from: http://www.winpcap.org/docs/docs_40_2/html/group__language.html
  Some offsets and field values may be expressed as names rather than as numeric values. The following protocol header field offsets are available: icmptype (ICMP type field), icmpcode (ICMP code field), and tcpflags (TCP flags field).
  The following ICMP type field values are available: icmp-echoreply, icmp-unreach, icmp-sourcequench, icmp-redirect, icmp-echo, icmp-routeradvert, icmp-routersolicit, icmp-timxceed, icmp-paramprob, icmp-tstamp, icmp-tstampreply, icmp-ireq, icmp-ireqreply, icmp-maskreq, icmp-maskreply.
  The following TCP flags field values are available: tcp-fin, tcp-syn, tcp-rst, tcp-push, tcp-push, tcp-ack, tcp-urg. 
*)

(*
  (*NOTE used for parameters to "port"*)
  | "ftp" {FTP}
  | "ftp-data" {FTP_DATA}
*)

  (*FIXME could put extension syntax under different lexer*)
  | "True" {TRUE}
  | "False" {FALSE}
  | "@:" {EXTENSION_OPEN}
  | ":@" {EXTENSION_CLOSE}

  | ipv6_address as address {IPv6_ADDRESS address}
  | mac_address as address {MAC_ADDRESS address}
  | (ipv6_address as address) slash (natural as length) {IPv6_NETWORK (address, length)}

  | (upto3nat as o1) period (upto3nat as o2) period (upto3nat as o3) period '0'
    {IPv4_NETWORK (o1, Some o2, Some o3, Some "0")}
  | (upto3nat as o1) period (upto3nat as o2) period (upto3nat as o3)
    {IPv4_NETWORK (o1, Some o2, Some o3, None)}
  | (upto3nat as o1) period (upto3nat as o2)
    {IPv4_NETWORK (o1, Some o2, None, None)}

  | (upto3nat as o1) period (upto3nat as o2) period (upto3nat as o3) period (upto3nat as o4) {IPv4_ADDRESS (o1, o2, o3, o4)}


  | "0x"['a'-'f''A'-'F''0'-'9']* as hex {HEX hex}
  | ['a'-'z''A'-'Z''0'-'9''_''.']['a'-'z''A'-'Z''0'-'9''_''.''-']* as id {IDENTIFIER id}
  | "\\"['a'-'z''A'-'Z''0'-'9''_''.''-']* as escaped_id {ESCAPED_IDENTIFIER escaped_id}

  | comment {main lexbuf}

  | eof {EOF}
