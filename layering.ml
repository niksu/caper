(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Expansion for pcap expressions
*)


open Pcap_syntax

(*Possible dependencies of a protocol. Returns list of protocols on which a
 given protocol can run. We only look one layer down, and call this function
 repetitively to derive all possible dependencies for a given protocol (and all
 the way down to the link protocol*)
let deps_of_proto (proto : proto) : proto list =
  match proto with
  | Ether -> []
  | Ip -> [Ether]
  | Ip6 -> [Ether]
  | Arp -> [Ether]
  | Rarp -> [Ether]
  | Tcp -> [Ip; Ip6]
  | Udp -> [Ip; Ip6]
  | Sctp -> [Ip; Ip6]
  (*Even though VLAN and MPLS go over Ethernet we do not spell this out here
    because of a quirk in libpcap:
      $ sudo tcpdump -i en5 -d "ether proto \vlan && vlan 100"
      tcpdump: unknown ether proto 'vlan'
  *)
  | Vlan -> []
  | Mpls -> []
  | Icmp -> [Ip]
  | Icmp6 -> [Ip6]
(*
  | p -> failwith (Aux.error_output_prefix ^ ": deps_of_proto: " ^ "Unsupported protocol: " ^ string_of_proto p)
*)

(*This doesn't actually report the "OSI layer", it simply orders headers relative
  to one another in a pragmatic way.*)
let layer_of_proto (proto : proto) : int =
  match proto with
  | Ether -> 2
  | Ip -> 4
  | Ip6 -> 4
  | Arp -> 4
  | Rarp -> 4
  | Tcp -> 5
  | Udp -> 5
  | Sctp -> 5
  | Vlan -> 3
  | Mpls -> 3
  | Icmp -> 5
  | Icmp6 -> 5
(*
  | p -> failwith (Aux.error_output_prefix ^ ": layer_of_proto: " ^ "Unsupported protocol: " ^ string_of_proto p)
*)
