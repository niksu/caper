(*
  Copyright Marelle Leon, April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module:  Translating Pcap expressions into English expressions
*)

open Pcap_to_english_spec
open English_syntax
open Pcap_syntax
open Pcap_syntax_aux
open Angstrom

let engl_term_of_value_atom v =
  let proto_engl_string_of_pcap_string_opt s =
    (match s with
    | "ether" -> Some Ether
    | "ip" -> Some Ip
    | "ip6" -> Some Ip6
    | "arp" -> Some Arp
    | "rarp" -> Some Rarp
    | "tcp" -> Some Tcp
    | "udp" -> Some Udp
    | "sctp" -> Some Sctp
    | "vlan" -> Some Vlan
    | "mpls" -> Some Mpls
    | "icmp" -> Some Icmp
    | "icmp6" -> Some Icmp6
    | _ -> None)
    |> Option.map engl_string_of_proto
  in
  let engl_str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) =
    let net_str =
      string_of_id_atom (IPv4_Network (i1, i2_opt, i3_opt, i4_opt, None))
    in
    match mask_size_opt with
    | Some mask_size ->
        OfSize_term ("length", "bits", net_str, string_of_int mask_size)
    | None -> Phrase net_str
  in
  let engl_str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask) =
    let net_str =
      string_of_id_atom (IPv4_Network (i1, i2_opt, i3_opt, i4_opt, None))
    in
    With_term ("mask", net_str, mask)
  in
  match v with
  | Escaped_String escaped_s -> (
      match proto_engl_string_of_pcap_string_opt escaped_s with
      | Some s -> Phrase s
      | None -> Phrase escaped_s)
  | IPv4_Network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt) ->
      engl_str_of_ipv4_network (i1, i2_opt, i3_opt, i4_opt, mask_size_opt)
  | IPv4_Network_Masked (i1, i2_opt, i3_opt, i4_opt, mask) ->
      engl_str_of_ipv4_network_masked (i1, i2_opt, i3_opt, i4_opt, mask)
  | _ -> Phrase (string_of_id_atom v)

let engl_clause_of_primitive (p : primitive) : (clause, string) result =
  let fld, v = p in
  let value = engl_term_of_value_atom v in
  match fld with
  | Some p, Some d, Some t ->
      Ok
        (AttributeIsThatOf
           ( [ Phrase (engl_string_of_dir d); Phrase (engl_string_of_typ t) ],
             Some (Phrase (engl_string_of_proto p)),
             value,
             Phrase "header" ))
      (* (ThatHas
         (
           Phrase (engl_string_of_proto p),
           [Phrase (engl_string_of_dir d); Phrase (engl_string_of_typ t)],
           value
         )) *)
  | Some p, None, Some t ->
      Ok
        (AttributeIsThatOf
           ( [ Phrase (engl_string_of_typ t) ],
             Some (Phrase (engl_string_of_proto p)),
             value,
             Phrase "header" ))
      (* (ThatHas
         (
           Phrase (engl_string_of_proto p),
           [Phrase (engl_string_of_typ t)],
           value
         )) *)
  | Some p, Some d, None -> (
      match d with
      | Broadcast | Multicast ->
          Ok
            (ThatIs
               (Phrase (engl_string_of_proto p), Phrase (engl_string_of_dir d)))
      | _ ->
          Ok
            (AttributeIsThatOf
               ( [ Phrase (engl_string_of_dir d) ],
                 Some (Phrase (engl_string_of_proto p)),
                 value,
                 Phrase "header" ))
          (* (ThatHas
             (
               Phrase (engl_string_of_proto p),
               [Phrase (engl_string_of_dir d)],
               value
             )) *))
  | Some p, None, None ->
      Ok (KeywordIsThatOf (Phrase "protocol", Phrase (engl_string_of_proto p)))
  | None, Some d, Some t ->
      Ok
        (AttributeIsThatOf
           ( [ Phrase (engl_string_of_dir d); Phrase (engl_string_of_typ t) ],
             None,
             value,
             Phrase "header" ))
      (* (IsThatOf
         (
           [Phrase (engl_string_of_dir d); Phrase (engl_string_of_typ t)],
           value
         )) *)
  | None, None, Some t -> (
      match t with
      | Less -> (
          match value with
          | Phrase val_s ->
              Ok
                (Relation_clause
                   ( Phrase "is less than or equal to",
                     Arithmetic ("len", []),
                     Arithmetic (val_s, []) )))
      | Greater -> (
          match value with
          | Phrase val_s ->
              Ok
                (Relation_clause
                   ( Phrase "is greater than or equal to",
                     Arithmetic ("len", []),
                     Arithmetic (val_s, []) )))
      | _ ->
          Ok
            (AttributeIsThatOf
               ([ Phrase (engl_string_of_typ t) ], None, value, Phrase "header"))
          (* (IsThatOf
             (
               [Phrase (engl_string_of_typ t)],
               value
             )) *))
  | None, Some d, None -> (
      match d with
      | Inbound | Outbound ->
          Ok
            (KeywordIsThatOf (Phrase "direction", Phrase (engl_string_of_dir d)))
      | Broadcast | Multicast -> Error "non-translatable expression"
      | _ ->
          Ok
            (AttributeIsThatOf
               ([ Phrase (engl_string_of_dir d) ], None, value, Phrase "header"))
          (* (IsThatOf
             (
               [Phrase (engl_string_of_dir d)],
               value
             )) *))
  | _ -> Error "single value with no field"

let engl_clause_of_primitive_disjunction (disjunction : pcap_expression list) :
    clause option =
  let rec flatten_values es =
    List.fold_right
      (fun p vals_opt ->
        match vals_opt with
        | Some vs -> (
            match p with
            | Primitive (fld, v) -> (
                let value = engl_term_of_value_atom v in
                match fld with
                | None, None, None -> Some (value :: vs)
                | _ -> None)
            | Or es' -> (
                match flatten_values es' with
                | Some vs' -> Some (vs' @ vs)
                | None -> None)
            | _ -> None)
        | None -> None)
      es (Some [])
  in
  match disjunction with
  | [] | [ _ ] -> None
  | Primitive prim :: es -> (
      let singe_values = flatten_values es in
      match singe_values with
      | Some vs -> (
          let fld, v = prim in
          let value = engl_term_of_value_atom v in
          match engl_clause_of_primitive prim with
          | Ok (AttributeIsThatOf (left, middle, _, name)) ->
              Some
                (AttributeIsThatOf (left, middle, TermList (value :: vs), name))
          | Ok (IsThatOf (left, _)) ->
              Some (IsThatOf (left, TermList (value :: vs)))
          | Ok (ThatHas (left, middle, _)) ->
              Some (ThatHas (left, middle, TermList (value :: vs)))
          | Ok _ -> None
          | Error _ -> None)
      | None -> None)
  | _ -> None

let ordinal_reference_csv_of_expr s byte_no nb_opt =
  let ordinal_name_string_of_no_bytes = function
    | One -> "byte"
    | Two -> "2 bytes"
    | Four -> "4 bytes"
  in
  let collection_name = engl_string_of_proto (proto_of_string s) in
  let ordinal_name_str =
    match nb_opt with
    | None -> ordinal_name_string_of_no_bytes One
    | Some nb -> ordinal_name_string_of_no_bytes nb
  in
  "ref_ord:" ^ string_of_expr byte_no ^ "|" ^ ordinal_name_str ^ "|"
  ^ collection_name ^ "|" ^ "header"

let rec contains_expr outer_e inner_e =
  match outer_e with
  | Identifier _ | Nat_Literal _ | Hex_Literal _ | Len | Packet_Access _ ->
      outer_e = inner_e
  | Plus es | Times es | Binary_And es | Binary_Or es | Binary_Xor es ->
      not
        (List.for_all (( = ) false)
           (List.map (fun e -> contains_expr e inner_e) es))
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) ->
      contains_expr e1 inner_e || contains_expr e2 inner_e

let matched_header_refs_expr (ref_pes : (expr * string) list) (e : expr) =
  let rec matched_header_refs_expr' e =
    let match_expr e' default =
      match List.assoc_opt e' ref_pes with
      | Some csv -> Identifier ("{" ^ csv ^ "}")
      | None -> default
    in
    match e with
    | Identifier s -> Identifier s
    | Nat_Literal i -> Nat_Literal i
    | Hex_Literal v -> Hex_Literal v
    | Len -> Len
    | Plus es as e' ->
        match_expr e' (Plus (List.map matched_header_refs_expr' es))
    | Minus (e1, e2) as e' ->
        match_expr e'
          (Minus (matched_header_refs_expr' e1, matched_header_refs_expr' e2))
    | Times es as e' ->
        match_expr e' (Times (List.map matched_header_refs_expr' es))
    | Quotient (e1, e2) as e' ->
        match_expr e'
          (Quotient (matched_header_refs_expr' e1, matched_header_refs_expr' e2))
    | Mod (e1, e2) as e' ->
        match_expr e'
          (Mod (matched_header_refs_expr' e1, matched_header_refs_expr' e2))
    | Binary_And es as e' ->
        match_expr e' (Binary_And (List.map matched_header_refs_expr' es))
    | Binary_Or es as e' ->
        match_expr e' (Binary_Or (List.map matched_header_refs_expr' es))
    | Binary_Xor es as e' -> Binary_Xor (List.map matched_header_refs_expr' es)
    | Shift_Right (e1, e2) as e' ->
        match_expr e'
          (Shift_Right
             (matched_header_refs_expr' e1, matched_header_refs_expr' e2))
    | Shift_Left (e1, e2) as e' ->
        match_expr e'
          (Shift_Left
             (matched_header_refs_expr' e1, matched_header_refs_expr' e2))
    | Packet_Access (s, e, nb_opt) as e' ->
        match_expr e' (Packet_Access (s, e, nb_opt))
  in
  match matched_header_refs_expr' e with
  (*
         if expr is a single packet access,
           replace it with an ordinal reference
  *)
  (* only translate packet accesses without expressions in them *)
  (* FIXME english cannot parse hex inside Packet_Access *)
  | Packet_Access (s, (Nat_Literal _ as byte_no), nb_opt) ->
      (* | Packet_Access (s, ((Hex_Literal _) as byte_no), nb_opt) *)
      Identifier ("{" ^ ordinal_reference_csv_of_expr s byte_no nb_opt ^ "}")
  | e' -> e'

let ordinal_reference_term_of_expr s e nb_opt =
  let ordinal_name_string_of_no_bytes = function
    | One -> "byte"
    | Two -> "2 bytes"
    | Four -> "4 bytes"
  in
  let collection_name = engl_string_of_proto (proto_of_string s) in
  let ordinal_name_str =
    match nb_opt with
    | None -> ordinal_name_string_of_no_bytes One
    | Some nb -> ordinal_name_string_of_no_bytes nb
  in
  OrdinalReference
    (string_of_expr e, ordinal_name_str, collection_name, "header")

let csv_of_ordinal_reference_term = function
  | OrdinalReference (byte_no, no_bytes, proto_s, col_name) ->
      "ref_ord:" ^ byte_no ^ "|" ^ no_bytes ^ "|" ^ proto_s ^ "|" ^ col_name
  | _ -> "impossible"

let term_of_csv csv =
  let [ name; params ] = String.split_on_char ':' csv in
  match name with
  (* "ref_ord:6|2 bytes|IPv4|header" *)
  | "ref_ord" ->
      let [ byte_num; no_bytes; proto_s; col_name ] =
        String.split_on_char '|' params
      in
      OrdinalReference (byte_num, no_bytes, proto_s, col_name)
  | "ref_field" ->
      let [ dir_typ_s; proto_s; col_name ] = String.split_on_char '|' params in
      let dir_typ_ss = String.split_on_char ',' dir_typ_s in
      AttributeIsThatOf_term
        ( List.map (fun x -> Phrase x) dir_typ_ss,
          Phrase proto_s,
          Phrase col_name )
  | "ref_other" ->
      let [ attr_s; proto_s; col_name ] = String.split_on_char '|' params in
      AttributeIsThatOf_term ([ Phrase attr_s ], Phrase proto_s, Phrase col_name)

let arith_val_of_split_csv_expr_string (it_var_opt : string option)
    (s :: ss : string list) : arith_val =
  let rec build_arith_val_refs ss =
    match ss with
    | s1 :: s2 :: ss' ->
        let used_it_var =
          match it_var_opt with Some i_s -> s1 = i_s | None -> false
        in
        (term_of_csv s1, s2, used_it_var) :: build_arith_val_refs ss'
    | [] -> []
  in
  Arithmetic (s, build_arith_val_refs ss)

let engl_clause_of_rel_expr (r : rel_expr) : clause =
  let split_on_braces e_s =
    e_s |> String.split_on_char '{'
    |> List.map (String.split_on_char '}')
    |> List.concat
  in
  let csv_string_of_expr ref_pes e =
    e |> matched_header_refs_expr ref_pes |> string_of_expr
  in
  let split_csv_string_of_expr ref_pes e =
    e |> csv_string_of_expr ref_pes |> split_on_braces
  in
  let relation_clause (comp : string) (e1 : expr) (e2 : expr) : clause =
    (* IMPORTANT must have 'it' variable reference last because 'it' might be inside these references *)
    let ref_pes =
      pcap_expr_proto_other_header_references
      @ pcap_expr_proto_field_header_references
      @ pcap_expr_proto_flag_header_references
    in
    match (e1, e2) with
    | Packet_Access (s, e, nb_opt), _ when contains_expr e2 e1 ->
        (* 'it' variable must be contained in other side's expression *)
        let it_term = ordinal_reference_term_of_expr s e nb_opt in
        let it_csv = csv_of_ordinal_reference_term it_term in
        let ref_pes = ref_pes @ [ (e1, it_csv) ] in
        let split_csv_s1 = split_csv_string_of_expr ref_pes e1 in
        let split_csv_s2 = split_csv_string_of_expr ref_pes e2 in
        ExaminingRelation_clause
          ( Phrase comp,
            it_term,
            arith_val_of_split_csv_expr_string (Some it_csv) split_csv_s1,
            arith_val_of_split_csv_expr_string (Some it_csv) split_csv_s2 )
    | _, Packet_Access (s, e, nb_opt) when contains_expr e1 e2 ->
        (* 'it' variable must be contained in other side's expression *)
        let it_term = ordinal_reference_term_of_expr s e nb_opt in
        let it_csv = csv_of_ordinal_reference_term it_term in
        let ref_pes = ref_pes @ [ (e2, it_csv) ] in
        let split_csv_s1 = split_csv_string_of_expr ref_pes e1 in
        let split_csv_s2 = split_csv_string_of_expr ref_pes e2 in
        ExaminingRelation_clause
          ( Phrase comp,
            it_term,
            arith_val_of_split_csv_expr_string (Some it_csv) split_csv_s1,
            arith_val_of_split_csv_expr_string (Some it_csv) split_csv_s2 )
    | _ -> (
        (* no 'it' variable *)
        let split_csv_s1 = split_csv_string_of_expr ref_pes e1 in
        let split_csv_s2 = split_csv_string_of_expr ref_pes e2 in
        let default_relation_clause =
          Relation_clause
            ( Phrase comp,
              arith_val_of_split_csv_expr_string None split_csv_s1,
              arith_val_of_split_csv_expr_string None split_csv_s2 )
        in
        let equal_relation_clause =
          match split_csv_s1 with
          | _ :: csv :: _ -> (
              match String.split_on_char ':' csv with
              | [ "ref_other"; params ] -> (
                  let [ attr_s; proto_s; col_name ] =
                    String.split_on_char '|' params
                  in
                  let v = string_of_expr e2 in
                  let expected_v =
                    pcap_expr_proto_flag_header_reference_values
                    |> List.assoc_opt csv
                  in
                  let with_attr_val_clause =
                    match expected_v with
                    | Some v_pe when string_of_expr v_pe = v ->
                        WithExistingAttributes
                          (Phrase proto_s, Phrase "flag", Phrase attr_s)
                    | _ ->
                        WithAttributeValue
                          (Phrase proto_s, Phrase attr_s, UnidentifiedString v)
                  in
                  match e2 with
                  | Identifier _ -> with_attr_val_clause
                  | Nat_Literal _ -> with_attr_val_clause
                  | Hex_Literal _ -> with_attr_val_clause
                  | _ -> default_relation_clause)
              | _ -> default_relation_clause)
          | _ -> default_relation_clause
        in
        match comp with
        | "is equal to" -> equal_relation_clause
        (* FIXME cannot make Not_expr at clause level *)
        (* | "does not equal" -> Not_expr equal_relation_clause *)
        | _ -> default_relation_clause)
  in
  match r with
  | Gt (left, right) -> relation_clause "is greater than" left right
  | Lt (left, right) -> relation_clause "is less than" left right
  | Geq (left, right) ->
      relation_clause "is greater than or equal to" left right
  | Leq (left, right) -> relation_clause "is less than or equal to" left right
  | Eq (left, right) -> relation_clause "is equal to" left right
  | Neq (left, right) -> relation_clause "does not equal" left right

let rec engl_expression_result_of_pcap_expression (pcap : pcap_expression) :
    (engl_expression, string) result =
  let list_expression_result expr_f es =
    Result.map expr_f
      (List.fold_right
         (fun e rest ->
           match rest with
           | Ok xs ->
               Result.map
                 (fun x -> x :: xs)
                 (engl_expression_result_of_pcap_expression e)
           | Error err -> Error err)
         es (Ok []))
  in
  let e_res =
    match pcap with
    | Primitive p -> Result.map (fun x -> Clause x) (engl_clause_of_primitive p)
    | Atom r -> Ok (Clause (engl_clause_of_rel_expr r))
    | And es -> list_expression_result (fun x -> And_expr x) es
    | Or es -> (
        match engl_clause_of_primitive_disjunction es with
        | Some c -> Ok (Clause c)
        | None -> list_expression_result (fun x -> Or_expr x) es)
    | Not e ->
        Result.map
          (fun x -> Not_expr x)
          (engl_expression_result_of_pcap_expression e)
    | True -> Ok True_expr
    | False -> Ok False_expr
  in
  match e_res with Ok e -> Ok e | Error err -> Error ("Semantic error: " ^ err)
