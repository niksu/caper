(*
  Copyright Marelle Leon, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: AST spec for English expressions
*)

type term =
  | Phrase of string
  | UnidentifiedString of string
  | With_term of string * string * string (* (name, left, right) *)
  | OfSize_term of
      string
      * string
      * string
      * string (* (size name, unit name, left, right) *)
  | OrdinalReference of string * string * string * string
    (* (number, ordinal name, collection name, collection type name) *)
  | AttributeIsThatOf_term of term list * term * term (* (attr, obj, value) *)
  | TermList of term list

(* to be parsed by the recipient of the AST *)
type arith_val = Arithmetic of string * (term * string * bool) list
(* (start unparsed arithmetic string,
    [ (English term, following unparsed arithmetic string); ... ]) *)

type clause =
  | OfType of term
  | ThatIs of term * term
  | AttributeIsThatOf of term list * term option * term * term
    (* (attr, obj, value) *)
  | IsThatOf of term list * term
  | KeywordIsThatOf of term * term
  | ThatHas of term * term list * term
  | Unit of term
  | WithAttributeValue of term * term * term (* (name, attr name, value) *)
  | WithExistingAttributes of
      term * term * term (* (name, attr collection name, attr name) *)
  | Relation_clause of
      term * arith_val * arith_val (* (comparator, left, right) *)
  | ExaminingRelation_clause of term * term * arith_val * arith_val
(* (comparator, "{it}" variable, left, right) *)
(* WARN: it_variable is technically not needed, it just makes translation back easier *)

type engl_expression =
  | Clause of clause
  | And_expr of engl_expression list
  | Or_expr of engl_expression list
  | Not_expr of engl_expression
  | True_expr
  | False_expr
