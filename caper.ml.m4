(*
  Copyright Nik Sultana, November 2018-December 2022
  Copyright Hyunsuk Bang, December 2022
  Copyright Marelle Leon, September 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Front-end.
*)

let expand_expression (pe : Pcap_syntax.pcap_expression) : Pcap_syntax.pcap_expression =
  pe
(*  |> Pcap_syntax.flatten_pcap_expression*)
  |> Simplifire.flatten
  |> Aux.diag_output_id (fun pe -> "Flattened: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Expand.conn_expand
  |> Aux.diag_output_id (fun pe -> "Disjunction-expanded: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> (fun pe ->
    if not !Config.unresolved_names_mode then
      pe
      |> Names.resolve_pe
      |> Aux.diag_output_id (fun pe -> "Resolved names: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    else pe)
  |> Expand.expand_primitives
  |> Aux.diag_output_id (fun pe -> "Expanded primitives: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Expand.nnf_pcap
  |> Aux.diag_output_id (fun pe -> "NNF (1): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

  |> (fun pe ->
    assert (!Config.num_implicit_clause_expansions >= 0);
    if !Config.num_implicit_clause_expansions = 0 then pe
    else
      begin
        Config.num_implicit_clause_expansions := !Config.num_implicit_clause_expansions - 1;
        pe
        |> Expand.implicit_expand_pe
        |> Aux.diag_output_id (fun pe -> "Expanded implicit clauses: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
      end)
  |> Simplifire.flatten
  |> Aux.diag_output_id (fun pe -> "Flattened (2): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Contract.detect_inconsistency_pe
  |> Aux.diag_output_id (fun pe -> "Inconsistency check: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

let exp_buffer : (int * Pcap_syntax.pcap_expression) option ref = ref None

let process_expression (pe : Pcap_syntax.pcap_expression) : Pcap_syntax.pcap_expression =
  pe
  |> Expand.nnf_pcap
  |> Aux.diag_output_id (fun pe -> "NNF (2): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Simplifire.flatten
  |> Aux.diag_output_id (fun pe -> "Flattened: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Contract.deduplicate_pe
  |> Aux.diag_output_id (fun pe -> "Deduplicated: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Contract.subsumption_pe
  |> Simplifire.simplify_pcap_singleton_connectives
  |> Aux.diag_output_id (fun pe -> "Subsumed: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

  |> Simplifire.simplify_pcap
  |> Aux.diag_output_id (fun pe -> "Simplified: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

  |> (fun pe ->
      if Pcap_syntax_aux.contains_protocol Vlan pe || Pcap_syntax_aux.contains_protocol Mpls pe then
        begin
          if !Config.diagnostic_output_mode then
            Printf.eprintf "NOTE: Not applying reordering because of side-effecting propositions: %s\n" (Pcap_syntax_aux.string_of_pcap_expression pe);
          pe
        end
      else
        pe
        |> Simplifire.trampoline "process_expression -- Reorder.reorder_subformulas" Reorder.reorder_subformulas
        |> Aux.diag_output_id (fun pe -> "Reordered (1): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
     )

  |> Simplifire.simplify_pcap
  |> Aux.diag_output_id (fun pe -> "Simplified: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

  |> (fun pe ->
      if !Config.weak_inference || Pcap_syntax_aux.pe_weight pe > !Config.max_strong_inference_fmla_size then pe
      else
        let solve pe = Simplifire.simplify_pcap (Simplifire.trampoline_until_pred "Clausify" Simplifire.is_clausal Simplifire.clausify pe) in
        let solve_pe = solve pe in
        if solve_pe = Pcap_syntax.True then Pcap_syntax.True
        else if solve_pe = Pcap_syntax.False then Pcap_syntax.False
        else
          let solve_Not_pe = solve (Pcap_syntax.Not pe) in
          if solve_Not_pe = Pcap_syntax.False then Pcap_syntax.True
          else if solve_Not_pe = Pcap_syntax.True then Pcap_syntax.False
          else pe)

  |> Simplifire.trampoline "process_expression -- undistribute (block)" (fun pe ->
      if !Config.not_undistribute then pe
      else pe
        |> Simplifire.undistribute_forall
        |> Aux.diag_output_id (fun pe -> "undistribute_forall: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

        |> Simplifire.flatten
        |> Aux.diag_output_id (fun pe -> "Flattened (2): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

        |> Simplifire.trampoline "process_expression -- Simplifire.undistribute_some" Simplifire.undistribute_some
        |> Simplifire.simplify_pcap_singleton_connectives
        |> Aux.diag_output_id (fun pe -> "undistribute_some: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

        |> Simplifire.flatten
        |> Aux.diag_output_id (fun pe -> "Flattened (3): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

        |> Simplifire.trampoline "process_expression -- Simplifire.undistribute_some_conj" Simplifire.undistribute_some_conj
        |> Simplifire.simplify_pcap_singleton_connectives
        |> Aux.diag_output_id (fun pe -> "undistribute_some_conj: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
        )

  |> Contract.deduplicate_pe
  |> Aux.diag_output_id (fun pe -> "Deduplicated (2): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
  |> Contract.subsumption_pe
  |> Aux.diag_output_id (fun pe -> "Subsumed (2): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

(*
  |> (fun pe ->
    assert (!Config.num_distribution_application >= 0);
    if !Config.num_distribution_application = 0 then pe
    else
      begin
        Config.num_distribution_application := !Config.num_distribution_application - 1;
        pe
        |> Simplifire.distribute_if_instructed
      end)
*)
(*
  |> (fun pe ->
    if !exp_buffer = None then
      begin
      exp_buffer := Some (Pcap_syntax_aux.pe_weight pe, pe);
      pe
      |> Simplifire.distribute_if_instructed
      end
    else
      let Some (prev_pe_weight, prev_pe) = !exp_buffer in
      if prev_pe_weight < Pcap_syntax_aux.pe_weight pe then
        begin
        exp_buffer := None;
        prev_pe
        end
      else if prev_pe_weight > Pcap_syntax_aux.pe_weight pe then
        begin
        exp_buffer := None;
        pe
        end
      else
        begin
        assert (prev_pe_weight = Pcap_syntax_aux.pe_weight pe);
        exp_buffer := None;
        prev_pe
        end)
*)

  |> Simplifire.simplify_pcap
  |> Aux.diag_output_id (fun pe -> "Simplified: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

  |> (fun pe ->
      if Pcap_syntax_aux.contains_protocol Vlan pe || Pcap_syntax_aux.contains_protocol Mpls pe then
        begin
          if !Config.diagnostic_output_mode then
            Printf.eprintf "NOTE: Not applying reordering because of side-effecting propositions: %s\n" (Pcap_syntax_aux.string_of_pcap_expression pe);
          pe
        end
      else
        pe
        |> Simplifire.trampoline "process_expression -- Reorder.reorder_subformulas" Reorder.reorder_subformulas
        |> Aux.diag_output_id (fun pe -> "Reordered (2): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
     )

type general_expression =
  | PCAP of Pcap_syntax.pcap_expression
  | SMT of Pcap_syntax.pcap_expression * Bitvector.bv_formula
ifdef(`CAPER_WITH_ENGLISH',`  | ENGL of English_syntax.engl_expression',`')

let convert_to_string (ge : general_expression) : string =
  match ge with
  | PCAP pe -> (if !Config.parse_and_prettyprint_mode
    then Pretty_printing.string_of_pcap_expression
    else Pcap_syntax_aux.string_of_pcap_expression ~relaxed:!Config.relaxed_mode) pe
    |> (fun s -> if String.get s 0 = '\n' then String.sub s 1 (String.length s - 1) else s)
  | SMT (pe, bv) ->
      "; " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n" ^
      Bitvector.string_of_bv_formula bv
ifdef(`CAPER_WITH_ENGLISH',`  | ENGL ee -> English_to_string.string_of_engl_expression ee',`')

let process (parsed_expression : Pcap_syntax.pcap_expression) : general_expression =
  let expand pe =
    pe
    |> Aux.diag_output_id (fun pe -> "Starting processing: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> (fun pe ->
      if not !Config.not_expand && not !Config.pseudonymise then
        pe
        |> Aux.diag_output_id (fun pe -> "Expanding expression: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
        |> expand_expression
        |> (fun pe ->
              if !Config.strict_expand_only then pe
              else Simplifire.trampoline "process_expression" process_expression pe)
      else pe)
    |> (fun pe ->
        if not (Pcap_wellformed.check_wellformedness_pe pe) then
          failwith (Aux.error_output_prefix ^ ": " ^ "Failed well-formedness check")
        else pe)
    |> (fun pe ->
      if !Config.pseudonymise then
        pe
        |> Aux.diag_output_id (fun pe -> "Pseudonymising: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
        |> Pseudonymise.pseudonymise_pe
      else pe)
    |> (fun pe ->
      if !Config.smt_translate then
        pe
        |> Aux.diag_output_id (fun pe -> "Translating to SMT: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
        |> Translate.translate_pe
        |> Bitvector_sizing.infer_width_formula
        |> (fun bv -> SMT (pe, bv))
      else PCAP pe)
  in
    if !Config.english_input_mode || !Config.english_output_mode then
      let e = if !Config.not_expand then PCAP parsed_expression else expand parsed_expression in
      if !Config.english_output_mode then
        match e with
        | PCAP pe ->
            ifdef(`CAPER_WITH_ENGLISH',`begin
              match Pcap_to_english.engl_expression_result_of_pcap_expression pe with
              | Ok ee -> ENGL ee
              | Error err -> failwith err
            end',`failwith "English conversion is not supported by this build."')
        | _ -> failwith "translating expression that is not Pcap into English"
      else PCAP parsed_expression
    else
      expand parsed_expression

let parse (e : string) : Pcap_syntax.pcap_expression =
  if !Config.english_input_mode then
    ifdef(`CAPER_WITH_ENGLISH',`match English_to_pcap.pcap_expression_result_of_english_string e with
    | Ok pe -> pe
    | Error err -> failwith err',`failwith "English conversion is not supported by this build."')
  else Parsing_support.parse_string e

let main =
  Cli.process_params (Array.to_list Sys.argv);
  if !Config.generate_html_output then Aux.standard_output ~raw:true ("<!--Caper v" ^ Config.version ^ "-->")
  else if not !Config.quiet_mode then Aux.standard_output ("Caper v" ^ Config.version) else ();
  if !Config.show_config_mode then Cli.print_config () else ();
  if not !Config.process_stdin_single_expression_mode && !Config.expression_list = [] then
    Cli.show_usage false (*FIXME exit with non-zero status*)
  else if !Config.process_stdin_single_expression_mode && !Config.expression_list <> [] then
    begin
      Aux.error_output "ERR (CLI): Cannot specify both -1stdin and -e\n";
      Cli.show_usage false
    end
  else if !Config.pseudonymise && (!Config.parse_and_prettyprint_mode || !Config.smt_translate || !Config.generate_html_output) then
    begin
      Aux.error_output "ERR (CLI): Cannot specify -pseudonymise and (-p or -HTML or -smt_translate)\n";
      exit 1
    end
  else if !Config.not_expand && !Config.strict_expand_only then
    begin
      Aux.error_output "ERR (CLI): Cannot specify both -not_expand and -strict_expand_only\n";
      exit 1
    end
  else ();
  if !Config.process_stdin_single_expression_mode && !Config.generate_bpf_output then
    Parsing_support.gather_input ()
    |> String.concat " "
    |> parse
    |> process
    |> convert_to_string
    |> Aux.diag_output_id (fun _ -> "Pretty-printed\n")
    |> Aux.bpf_output
    |> Aux.diag_output_id (fun _ -> "Done\n")
  else if !Config.process_stdin_single_expression_mode then
    Parsing_support.gather_input ()
    |> String.concat " "
    |> parse
    |> process
    |> convert_to_string
    |> Aux.diag_output_id (fun _ -> "Pretty-printed\n")
    |> Aux.standard_output
    |> Aux.diag_output_id (fun _ -> "Done\n")
  else if !Config.expression_list <> [] && !Config.generate_bpf_output then
    let expression_count = ref 1 in
    List.iter (fun e ->
      let processed_e =
        e
        |> parse
        |> process
        |> convert_to_string in
      if List.length (!Config.expression_list) > 1 then
        Aux.standard_output ("; expression " ^ string_of_int !expression_count ^ ":");
      Aux.bpf_output processed_e;
      expression_count := 1 + !expression_count
    ) !Config.expression_list
  else if !Config.expression_list <> [] then
    let expression_count = ref 1 in
    List.iter (fun e ->
      let processed_e =
        e
        |> parse
        |> process
        |> convert_to_string in
      if List.length (!Config.expression_list) > 1 then
        Aux.standard_output ("; expression " ^ string_of_int !expression_count ^ ":");
      (*FIXME Might be useful to enable this?
        Aux.standard_output ("; " ^ Pcap_syntax_aux.string_of_pcap_expression pe);*)
      Aux.standard_output processed_e;
      expression_count := 1 + !expression_count
    ) !Config.expression_list
  else
    begin
      Aux.error_output "ERR (CLI): Cannot specify both -1stdin and -e\n";
      Cli.show_usage false
    end
